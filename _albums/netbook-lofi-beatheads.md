---
layout: album
slug: netbook-lofi-beatheads
name: Netbook Lofi Beatheads
artists: Panix Hilton
bitrate: 320000
trackCount: 14
cover: /assets/albums/netbook-lofi-beatheads/1-wtf.jpeg
date: 2011-1-1
tracks:
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/1-wtf.mp3
    audio: /assets/albums/netbook-lofi-beatheads/1-wtf.mp3
    slug: netbook-lofi-beatheads/1-wtf
    albumSlug: netbook-lofi-beatheads
    trackSlug: 1-wtf
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/1-wtf.jpeg
    cover: /assets/albums/netbook-lofi-beatheads/1-wtf.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 130.11591836734695
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TYER
          value: '2011'
        - id: TALB
          value: Netbook Lofi Beatheads
        - id: TRCK
          value: '1'
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: WTF
    quality:
      warnings: []
    common:
      track:
        'no': 1
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2011
      album: Netbook Lofi Beatheads
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: WTF
    transformed:
      ID3v2.3:
        TCON: Electronic
        TYER: '2011'
        TALB: Netbook Lofi Beatheads
        TRCK: '1'
        TPE1: Panix Hilton
        TIT2: WTF
    all:
      TCON: Electronic
      TYER: '2011'
      TALB: Netbook Lofi Beatheads
      TRCK: '1'
      TPE1: Panix Hilton
      TIT2: WTF
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/2-blockerad.mp3
    audio: /assets/albums/netbook-lofi-beatheads/2-blockerad.mp3
    slug: netbook-lofi-beatheads/2-blockerad
    albumSlug: netbook-lofi-beatheads
    trackSlug: 2-blockerad
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/2-blockerad.jpeg
    cover: /assets/albums/netbook-lofi-beatheads/2-blockerad.jpeg
    format:
      tagTypes:
        - ID3v2.4
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 133.90367346938777
    native:
      ID3v2.4:
        - id: TCON
          value: Electronic
        - id: TDRC
          value: '2011'
        - id: TYER
          value: '2011'
        - id: TALB
          value: Netbook Lofi Beatheads
        - id: TRCK
          value: '2'
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: Blockerad
    quality:
      warnings: []
    common:
      track:
        'no': 2
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2011
      date: '2011'
      album: Netbook Lofi Beatheads
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: Blockerad
    transformed:
      ID3v2.4:
        TCON: Electronic
        TDRC: '2011'
        TYER: '2011'
        TALB: Netbook Lofi Beatheads
        TRCK: '2'
        TPE1: Panix Hilton
        TIT2: Blockerad
    all:
      TCON: Electronic
      TDRC: '2011'
      TYER: '2011'
      TALB: Netbook Lofi Beatheads
      TRCK: '2'
      TPE1: Panix Hilton
      TIT2: Blockerad
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/3-conscious.mp3
    audio: /assets/albums/netbook-lofi-beatheads/3-conscious.mp3
    slug: netbook-lofi-beatheads/3-conscious
    albumSlug: netbook-lofi-beatheads
    trackSlug: 3-conscious
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/3-conscious.jpeg
    cover: /assets/albums/netbook-lofi-beatheads/3-conscious.jpeg
    format:
      tagTypes:
        - ID3v2.4
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 115.25224489795919
    native:
      ID3v2.4:
        - id: TCON
          value: Electronic
        - id: TDRC
          value: '2011'
        - id: TYER
          value: '2011'
        - id: TALB
          value: Netbook Lofi Beatheads
        - id: TRCK
          value: '3'
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: Conscious
    quality:
      warnings: []
    common:
      track:
        'no': 3
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2011
      date: '2011'
      album: Netbook Lofi Beatheads
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: Conscious
    transformed:
      ID3v2.4:
        TCON: Electronic
        TDRC: '2011'
        TYER: '2011'
        TALB: Netbook Lofi Beatheads
        TRCK: '3'
        TPE1: Panix Hilton
        TIT2: Conscious
    all:
      TCON: Electronic
      TDRC: '2011'
      TYER: '2011'
      TALB: Netbook Lofi Beatheads
      TRCK: '3'
      TPE1: Panix Hilton
      TIT2: Conscious
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/4-frankentune.mp3
    audio: /assets/albums/netbook-lofi-beatheads/4-frankentune.mp3
    slug: netbook-lofi-beatheads/4-frankentune
    albumSlug: netbook-lofi-beatheads
    trackSlug: 4-frankentune
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/4-frankentune.jpeg
    cover: /assets/albums/netbook-lofi-beatheads/4-frankentune.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 328.385306122449
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TYER
          value: '2011'
        - id: TALB
          value: Netbook Lofi Beatheads
        - id: TRCK
          value: '4'
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: Frankentune
    quality:
      warnings: []
    common:
      track:
        'no': 4
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2011
      album: Netbook Lofi Beatheads
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: Frankentune
    transformed:
      ID3v2.3:
        TCON: Electronic
        TYER: '2011'
        TALB: Netbook Lofi Beatheads
        TRCK: '4'
        TPE1: Panix Hilton
        TIT2: Frankentune
    all:
      TCON: Electronic
      TYER: '2011'
      TALB: Netbook Lofi Beatheads
      TRCK: '4'
      TPE1: Panix Hilton
      TIT2: Frankentune
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/5-interlude.mp3
    audio: /assets/albums/netbook-lofi-beatheads/5-interlude.mp3
    slug: netbook-lofi-beatheads/5-interlude
    albumSlug: netbook-lofi-beatheads
    trackSlug: 5-interlude
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/5-interlude.jpeg
    cover: /assets/albums/netbook-lofi-beatheads/5-interlude.jpeg
    format:
      tagTypes:
        - ID3v2.4
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 121.10367346938776
    native:
      ID3v2.4:
        - id: TCON
          value: Electronic
        - id: TDRC
          value: '2011'
        - id: TYER
          value: '2011'
        - id: TALB
          value: Netbook Lofi Beatheads
        - id: TRCK
          value: '5'
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: Interlude
    quality:
      warnings: []
    common:
      track:
        'no': 5
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2011
      date: '2011'
      album: Netbook Lofi Beatheads
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: Interlude
    transformed:
      ID3v2.4:
        TCON: Electronic
        TDRC: '2011'
        TYER: '2011'
        TALB: Netbook Lofi Beatheads
        TRCK: '5'
        TPE1: Panix Hilton
        TIT2: Interlude
    all:
      TCON: Electronic
      TDRC: '2011'
      TYER: '2011'
      TALB: Netbook Lofi Beatheads
      TRCK: '5'
      TPE1: Panix Hilton
      TIT2: Interlude
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/6-lukewarm.mp3
    audio: /assets/albums/netbook-lofi-beatheads/6-lukewarm.mp3
    slug: netbook-lofi-beatheads/6-lukewarm
    albumSlug: netbook-lofi-beatheads
    trackSlug: 6-lukewarm
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/6-lukewarm.jpeg
    cover: /assets/albums/netbook-lofi-beatheads/6-lukewarm.jpeg
    format:
      tagTypes:
        - ID3v2.4
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 138.2922448979592
    native:
      ID3v2.4:
        - id: TCON
          value: Electronic
        - id: TDRC
          value: '2011'
        - id: TYER
          value: '2011'
        - id: TALB
          value: Netbook Lofi Beatheads
        - id: TRCK
          value: '6'
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: Lukewarm
    quality:
      warnings: []
    common:
      track:
        'no': 6
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2011
      date: '2011'
      album: Netbook Lofi Beatheads
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: Lukewarm
    transformed:
      ID3v2.4:
        TCON: Electronic
        TDRC: '2011'
        TYER: '2011'
        TALB: Netbook Lofi Beatheads
        TRCK: '6'
        TPE1: Panix Hilton
        TIT2: Lukewarm
    all:
      TCON: Electronic
      TDRC: '2011'
      TYER: '2011'
      TALB: Netbook Lofi Beatheads
      TRCK: '6'
      TPE1: Panix Hilton
      TIT2: Lukewarm
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/7-montag000.mp3
    audio: /assets/albums/netbook-lofi-beatheads/7-montag000.mp3
    slug: netbook-lofi-beatheads/7-montag000
    albumSlug: netbook-lofi-beatheads
    trackSlug: 7-montag000
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/7-montag000.jpeg
    cover: /assets/albums/netbook-lofi-beatheads/7-montag000.jpeg
    format:
      tagTypes:
        - ID3v2.4
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 128.05224489795918
    native:
      ID3v2.4:
        - id: TCON
          value: Electronic
        - id: TDRC
          value: '2011'
        - id: TYER
          value: '2011'
        - id: TALB
          value: Netbook Lofi Beatheads
        - id: TRCK
          value: '7'
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: Montag000
    quality:
      warnings: []
    common:
      track:
        'no': 7
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2011
      date: '2011'
      album: Netbook Lofi Beatheads
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: Montag000
    transformed:
      ID3v2.4:
        TCON: Electronic
        TDRC: '2011'
        TYER: '2011'
        TALB: Netbook Lofi Beatheads
        TRCK: '7'
        TPE1: Panix Hilton
        TIT2: Montag000
    all:
      TCON: Electronic
      TDRC: '2011'
      TYER: '2011'
      TALB: Netbook Lofi Beatheads
      TRCK: '7'
      TPE1: Panix Hilton
      TIT2: Montag000
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/8-montag001.mp3
    audio: /assets/albums/netbook-lofi-beatheads/8-montag001.mp3
    slug: netbook-lofi-beatheads/8-montag001
    albumSlug: netbook-lofi-beatheads
    trackSlug: 8-montag001
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/8-montag001.jpeg
    cover: /assets/albums/netbook-lofi-beatheads/8-montag001.jpeg
    format:
      tagTypes:
        - ID3v2.4
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 117.39428571428572
    native:
      ID3v2.4:
        - id: TCON
          value: Electronic
        - id: TDRC
          value: '2011'
        - id: TYER
          value: '2011'
        - id: TALB
          value: Netbook Lofi Beatheads
        - id: TRCK
          value: '8'
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: Montag001
    quality:
      warnings: []
    common:
      track:
        'no': 8
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2011
      date: '2011'
      album: Netbook Lofi Beatheads
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: Montag001
    transformed:
      ID3v2.4:
        TCON: Electronic
        TDRC: '2011'
        TYER: '2011'
        TALB: Netbook Lofi Beatheads
        TRCK: '8'
        TPE1: Panix Hilton
        TIT2: Montag001
    all:
      TCON: Electronic
      TDRC: '2011'
      TYER: '2011'
      TALB: Netbook Lofi Beatheads
      TRCK: '8'
      TPE1: Panix Hilton
      TIT2: Montag001
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/9-neckhurt.mp3
    audio: /assets/albums/netbook-lofi-beatheads/9-neckhurt.mp3
    slug: netbook-lofi-beatheads/9-neckhurt
    albumSlug: netbook-lofi-beatheads
    trackSlug: 9-neckhurt
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/9-neckhurt.jpeg
    cover: /assets/albums/netbook-lofi-beatheads/9-neckhurt.jpeg
    format:
      tagTypes:
        - ID3v2.4
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 172.8522448979592
    native:
      ID3v2.4:
        - id: TCON
          value: Electronic
        - id: TDRC
          value: '2011'
        - id: TYER
          value: '2011'
        - id: TALB
          value: Netbook Lofi Beatheads
        - id: TRCK
          value: '9'
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: Neckhurt
    quality:
      warnings: []
    common:
      track:
        'no': 9
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2011
      date: '2011'
      album: Netbook Lofi Beatheads
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: Neckhurt
    transformed:
      ID3v2.4:
        TCON: Electronic
        TDRC: '2011'
        TYER: '2011'
        TALB: Netbook Lofi Beatheads
        TRCK: '9'
        TPE1: Panix Hilton
        TIT2: Neckhurt
    all:
      TCON: Electronic
      TDRC: '2011'
      TYER: '2011'
      TALB: Netbook Lofi Beatheads
      TRCK: '9'
      TPE1: Panix Hilton
      TIT2: Neckhurt
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/10-one-cycle.mp3
    audio: /assets/albums/netbook-lofi-beatheads/10-one-cycle.mp3
    slug: netbook-lofi-beatheads/10-one-cycle
    albumSlug: netbook-lofi-beatheads
    trackSlug: 10-one-cycle
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/10-one-cycle.jpeg
    cover: /assets/albums/netbook-lofi-beatheads/10-one-cycle.jpeg
    format:
      tagTypes:
        - ID3v2.4
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 93.17877551020408
    native:
      ID3v2.4:
        - id: TCON
          value: Electronic
        - id: TDRC
          value: '2011'
        - id: TYER
          value: '2011'
        - id: TALB
          value: Netbook Lofi Beatheads
        - id: TRCK
          value: '10'
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: One Cycle
    quality:
      warnings: []
    common:
      track:
        'no': 10
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2011
      date: '2011'
      album: Netbook Lofi Beatheads
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: One Cycle
    transformed:
      ID3v2.4:
        TCON: Electronic
        TDRC: '2011'
        TYER: '2011'
        TALB: Netbook Lofi Beatheads
        TRCK: '10'
        TPE1: Panix Hilton
        TIT2: One Cycle
    all:
      TCON: Electronic
      TDRC: '2011'
      TYER: '2011'
      TALB: Netbook Lofi Beatheads
      TRCK: '10'
      TPE1: Panix Hilton
      TIT2: One Cycle
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/11-one-cycle-two.mp3
    audio: /assets/albums/netbook-lofi-beatheads/11-one-cycle-two.mp3
    slug: netbook-lofi-beatheads/11-one-cycle-two
    albumSlug: netbook-lofi-beatheads
    trackSlug: 11-one-cycle-two
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/11-one-cycle-two.jpeg
    cover: /assets/albums/netbook-lofi-beatheads/11-one-cycle-two.jpeg
    format:
      tagTypes:
        - ID3v2.4
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 157.77959183673468
    native:
      ID3v2.4:
        - id: TCON
          value: Electronic
        - id: TDRC
          value: '2011'
        - id: TYER
          value: '2011'
        - id: TALB
          value: Netbook Lofi Beatheads
        - id: TRCK
          value: '11'
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: One Cycle Two
    quality:
      warnings: []
    common:
      track:
        'no': 11
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2011
      date: '2011'
      album: Netbook Lofi Beatheads
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: One Cycle Two
    transformed:
      ID3v2.4:
        TCON: Electronic
        TDRC: '2011'
        TYER: '2011'
        TALB: Netbook Lofi Beatheads
        TRCK: '11'
        TPE1: Panix Hilton
        TIT2: One Cycle Two
    all:
      TCON: Electronic
      TDRC: '2011'
      TYER: '2011'
      TALB: Netbook Lofi Beatheads
      TRCK: '11'
      TPE1: Panix Hilton
      TIT2: One Cycle Two
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/12-show-you-love.mp3
    audio: /assets/albums/netbook-lofi-beatheads/12-show-you-love.mp3
    slug: netbook-lofi-beatheads/12-show-you-love
    albumSlug: netbook-lofi-beatheads
    trackSlug: 12-show-you-love
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/12-show-you-love.jpeg
    cover: /assets/albums/netbook-lofi-beatheads/12-show-you-love.jpeg
    format:
      tagTypes:
        - ID3v2.4
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 145.97224489795917
    native:
      ID3v2.4:
        - id: TCON
          value: Electronic
        - id: TDRC
          value: '2011'
        - id: TYER
          value: '2011'
        - id: TALB
          value: Netbook Lofi Beatheads
        - id: TRCK
          value: '12'
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: Show You Love
    quality:
      warnings: []
    common:
      track:
        'no': 12
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2011
      date: '2011'
      album: Netbook Lofi Beatheads
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: Show You Love
    transformed:
      ID3v2.4:
        TCON: Electronic
        TDRC: '2011'
        TYER: '2011'
        TALB: Netbook Lofi Beatheads
        TRCK: '12'
        TPE1: Panix Hilton
        TIT2: Show You Love
    all:
      TCON: Electronic
      TDRC: '2011'
      TYER: '2011'
      TALB: Netbook Lofi Beatheads
      TRCK: '12'
      TPE1: Panix Hilton
      TIT2: Show You Love
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/13-waapper.mp3
    audio: /assets/albums/netbook-lofi-beatheads/13-waapper.mp3
    slug: netbook-lofi-beatheads/13-waapper
    albumSlug: netbook-lofi-beatheads
    trackSlug: 13-waapper
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/13-waapper.jpeg
    cover: /assets/albums/netbook-lofi-beatheads/13-waapper.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 115.25224489795919
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TYER
          value: '2011'
        - id: TALB
          value: Netbook Lofi Beatheads
        - id: TRCK
          value: '13'
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: Waapper
    quality:
      warnings: []
    common:
      track:
        'no': 13
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2011
      album: Netbook Lofi Beatheads
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: Waapper
    transformed:
      ID3v2.3:
        TCON: Electronic
        TYER: '2011'
        TALB: Netbook Lofi Beatheads
        TRCK: '13'
        TPE1: Panix Hilton
        TIT2: Waapper
    all:
      TCON: Electronic
      TYER: '2011'
      TALB: Netbook Lofi Beatheads
      TRCK: '13'
      TPE1: Panix Hilton
      TIT2: Waapper
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/14-voicing.mp3
    audio: /assets/albums/netbook-lofi-beatheads/14-voicing.mp3
    slug: netbook-lofi-beatheads/14-voicing
    albumSlug: netbook-lofi-beatheads
    trackSlug: 14-voicing
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/14-voicing.jpeg
    cover: /assets/albums/netbook-lofi-beatheads/14-voicing.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 101.79918367346939
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TYER
          value: '2011'
        - id: TALB
          value: Netbook Lofi Beatheads
        - id: TRCK
          value: '14'
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: Voicing
    quality:
      warnings: []
    common:
      track:
        'no': 14
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2011
      album: Netbook Lofi Beatheads
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: Voicing
    transformed:
      ID3v2.3:
        TCON: Electronic
        TYER: '2011'
        TALB: Netbook Lofi Beatheads
        TRCK: '14'
        TPE1: Panix Hilton
        TIT2: Voicing
    all:
      TCON: Electronic
      TYER: '2011'
      TALB: Netbook Lofi Beatheads
      TRCK: '14'
      TPE1: Panix Hilton
      TIT2: Voicing
---
