---
layout: album
slug: 2012-grab-bag
name: 2012 Grab Bag
artists: Panix Hilton
bitrate: 320000
trackCount: 11
cover: /assets/albums/2012-grab-bag/1-lauantai.jpeg
date: 2012-1-1
tracks:
  - path: /mnt/usb32gb/organized-pax-nion/assets/albums/2012-grab-bag/1-lauantai.mp3
    audio: /assets/albums/2012-grab-bag/1-lauantai.mp3
    slug: 2012-grab-bag/1-lauantai
    albumSlug: 2012-grab-bag
    trackSlug: 1-lauantai
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/2012-grab-bag/1-lauantai.jpeg
    cover: /assets/albums/2012-grab-bag/1-lauantai.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 112.43102040816326
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TYER
          value: '2012'
        - id: TALB
          value: 2012 Grab Bag
        - id: TRCK
          value: '1'
        - id: TIT2
          value: Lauantai
        - id: TPE1
          value: Panix Hilton
    quality:
      warnings: []
    common:
      track:
        'no': 1
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2012
      album: 2012 Grab Bag
      title: Lauantai
      artists:
        - Panix Hilton
      artist: Panix Hilton
    transformed:
      ID3v2.3:
        TCON: Electronic
        TYER: '2012'
        TALB: 2012 Grab Bag
        TRCK: '1'
        TIT2: Lauantai
        TPE1: Panix Hilton
    all:
      TCON: Electronic
      TYER: '2012'
      TALB: 2012 Grab Bag
      TRCK: '1'
      TIT2: Lauantai
      TPE1: Panix Hilton
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/2012-grab-bag/2-hookin-for-cheeseburgers.mp3
    audio: /assets/albums/2012-grab-bag/2-hookin-for-cheeseburgers.mp3
    slug: 2012-grab-bag/2-hookin-for-cheeseburgers
    albumSlug: 2012-grab-bag
    trackSlug: 2-hookin-for-cheeseburgers
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/2012-grab-bag/2-hookin-for-cheeseburgers.jpeg
    cover: /assets/albums/2012-grab-bag/2-hookin-for-cheeseburgers.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 112.53551020408163
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TYER
          value: '2012'
        - id: TALB
          value: 2012 Grab Bag
        - id: TRCK
          value: '2'
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: Hookin For Cheeseburgers
    quality:
      warnings: []
    common:
      track:
        'no': 2
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2012
      album: 2012 Grab Bag
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: Hookin For Cheeseburgers
    transformed:
      ID3v2.3:
        TCON: Electronic
        TYER: '2012'
        TALB: 2012 Grab Bag
        TRCK: '2'
        TPE1: Panix Hilton
        TIT2: Hookin For Cheeseburgers
    all:
      TCON: Electronic
      TYER: '2012'
      TALB: 2012 Grab Bag
      TRCK: '2'
      TPE1: Panix Hilton
      TIT2: Hookin For Cheeseburgers
  - path: /mnt/usb32gb/organized-pax-nion/assets/albums/2012-grab-bag/3-mandaak.mp3
    audio: /assets/albums/2012-grab-bag/3-mandaak.mp3
    slug: 2012-grab-bag/3-mandaak
    albumSlug: 2012-grab-bag
    trackSlug: 3-mandaak
    coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/2012-grab-bag/3-mandaak.jpeg
    cover: /assets/albums/2012-grab-bag/3-mandaak.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 140.87836734693877
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TYER
          value: '2012'
        - id: TALB
          value: 2012 Grab Bag
        - id: TRCK
          value: '3'
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: Mandaak
    quality:
      warnings: []
    common:
      track:
        'no': 3
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2012
      album: 2012 Grab Bag
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: Mandaak
    transformed:
      ID3v2.3:
        TCON: Electronic
        TYER: '2012'
        TALB: 2012 Grab Bag
        TRCK: '3'
        TPE1: Panix Hilton
        TIT2: Mandaak
    all:
      TCON: Electronic
      TYER: '2012'
      TALB: 2012 Grab Bag
      TRCK: '3'
      TPE1: Panix Hilton
      TIT2: Mandaak
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/2012-grab-bag/4-inte-sova.mp3
    audio: /assets/albums/2012-grab-bag/4-inte-sova.mp3
    slug: 2012-grab-bag/4-inte-sova
    albumSlug: 2012-grab-bag
    trackSlug: 4-inte-sova
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/2012-grab-bag/4-inte-sova.jpeg
    cover: /assets/albums/2012-grab-bag/4-inte-sova.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 131.34367346938777
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TYER
          value: '2012'
        - id: TALB
          value: 2012 Grab Bag
        - id: TRCK
          value: '4'
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: Inte Sova
    quality:
      warnings: []
    common:
      track:
        'no': 4
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2012
      album: 2012 Grab Bag
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: Inte Sova
    transformed:
      ID3v2.3:
        TCON: Electronic
        TYER: '2012'
        TALB: 2012 Grab Bag
        TRCK: '4'
        TPE1: Panix Hilton
        TIT2: Inte Sova
    all:
      TCON: Electronic
      TYER: '2012'
      TALB: 2012 Grab Bag
      TRCK: '4'
      TPE1: Panix Hilton
      TIT2: Inte Sova
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/2012-grab-bag/5-you-need-it.mp3
    audio: /assets/albums/2012-grab-bag/5-you-need-it.mp3
    slug: 2012-grab-bag/5-you-need-it
    albumSlug: 2012-grab-bag
    trackSlug: 5-you-need-it
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/2012-grab-bag/5-you-need-it.jpeg
    cover: /assets/albums/2012-grab-bag/5-you-need-it.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 146.59918367346938
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TYER
          value: '2012'
        - id: TALB
          value: 2012 Grab Bag
        - id: TRCK
          value: '5'
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: You Need It
    quality:
      warnings: []
    common:
      track:
        'no': 5
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2012
      album: 2012 Grab Bag
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: You Need It
    transformed:
      ID3v2.3:
        TCON: Electronic
        TYER: '2012'
        TALB: 2012 Grab Bag
        TRCK: '5'
        TPE1: Panix Hilton
        TIT2: You Need It
    all:
      TCON: Electronic
      TYER: '2012'
      TALB: 2012 Grab Bag
      TRCK: '5'
      TPE1: Panix Hilton
      TIT2: You Need It
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/2012-grab-bag/6-spinal-cord-forever.mp3
    audio: /assets/albums/2012-grab-bag/6-spinal-cord-forever.mp3
    slug: 2012-grab-bag/6-spinal-cord-forever
    albumSlug: 2012-grab-bag
    trackSlug: 6-spinal-cord-forever
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/2012-grab-bag/6-spinal-cord-forever.jpeg
    cover: /assets/albums/2012-grab-bag/6-spinal-cord-forever.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 142.785306122449
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TYER
          value: '2012'
        - id: TALB
          value: 2012 Grab Bag
        - id: TRCK
          value: '6'
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: Spinal Cord Forever
    quality:
      warnings: []
    common:
      track:
        'no': 6
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2012
      album: 2012 Grab Bag
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: Spinal Cord Forever
    transformed:
      ID3v2.3:
        TCON: Electronic
        TYER: '2012'
        TALB: 2012 Grab Bag
        TRCK: '6'
        TPE1: Panix Hilton
        TIT2: Spinal Cord Forever
    all:
      TCON: Electronic
      TYER: '2012'
      TALB: 2012 Grab Bag
      TRCK: '6'
      TPE1: Panix Hilton
      TIT2: Spinal Cord Forever
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/2012-grab-bag/7-bored-beyond-belief.mp3
    audio: /assets/albums/2012-grab-bag/7-bored-beyond-belief.mp3
    slug: 2012-grab-bag/7-bored-beyond-belief
    albumSlug: 2012-grab-bag
    trackSlug: 7-bored-beyond-belief
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/2012-grab-bag/7-bored-beyond-belief.jpeg
    cover: /assets/albums/2012-grab-bag/7-bored-beyond-belief.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 85.62938775510204
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TYER
          value: '2012'
        - id: TALB
          value: 2012 Grab Bag
        - id: TRCK
          value: '7'
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: Bored Beyond Belief
    quality:
      warnings: []
    common:
      track:
        'no': 7
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2012
      album: 2012 Grab Bag
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: Bored Beyond Belief
    transformed:
      ID3v2.3:
        TCON: Electronic
        TYER: '2012'
        TALB: 2012 Grab Bag
        TRCK: '7'
        TPE1: Panix Hilton
        TIT2: Bored Beyond Belief
    all:
      TCON: Electronic
      TYER: '2012'
      TALB: 2012 Grab Bag
      TRCK: '7'
      TPE1: Panix Hilton
      TIT2: Bored Beyond Belief
  - path: /mnt/usb32gb/organized-pax-nion/assets/albums/2012-grab-bag/8-orkarnt.mp3
    audio: /assets/albums/2012-grab-bag/8-orkarnt.mp3
    slug: 2012-grab-bag/8-orkarnt
    albumSlug: 2012-grab-bag
    trackSlug: 8-orkarnt
    coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/2012-grab-bag/8-orkarnt.jpeg
    cover: /assets/albums/2012-grab-bag/8-orkarnt.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME 3.98.4
      duration: 127.32081632653062
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TYER
          value: '2012'
        - id: TALB
          value: 2012 Grab Bag
        - id: TRCK
          value: '8'
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: Orkarnt
    quality:
      warnings: []
    common:
      track:
        'no': 8
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2012
      album: 2012 Grab Bag
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: Orkarnt
    transformed:
      ID3v2.3:
        TCON: Electronic
        TYER: '2012'
        TALB: 2012 Grab Bag
        TRCK: '8'
        TPE1: Panix Hilton
        TIT2: Orkarnt
    all:
      TCON: Electronic
      TYER: '2012'
      TALB: 2012 Grab Bag
      TRCK: '8'
      TPE1: Panix Hilton
      TIT2: Orkarnt
  - path: /mnt/usb32gb/organized-pax-nion/assets/albums/2012-grab-bag/9-skithink.mp3
    audio: /assets/albums/2012-grab-bag/9-skithink.mp3
    slug: 2012-grab-bag/9-skithink
    albumSlug: 2012-grab-bag
    trackSlug: 9-skithink
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/2012-grab-bag/9-skithink.jpeg
    cover: /assets/albums/2012-grab-bag/9-skithink.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 103.31428571428572
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TYER
          value: '2012'
        - id: TALB
          value: 2012 Grab Bag
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: Skithink
        - id: TRCK
          value: '9'
    quality:
      warnings: []
    common:
      track:
        'no': 9
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2012
      album: 2012 Grab Bag
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: Skithink
    transformed:
      ID3v2.3:
        TCON: Electronic
        TYER: '2012'
        TALB: 2012 Grab Bag
        TPE1: Panix Hilton
        TIT2: Skithink
        TRCK: '9'
    all:
      TCON: Electronic
      TYER: '2012'
      TALB: 2012 Grab Bag
      TPE1: Panix Hilton
      TIT2: Skithink
      TRCK: '9'
  - path: /mnt/usb32gb/organized-pax-nion/assets/albums/2012-grab-bag/10-tracks.mp3
    audio: /assets/albums/2012-grab-bag/10-tracks.mp3
    slug: 2012-grab-bag/10-tracks
    albumSlug: 2012-grab-bag
    trackSlug: 10-tracks
    coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/2012-grab-bag/10-tracks.jpeg
    cover: /assets/albums/2012-grab-bag/10-tracks.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 156.36897959183673
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TYER
          value: '2012'
        - id: TALB
          value: 2012 Grab Bag
        - id: TRCK
          value: '10'
        - id: TIT2
          value: Tracks
        - id: TPE1
          value: Panix Hilton
    quality:
      warnings: []
    common:
      track:
        'no': 10
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2012
      album: 2012 Grab Bag
      title: Tracks
      artists:
        - Panix Hilton
      artist: Panix Hilton
    transformed:
      ID3v2.3:
        TCON: Electronic
        TYER: '2012'
        TALB: 2012 Grab Bag
        TRCK: '10'
        TIT2: Tracks
        TPE1: Panix Hilton
    all:
      TCON: Electronic
      TYER: '2012'
      TALB: 2012 Grab Bag
      TRCK: '10'
      TIT2: Tracks
      TPE1: Panix Hilton
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/2012-grab-bag/11-verbophobia.mp3
    audio: /assets/albums/2012-grab-bag/11-verbophobia.mp3
    slug: 2012-grab-bag/11-verbophobia
    albumSlug: 2012-grab-bag
    trackSlug: 11-verbophobia
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/2012-grab-bag/11-verbophobia.jpeg
    cover: /assets/albums/2012-grab-bag/11-verbophobia.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 114.20734693877552
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TYER
          value: '2012'
        - id: TALB
          value: 2012 Grab Bag
        - id: TRCK
          value: '11'
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: Verbophobia
    quality:
      warnings: []
    common:
      track:
        'no': 11
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2012
      album: 2012 Grab Bag
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: Verbophobia
    transformed:
      ID3v2.3:
        TCON: Electronic
        TYER: '2012'
        TALB: 2012 Grab Bag
        TRCK: '11'
        TPE1: Panix Hilton
        TIT2: Verbophobia
    all:
      TCON: Electronic
      TYER: '2012'
      TALB: 2012 Grab Bag
      TRCK: '11'
      TPE1: Panix Hilton
      TIT2: Verbophobia
---
