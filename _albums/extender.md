---
layout: album
slug: extender
name: Extender
artists: Panix Hilton
bitrate: 320000
trackCount: 6
cover: /assets/albums/extender/1-house-kommer-alltid-att-vara-house.jpeg
date: 2012-1-1
tracks:
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/extender/1-house-kommer-alltid-att-vara-house.mp3
    audio: /assets/albums/extender/1-house-kommer-alltid-att-vara-house.mp3
    slug: extender/1-house-kommer-alltid-att-vara-house
    albumSlug: extender
    trackSlug: 1-house-kommer-alltid-att-vara-house
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/extender/1-house-kommer-alltid-att-vara-house.jpeg
    cover: /assets/albums/extender/1-house-kommer-alltid-att-vara-house.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 137.87428571428572
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TYER
          value: '2012'
        - id: TALB
          value: Extender
        - id: TRCK
          value: '1'
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: House Kommer Alltid Att Vara House
    quality:
      warnings: []
    common:
      track:
        'no': 1
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2012
      album: Extender
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: House Kommer Alltid Att Vara House
    transformed:
      ID3v2.3:
        TCON: Electronic
        TYER: '2012'
        TALB: Extender
        TRCK: '1'
        TPE1: Panix Hilton
        TIT2: House Kommer Alltid Att Vara House
    all:
      TCON: Electronic
      TYER: '2012'
      TALB: Extender
      TRCK: '1'
      TPE1: Panix Hilton
      TIT2: House Kommer Alltid Att Vara House
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/extender/2-i-want-your-soul.mp3
    audio: /assets/albums/extender/2-i-want-your-soul.mp3
    slug: extender/2-i-want-your-soul
    albumSlug: extender
    trackSlug: 2-i-want-your-soul
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/extender/2-i-want-your-soul.jpeg
    cover: /assets/albums/extender/2-i-want-your-soul.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 81.81551020408163
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TYER
          value: '2012'
        - id: TALB
          value: Extender
        - id: TRCK
          value: '2'
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: I Want Your Soul
    quality:
      warnings: []
    common:
      track:
        'no': 2
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2012
      album: Extender
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: I Want Your Soul
    transformed:
      ID3v2.3:
        TCON: Electronic
        TYER: '2012'
        TALB: Extender
        TRCK: '2'
        TPE1: Panix Hilton
        TIT2: I Want Your Soul
    all:
      TCON: Electronic
      TYER: '2012'
      TALB: Extender
      TRCK: '2'
      TPE1: Panix Hilton
      TIT2: I Want Your Soul
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/extender/3-kim-ska-vi-koepa-lim-.mp3
    audio: /assets/albums/extender/3-kim-ska-vi-koepa-lim-.mp3
    slug: extender/3-kim-ska-vi-koepa-lim-
    albumSlug: extender
    trackSlug: 3-kim-ska-vi-koepa-lim-
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/extender/3-kim-ska-vi-koepa-lim-.jpeg
    cover: /assets/albums/extender/3-kim-ska-vi-koepa-lim-.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 85.62938775510204
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TYER
          value: '2012'
        - id: TALB
          value: Extender
        - id: TRCK
          value: '3'
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: Kim Ska Vi Koepa Lim?
    quality:
      warnings: []
    common:
      track:
        'no': 3
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2012
      album: Extender
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: Kim Ska Vi Koepa Lim?
    transformed:
      ID3v2.3:
        TCON: Electronic
        TYER: '2012'
        TALB: Extender
        TRCK: '3'
        TPE1: Panix Hilton
        TIT2: Kim Ska Vi Koepa Lim?
    all:
      TCON: Electronic
      TYER: '2012'
      TALB: Extender
      TRCK: '3'
      TPE1: Panix Hilton
      TIT2: Kim Ska Vi Koepa Lim?
  - path: /mnt/usb32gb/organized-pax-nion/assets/albums/extender/4-eru-sejjoez-.mp3
    audio: /assets/albums/extender/4-eru-sejjoez-.mp3
    slug: extender/4-eru-sejjoez-
    albumSlug: extender
    trackSlug: 4-eru-sejjoez-
    coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/extender/4-eru-sejjoez-.jpeg
    cover: /assets/albums/extender/4-eru-sejjoez-.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 118.93551020408164
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TYER
          value: '2012'
        - id: TALB
          value: Extender
        - id: TRCK
          value: '4'
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: Eru sejjoez?
    quality:
      warnings: []
    common:
      track:
        'no': 4
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2012
      album: Extender
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: Eru sejjoez?
    transformed:
      ID3v2.3:
        TCON: Electronic
        TYER: '2012'
        TALB: Extender
        TRCK: '4'
        TPE1: Panix Hilton
        TIT2: Eru sejjoez?
    all:
      TCON: Electronic
      TYER: '2012'
      TALB: Extender
      TRCK: '4'
      TPE1: Panix Hilton
      TIT2: Eru sejjoez?
  - path: /mnt/usb32gb/organized-pax-nion/assets/albums/extender/5-organized.mp3
    audio: /assets/albums/extender/5-organized.mp3
    slug: extender/5-organized
    albumSlug: extender
    trackSlug: 5-organized
    coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/extender/5-organized.jpeg
    cover: /assets/albums/extender/5-organized.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 76.12081632653062
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TYER
          value: '2012'
        - id: TALB
          value: Extender
        - id: TRCK
          value: '5'
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: Organized
    quality:
      warnings: []
    common:
      track:
        'no': 5
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2012
      album: Extender
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: Organized
    transformed:
      ID3v2.3:
        TCON: Electronic
        TYER: '2012'
        TALB: Extender
        TRCK: '5'
        TPE1: Panix Hilton
        TIT2: Organized
    all:
      TCON: Electronic
      TYER: '2012'
      TALB: Extender
      TRCK: '5'
      TPE1: Panix Hilton
      TIT2: Organized
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/extender/6-matter-of-opinion.mp3
    audio: /assets/albums/extender/6-matter-of-opinion.mp3
    slug: extender/6-matter-of-opinion
    albumSlug: extender
    trackSlug: 6-matter-of-opinion
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/extender/6-matter-of-opinion.jpeg
    cover: /assets/albums/extender/6-matter-of-opinion.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME 3.98.4
      duration: 81.81551020408163
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TYER
          value: '2012'
        - id: TALB
          value: Extender
        - id: TRCK
          value: '6'
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: Matter Of Opinion
    quality:
      warnings: []
    common:
      track:
        'no': 6
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2012
      album: Extender
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: Matter Of Opinion
    transformed:
      ID3v2.3:
        TCON: Electronic
        TYER: '2012'
        TALB: Extender
        TRCK: '6'
        TPE1: Panix Hilton
        TIT2: Matter Of Opinion
    all:
      TCON: Electronic
      TYER: '2012'
      TALB: Extender
      TRCK: '6'
      TPE1: Panix Hilton
      TIT2: Matter Of Opinion
---
