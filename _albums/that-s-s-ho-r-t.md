---
layout: album
slug: that-s-s-ho-r-t
name: That's (s)Ho(r)t
artists: Panix Hilton
bitrate: 320000
trackCount: 4
cover: /assets/albums/that-s-s-ho-r-t/1-panic-addicts.jpeg
date: 2011-1-1
tracks:
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/that-s-s-ho-r-t/1-panic-addicts.mp3
    audio: /assets/albums/that-s-s-ho-r-t/1-panic-addicts.mp3
    slug: that-s-s-ho-r-t/1-panic-addicts
    albumSlug: that-s-s-ho-r-t
    trackSlug: 1-panic-addicts
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/that-s-s-ho-r-t/1-panic-addicts.jpeg
    cover: /assets/albums/that-s-s-ho-r-t/1-panic-addicts.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME 3.98.4
      duration: 131.34367346938777
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TALB
          value: That's (s)Ho(r)t
        - id: TRCK
          value: '1'
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: Panic Addicts
        - id: TYER
          value: '2011'
    quality:
      warnings: []
    common:
      track:
        'no': 1
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      album: That's (s)Ho(r)t
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: Panic Addicts
      year: 2011
    transformed:
      ID3v2.3:
        TCON: Electronic
        TALB: That's (s)Ho(r)t
        TRCK: '1'
        TPE1: Panix Hilton
        TIT2: Panic Addicts
        TYER: '2011'
    all:
      TCON: Electronic
      TALB: That's (s)Ho(r)t
      TRCK: '1'
      TPE1: Panix Hilton
      TIT2: Panic Addicts
      TYER: '2011'
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/that-s-s-ho-r-t/2-systembolag.mp3
    audio: /assets/albums/that-s-s-ho-r-t/2-systembolag.mp3
    slug: that-s-s-ho-r-t/2-systembolag
    albumSlug: that-s-s-ho-r-t
    trackSlug: 2-systembolag
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/that-s-s-ho-r-t/2-systembolag.jpeg
    cover: /assets/albums/that-s-s-ho-r-t/2-systembolag.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 127.52979591836734
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TALB
          value: That's (s)Ho(r)t
        - id: TRCK
          value: '2'
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: Systembolag
        - id: TYER
          value: '2011'
    quality:
      warnings: []
    common:
      track:
        'no': 2
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      album: That's (s)Ho(r)t
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: Systembolag
      year: 2011
    transformed:
      ID3v2.3:
        TCON: Electronic
        TALB: That's (s)Ho(r)t
        TRCK: '2'
        TPE1: Panix Hilton
        TIT2: Systembolag
        TYER: '2011'
    all:
      TCON: Electronic
      TALB: That's (s)Ho(r)t
      TRCK: '2'
      TPE1: Panix Hilton
      TIT2: Systembolag
      TYER: '2011'
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/that-s-s-ho-r-t/3-tunnelbanan.mp3
    audio: /assets/albums/that-s-s-ho-r-t/3-tunnelbanan.mp3
    slug: that-s-s-ho-r-t/3-tunnelbanan
    albumSlug: that-s-s-ho-r-t
    trackSlug: 3-tunnelbanan
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/that-s-s-ho-r-t/3-tunnelbanan.jpeg
    cover: /assets/albums/that-s-s-ho-r-t/3-tunnelbanan.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 114.20734693877552
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TALB
          value: That's (s)Ho(r)t
        - id: TRCK
          value: '3'
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: Tunnelbanan
        - id: TYER
          value: '2011'
    quality:
      warnings: []
    common:
      track:
        'no': 3
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      album: That's (s)Ho(r)t
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: Tunnelbanan
      year: 2011
    transformed:
      ID3v2.3:
        TCON: Electronic
        TALB: That's (s)Ho(r)t
        TRCK: '3'
        TPE1: Panix Hilton
        TIT2: Tunnelbanan
        TYER: '2011'
    all:
      TCON: Electronic
      TALB: That's (s)Ho(r)t
      TRCK: '3'
      TPE1: Panix Hilton
      TIT2: Tunnelbanan
      TYER: '2011'
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/that-s-s-ho-r-t/4-rootmos.mp3
    audio: /assets/albums/that-s-s-ho-r-t/4-rootmos.mp3
    slug: that-s-s-ho-r-t/4-rootmos
    albumSlug: that-s-s-ho-r-t
    trackSlug: 4-rootmos
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/that-s-s-ho-r-t/4-rootmos.jpeg
    cover: /assets/albums/that-s-s-ho-r-t/4-rootmos.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 95.92163265306122
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TALB
          value: That's (s)Ho(r)t
        - id: TRCK
          value: '4'
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: Rootmos
        - id: TYER
          value: '2011'
    quality:
      warnings: []
    common:
      track:
        'no': 4
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      album: That's (s)Ho(r)t
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: Rootmos
      year: 2011
    transformed:
      ID3v2.3:
        TCON: Electronic
        TALB: That's (s)Ho(r)t
        TRCK: '4'
        TPE1: Panix Hilton
        TIT2: Rootmos
        TYER: '2011'
    all:
      TCON: Electronic
      TALB: That's (s)Ho(r)t
      TRCK: '4'
      TPE1: Panix Hilton
      TIT2: Rootmos
      TYER: '2011'
---
