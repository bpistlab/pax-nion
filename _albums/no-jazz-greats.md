---
layout: album
slug: no-jazz-greats
name: No Jazz Greats
artists: Panix Hilton
bitrate: 320000
trackCount: 8
cover: /assets/albums/no-jazz-greats/1-forever-dusted.jpeg
date: 2013-1-1
tracks:
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/no-jazz-greats/1-forever-dusted.mp3
    audio: /assets/albums/no-jazz-greats/1-forever-dusted.mp3
    slug: no-jazz-greats/1-forever-dusted
    albumSlug: no-jazz-greats
    trackSlug: 1-forever-dusted
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/no-jazz-greats/1-forever-dusted.jpeg
    cover: /assets/albums/no-jazz-greats/1-forever-dusted.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 112.3004081632653
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TYER
          value: '2013'
        - id: TALB
          value: No Jazz Greats
        - id: TRCK
          value: '1'
        - id: TIT2
          value: Forever Dusted
        - id: TPE1
          value: Panix Hilton
    quality:
      warnings: []
    common:
      track:
        'no': 1
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2013
      album: No Jazz Greats
      title: Forever Dusted
      artists:
        - Panix Hilton
      artist: Panix Hilton
    transformed:
      ID3v2.3:
        TCON: Electronic
        TYER: '2013'
        TALB: No Jazz Greats
        TRCK: '1'
        TIT2: Forever Dusted
        TPE1: Panix Hilton
    all:
      TCON: Electronic
      TYER: '2013'
      TALB: No Jazz Greats
      TRCK: '1'
      TIT2: Forever Dusted
      TPE1: Panix Hilton
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/no-jazz-greats/2-vitbrusarn.mp3
    audio: /assets/albums/no-jazz-greats/2-vitbrusarn.mp3
    slug: no-jazz-greats/2-vitbrusarn
    albumSlug: no-jazz-greats
    trackSlug: 2-vitbrusarn
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/no-jazz-greats/2-vitbrusarn.jpeg
    cover: /assets/albums/no-jazz-greats/2-vitbrusarn.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 87.53632653061224
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TYER
          value: '2013'
        - id: TALB
          value: No Jazz Greats
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: Vitbrusarn
        - id: TRCK
          value: '2'
    quality:
      warnings: []
    common:
      track:
        'no': 2
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2013
      album: No Jazz Greats
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: Vitbrusarn
    transformed:
      ID3v2.3:
        TCON: Electronic
        TYER: '2013'
        TALB: No Jazz Greats
        TPE1: Panix Hilton
        TIT2: Vitbrusarn
        TRCK: '2'
    all:
      TCON: Electronic
      TYER: '2013'
      TALB: No Jazz Greats
      TPE1: Panix Hilton
      TIT2: Vitbrusarn
      TRCK: '2'
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/no-jazz-greats/3-pianamas.mp3
    audio: /assets/albums/no-jazz-greats/3-pianamas.mp3
    slug: no-jazz-greats/3-pianamas
    albumSlug: no-jazz-greats
    trackSlug: 3-pianamas
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/no-jazz-greats/3-pianamas.jpeg
    cover: /assets/albums/no-jazz-greats/3-pianamas.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 48000
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.99r
      duration: 116.232
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TYER
          value: '2013'
        - id: TALB
          value: No Jazz Greats
        - id: TRCK
          value: '3'
        - id: TIT2
          value: Pianamas
        - id: TPE1
          value: Panix Hilton
    quality:
      warnings: []
    common:
      track:
        'no': 3
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2013
      album: No Jazz Greats
      title: Pianamas
      artists:
        - Panix Hilton
      artist: Panix Hilton
    transformed:
      ID3v2.3:
        TCON: Electronic
        TYER: '2013'
        TALB: No Jazz Greats
        TRCK: '3'
        TIT2: Pianamas
        TPE1: Panix Hilton
    all:
      TCON: Electronic
      TYER: '2013'
      TALB: No Jazz Greats
      TRCK: '3'
      TIT2: Pianamas
      TPE1: Panix Hilton
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/no-jazz-greats/4-y-u-l-e-o-.mp3
    audio: /assets/albums/no-jazz-greats/4-y-u-l-e-o-.mp3
    slug: no-jazz-greats/4-y-u-l-e-o-
    albumSlug: no-jazz-greats
    trackSlug: 4-y-u-l-e-o-
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/no-jazz-greats/4-y-u-l-e-o-.jpeg
    cover: /assets/albums/no-jazz-greats/4-y-u-l-e-o-.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 48000
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.99r
      duration: 99.072
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TYER
          value: '2012'
        - id: TALB
          value: No Jazz Greats
        - id: TRCK
          value: '4'
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: Y.U.L.E.O.
    quality:
      warnings: []
    common:
      track:
        'no': 4
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2012
      album: No Jazz Greats
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: Y.U.L.E.O.
    transformed:
      ID3v2.3:
        TCON: Electronic
        TYER: '2012'
        TALB: No Jazz Greats
        TRCK: '4'
        TPE1: Panix Hilton
        TIT2: Y.U.L.E.O.
    all:
      TCON: Electronic
      TYER: '2012'
      TALB: No Jazz Greats
      TRCK: '4'
      TPE1: Panix Hilton
      TIT2: Y.U.L.E.O.
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/no-jazz-greats/5-all-my-days.mp3
    audio: /assets/albums/no-jazz-greats/5-all-my-days.mp3
    slug: no-jazz-greats/5-all-my-days
    albumSlug: no-jazz-greats
    trackSlug: 5-all-my-days
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/no-jazz-greats/5-all-my-days.jpeg
    cover: /assets/albums/no-jazz-greats/5-all-my-days.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 116.11428571428571
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TYER
          value: '2013'
        - id: TALB
          value: No Jazz Greats
        - id: TRCK
          value: '5'
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: All My Days
    quality:
      warnings: []
    common:
      track:
        'no': 5
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2013
      album: No Jazz Greats
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: All My Days
    transformed:
      ID3v2.3:
        TCON: Electronic
        TYER: '2013'
        TALB: No Jazz Greats
        TRCK: '5'
        TPE1: Panix Hilton
        TIT2: All My Days
    all:
      TCON: Electronic
      TYER: '2013'
      TALB: No Jazz Greats
      TRCK: '5'
      TPE1: Panix Hilton
      TIT2: All My Days
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/no-jazz-greats/6-2-tired-least-fave-kickdrum-shortmix-.mp3
    audio: /assets/albums/no-jazz-greats/6-2-tired-least-fave-kickdrum-shortmix-.mp3
    slug: no-jazz-greats/6-2-tired-least-fave-kickdrum-shortmix-
    albumSlug: no-jazz-greats
    trackSlug: 6-2-tired-least-fave-kickdrum-shortmix-
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/no-jazz-greats/6-2-tired-least-fave-kickdrum-shortmix-.jpeg
    cover: /assets/albums/no-jazz-greats/6-2-tired-least-fave-kickdrum-shortmix-.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.99r
      duration: 106.71020408163265
    native:
      ID3v2.3:
        - id: TRCK
          value: '6'
        - id: TCON
          value: Electronic
        - id: TYER
          value: '2013'
        - id: TALB
          value: No Jazz Greats
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: 2 Tired (Least fave kickdrum shortmix)
    quality:
      warnings: []
    common:
      track:
        'no': 6
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2013
      album: No Jazz Greats
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: 2 Tired (Least fave kickdrum shortmix)
    transformed:
      ID3v2.3:
        TRCK: '6'
        TCON: Electronic
        TYER: '2013'
        TALB: No Jazz Greats
        TPE1: Panix Hilton
        TIT2: 2 Tired (Least fave kickdrum shortmix)
    all:
      TRCK: '6'
      TCON: Electronic
      TYER: '2013'
      TALB: No Jazz Greats
      TPE1: Panix Hilton
      TIT2: 2 Tired (Least fave kickdrum shortmix)
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/no-jazz-greats/7-sleepers-ain-t-keepers.mp3
    audio: /assets/albums/no-jazz-greats/7-sleepers-ain-t-keepers.mp3
    slug: no-jazz-greats/7-sleepers-ain-t-keepers
    albumSlug: no-jazz-greats
    trackSlug: 7-sleepers-ain-t-keepers
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/no-jazz-greats/7-sleepers-ain-t-keepers.jpeg
    cover: /assets/albums/no-jazz-greats/7-sleepers-ain-t-keepers.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 48000
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.99r
      duration: 85.752
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TYER
          value: '2013'
        - id: TALB
          value: No Jazz Greats
        - id: TRCK
          value: '7'
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: Sleepers Ain't Keepers
    quality:
      warnings: []
    common:
      track:
        'no': 7
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2013
      album: No Jazz Greats
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: Sleepers Ain't Keepers
    transformed:
      ID3v2.3:
        TCON: Electronic
        TYER: '2013'
        TALB: No Jazz Greats
        TRCK: '7'
        TPE1: Panix Hilton
        TIT2: Sleepers Ain't Keepers
    all:
      TCON: Electronic
      TYER: '2013'
      TALB: No Jazz Greats
      TRCK: '7'
      TPE1: Panix Hilton
      TIT2: Sleepers Ain't Keepers
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/no-jazz-greats/8-bonus-beats.mp3
    audio: /assets/albums/no-jazz-greats/8-bonus-beats.mp3
    slug: no-jazz-greats/8-bonus-beats
    albumSlug: no-jazz-greats
    trackSlug: 8-bonus-beats
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/no-jazz-greats/8-bonus-beats.jpeg
    cover: /assets/albums/no-jazz-greats/8-bonus-beats.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 248.32
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TYER
          value: '2013'
        - id: TALB
          value: No Jazz Greats
        - id: TRCK
          value: '8'
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: Bonus Beats
    quality:
      warnings: []
    common:
      track:
        'no': 8
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2013
      album: No Jazz Greats
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: Bonus Beats
    transformed:
      ID3v2.3:
        TCON: Electronic
        TYER: '2013'
        TALB: No Jazz Greats
        TRCK: '8'
        TPE1: Panix Hilton
        TIT2: Bonus Beats
    all:
      TCON: Electronic
      TYER: '2013'
      TALB: No Jazz Greats
      TRCK: '8'
      TPE1: Panix Hilton
      TIT2: Bonus Beats
---
