---
layout: album
slug: morning-moods
name: Morning Moods
artists: Panix Hilton
bitrate: 320000
trackCount: 7
cover: /assets/albums/morning-moods/7-redemption.jpeg
date: 2012-1-1
tracks:
  - path: /mnt/usb32gb/organized-pax-nion/assets/albums/morning-moods/1-inhale.mp3
    audio: /assets/albums/morning-moods/1-inhale.mp3
    slug: morning-moods/1-inhale
    albumSlug: morning-moods
    trackSlug: 1-inhale
    coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/morning-moods/1-inhale.jpeg
    cover: /assets/albums/morning-moods/1-inhale.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 87.53632653061224
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TYER
          value: '2012'
        - id: TALB
          value: Morning Moods
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: Inhale
        - id: TRCK
          value: '1'
    quality:
      warnings: []
    common:
      track:
        'no': 1
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2012
      album: Morning Moods
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: Inhale
    transformed:
      ID3v2.3:
        TCON: Electronic
        TYER: '2012'
        TALB: Morning Moods
        TPE1: Panix Hilton
        TIT2: Inhale
        TRCK: '1'
    all:
      TCON: Electronic
      TYER: '2012'
      TALB: Morning Moods
      TPE1: Panix Hilton
      TIT2: Inhale
      TRCK: '1'
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/morning-moods/2-atom-leaves.mp3
    audio: /assets/albums/morning-moods/2-atom-leaves.mp3
    slug: morning-moods/2-atom-leaves
    albumSlug: morning-moods
    trackSlug: 2-atom-leaves
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/morning-moods/2-atom-leaves.jpeg
    cover: /assets/albums/morning-moods/2-atom-leaves.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME 3.98.4
      duration: 146.59918367346938
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TYER
          value: '2012'
        - id: TRCK
          value: '2'
        - id: TALB
          value: Morning Moods
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: Atom Leaves
    quality:
      warnings: []
    common:
      track:
        'no': 2
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2012
      album: Morning Moods
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: Atom Leaves
    transformed:
      ID3v2.3:
        TCON: Electronic
        TYER: '2012'
        TRCK: '2'
        TALB: Morning Moods
        TPE1: Panix Hilton
        TIT2: Atom Leaves
    all:
      TCON: Electronic
      TYER: '2012'
      TRCK: '2'
      TALB: Morning Moods
      TPE1: Panix Hilton
      TIT2: Atom Leaves
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/morning-moods/3-holy-mother-of-bread.mp3
    audio: /assets/albums/morning-moods/3-holy-mother-of-bread.mp3
    slug: morning-moods/3-holy-mother-of-bread
    albumSlug: morning-moods
    trackSlug: 3-holy-mother-of-bread
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/morning-moods/3-holy-mother-of-bread.jpeg
    cover: /assets/albums/morning-moods/3-holy-mother-of-bread.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 118.0734693877551
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TYER
          value: '2012'
        - id: TRCK
          value: '3'
        - id: TALB
          value: Morning Moods
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: Holy Mother Of Bread
    quality:
      warnings: []
    common:
      track:
        'no': 3
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2012
      album: Morning Moods
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: Holy Mother Of Bread
    transformed:
      ID3v2.3:
        TCON: Electronic
        TYER: '2012'
        TRCK: '3'
        TALB: Morning Moods
        TPE1: Panix Hilton
        TIT2: Holy Mother Of Bread
    all:
      TCON: Electronic
      TYER: '2012'
      TRCK: '3'
      TALB: Morning Moods
      TPE1: Panix Hilton
      TIT2: Holy Mother Of Bread
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/morning-moods/4-humorn-p-topp.mp3
    audio: /assets/albums/morning-moods/4-humorn-p-topp.mp3
    slug: morning-moods/4-humorn-p-topp
    albumSlug: morning-moods
    trackSlug: 4-humorn-p-topp
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/morning-moods/4-humorn-p-topp.jpeg
    cover: /assets/albums/morning-moods/4-humorn-p-topp.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 112.3004081632653
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TYER
          value: '2012'
        - id: TALB
          value: Morning Moods
        - id: TRCK
          value: '4'
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: Humorn På Topp
    quality:
      warnings: []
    common:
      track:
        'no': 4
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2012
      album: Morning Moods
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: Humorn På Topp
    transformed:
      ID3v2.3:
        TCON: Electronic
        TYER: '2012'
        TALB: Morning Moods
        TRCK: '4'
        TPE1: Panix Hilton
        TIT2: Humorn På Topp
    all:
      TCON: Electronic
      TYER: '2012'
      TALB: Morning Moods
      TRCK: '4'
      TPE1: Panix Hilton
      TIT2: Humorn På Topp
  - path: /mnt/usb32gb/organized-pax-nion/assets/albums/morning-moods/5-roller.mp3
    audio: /assets/albums/morning-moods/5-roller.mp3
    slug: morning-moods/5-roller
    albumSlug: morning-moods
    trackSlug: 5-roller
    coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/morning-moods/5-roller.jpeg
    cover: /assets/albums/morning-moods/5-roller.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 95.16408163265307
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TYER
          value: '2012'
        - id: TALB
          value: Morning Moods
        - id: TRCK
          value: '5'
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: Roller
    quality:
      warnings: []
    common:
      track:
        'no': 5
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2012
      album: Morning Moods
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: Roller
    transformed:
      ID3v2.3:
        TCON: Electronic
        TYER: '2012'
        TALB: Morning Moods
        TRCK: '5'
        TPE1: Panix Hilton
        TIT2: Roller
    all:
      TCON: Electronic
      TYER: '2012'
      TALB: Morning Moods
      TRCK: '5'
      TPE1: Panix Hilton
      TIT2: Roller
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/morning-moods/6-nejuschfy.mp3
    audio: /assets/albums/morning-moods/6-nejuschfy.mp3
    slug: morning-moods/6-nejuschfy
    albumSlug: morning-moods
    trackSlug: 6-nejuschfy
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/morning-moods/6-nejuschfy.jpeg
    cover: /assets/albums/morning-moods/6-nejuschfy.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME 3.98.4
      duration: 190.4065306122449
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TYER
          value: '2012'
        - id: TALB
          value: Morning Moods
        - id: TRCK
          value: '6'
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: Nejuschfy
    quality:
      warnings: []
    common:
      track:
        'no': 6
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2012
      album: Morning Moods
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: Nejuschfy
    transformed:
      ID3v2.3:
        TCON: Electronic
        TYER: '2012'
        TALB: Morning Moods
        TRCK: '6'
        TPE1: Panix Hilton
        TIT2: Nejuschfy
    all:
      TCON: Electronic
      TYER: '2012'
      TALB: Morning Moods
      TRCK: '6'
      TPE1: Panix Hilton
      TIT2: Nejuschfy
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/morning-moods/7-redemption.mp3
    audio: /assets/albums/morning-moods/7-redemption.mp3
    slug: morning-moods/7-redemption
    albumSlug: morning-moods
    trackSlug: 7-redemption
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/morning-moods/7-redemption.jpeg
    cover: /assets/albums/morning-moods/7-redemption.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 129.43673469387755
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TALB
          value: Morning Moods
        - id: TRCK
          value: '7'
        - id: TIT2
          value: Redemption
        - id: TPE1
          value: Panix Hilton
        - id: TYER
          value: '2012'
    quality:
      warnings: []
    common:
      track:
        'no': 7
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      album: Morning Moods
      title: Redemption
      artists:
        - Panix Hilton
      artist: Panix Hilton
      year: 2012
    transformed:
      ID3v2.3:
        TCON: Electronic
        TALB: Morning Moods
        TRCK: '7'
        TIT2: Redemption
        TPE1: Panix Hilton
        TYER: '2012'
    all:
      TCON: Electronic
      TALB: Morning Moods
      TRCK: '7'
      TIT2: Redemption
      TPE1: Panix Hilton
      TYER: '2012'
---
