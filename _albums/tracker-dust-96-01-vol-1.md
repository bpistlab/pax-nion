---
layout: album
slug: tracker-dust-96-01-vol-1
name: Tracker Dust 96-01 Vol 1
artists: Panix
bitrate:
  - 256000
  - 320000
  - 112000
trackCount: 10
cover: /assets/albums/tracker-dust-96-01-vol-1/1-cv001.jpeg
date: 2005-1-1
tracks:
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/tracker-dust-96-01-vol-1/1-cv001.mp3
    audio: /assets/albums/tracker-dust-96-01-vol-1/1-cv001.mp3
    slug: tracker-dust-96-01-vol-1/1-cv001
    albumSlug: tracker-dust-96-01-vol-1
    trackSlug: 1-cv001
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/tracker-dust-96-01-vol-1/1-cv001.jpeg
    cover: /assets/albums/tracker-dust-96-01-vol-1/1-cv001.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 1
      bitrate: 256000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 147.1738775510204
    native:
      ID3v2.3:
        - id: TRCK
          value: '1'
        - id: TCON
          value: Electronic
        - id: TALB
          value: Tracker Dust 96-01 Vol 1
        - id: TPE1
          value: Panix
        - id: TIT2
          value: CV001
        - id: TYER
          value: '2005'
    quality:
      warnings: []
    common:
      track:
        'no': 1
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      album: Tracker Dust 96-01 Vol 1
      artists:
        - Panix
      artist: Panix
      title: CV001
      year: 2005
    transformed:
      ID3v2.3:
        TRCK: '1'
        TCON: Electronic
        TALB: Tracker Dust 96-01 Vol 1
        TPE1: Panix
        TIT2: CV001
        TYER: '2005'
    all:
      TRCK: '1'
      TCON: Electronic
      TALB: Tracker Dust 96-01 Vol 1
      TPE1: Panix
      TIT2: CV001
      TYER: '2005'
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/tracker-dust-96-01-vol-1/2-cv002.mp3
    audio: /assets/albums/tracker-dust-96-01-vol-1/2-cv002.mp3
    slug: tracker-dust-96-01-vol-1/2-cv002
    albumSlug: tracker-dust-96-01-vol-1
    trackSlug: 2-cv002
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/tracker-dust-96-01-vol-1/2-cv002.jpeg
    cover: /assets/albums/tracker-dust-96-01-vol-1/2-cv002.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 1
      bitrate: 256000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 38.50448979591837
    native:
      ID3v2.3:
        - id: TRCK
          value: '2'
        - id: TCON
          value: Electronic
        - id: TALB
          value: Tracker Dust 96-01 Vol 1
        - id: TPE1
          value: Panix
        - id: TIT2
          value: CV002
        - id: TYER
          value: '2005'
    quality:
      warnings: []
    common:
      track:
        'no': 2
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      album: Tracker Dust 96-01 Vol 1
      artists:
        - Panix
      artist: Panix
      title: CV002
      year: 2005
    transformed:
      ID3v2.3:
        TRCK: '2'
        TCON: Electronic
        TALB: Tracker Dust 96-01 Vol 1
        TPE1: Panix
        TIT2: CV002
        TYER: '2005'
    all:
      TRCK: '2'
      TCON: Electronic
      TALB: Tracker Dust 96-01 Vol 1
      TPE1: Panix
      TIT2: CV002
      TYER: '2005'
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/tracker-dust-96-01-vol-1/3-cv003.mp3
    audio: /assets/albums/tracker-dust-96-01-vol-1/3-cv003.mp3
    slug: tracker-dust-96-01-vol-1/3-cv003
    albumSlug: tracker-dust-96-01-vol-1
    trackSlug: 3-cv003
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/tracker-dust-96-01-vol-1/3-cv003.jpeg
    cover: /assets/albums/tracker-dust-96-01-vol-1/3-cv003.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 1
      bitrate: 256000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 144.24816326530612
    native:
      ID3v2.3:
        - id: TRCK
          value: '3'
        - id: TALB
          value: Tracker Dust 96-01 Vol 1
        - id: TPE1
          value: Panix
        - id: TIT2
          value: CV003
        - id: TCON
          value: Electronic
        - id: TYER
          value: '2005'
    quality:
      warnings: []
    common:
      track:
        'no': 3
        of: null
      disk:
        'no': null
        of: null
      album: Tracker Dust 96-01 Vol 1
      artists:
        - Panix
      artist: Panix
      title: CV003
      genre:
        - Electronic
      year: 2005
    transformed:
      ID3v2.3:
        TRCK: '3'
        TALB: Tracker Dust 96-01 Vol 1
        TPE1: Panix
        TIT2: CV003
        TCON: Electronic
        TYER: '2005'
    all:
      TRCK: '3'
      TALB: Tracker Dust 96-01 Vol 1
      TPE1: Panix
      TIT2: CV003
      TCON: Electronic
      TYER: '2005'
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/tracker-dust-96-01-vol-1/4-husnix.mp3
    audio: /assets/albums/tracker-dust-96-01-vol-1/4-husnix.mp3
    slug: tracker-dust-96-01-vol-1/4-husnix
    albumSlug: tracker-dust-96-01-vol-1
    trackSlug: 4-husnix
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/tracker-dust-96-01-vol-1/4-husnix.jpeg
    cover: /assets/albums/tracker-dust-96-01-vol-1/4-husnix.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 1
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 177.8677551020408
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TALB
          value: Tracker Dust 96-01 Vol 1
        - id: TRCK
          value: '4'
        - id: TPE1
          value: Panix
        - id: TIT2
          value: Husnix
        - id: TYER
          value: '2005'
    quality:
      warnings: []
    common:
      track:
        'no': 4
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      album: Tracker Dust 96-01 Vol 1
      artists:
        - Panix
      artist: Panix
      title: Husnix
      year: 2005
    transformed:
      ID3v2.3:
        TCON: Electronic
        TALB: Tracker Dust 96-01 Vol 1
        TRCK: '4'
        TPE1: Panix
        TIT2: Husnix
        TYER: '2005'
    all:
      TCON: Electronic
      TALB: Tracker Dust 96-01 Vol 1
      TRCK: '4'
      TPE1: Panix
      TIT2: Husnix
      TYER: '2005'
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/tracker-dust-96-01-vol-1/5-crazed.mp3
    audio: /assets/albums/tracker-dust-96-01-vol-1/5-crazed.mp3
    slug: tracker-dust-96-01-vol-1/5-crazed
    albumSlug: tracker-dust-96-01-vol-1
    trackSlug: 5-crazed
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/tracker-dust-96-01-vol-1/5-crazed.jpeg
    cover: /assets/albums/tracker-dust-96-01-vol-1/5-crazed.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 1
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 291.5526530612245
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TALB
          value: Tracker Dust 96-01 Vol 1
        - id: TRCK
          value: '5'
        - id: TIT2
          value: Crazed
        - id: TPE1
          value: Panix
        - id: TYER
          value: '2005'
    quality:
      warnings: []
    common:
      track:
        'no': 5
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      album: Tracker Dust 96-01 Vol 1
      title: Crazed
      artists:
        - Panix
      artist: Panix
      year: 2005
    transformed:
      ID3v2.3:
        TCON: Electronic
        TALB: Tracker Dust 96-01 Vol 1
        TRCK: '5'
        TIT2: Crazed
        TPE1: Panix
        TYER: '2005'
    all:
      TCON: Electronic
      TALB: Tracker Dust 96-01 Vol 1
      TRCK: '5'
      TIT2: Crazed
      TPE1: Panix
      TYER: '2005'
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/tracker-dust-96-01-vol-1/6-hipphappbitzyoyozwei.mp3
    audio: /assets/albums/tracker-dust-96-01-vol-1/6-hipphappbitzyoyozwei.mp3
    slug: tracker-dust-96-01-vol-1/6-hipphappbitzyoyozwei
    albumSlug: tracker-dust-96-01-vol-1
    trackSlug: 6-hipphappbitzyoyozwei
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/tracker-dust-96-01-vol-1/6-hipphappbitzyoyozwei.jpeg
    cover: /assets/albums/tracker-dust-96-01-vol-1/6-hipphappbitzyoyozwei.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 1
      bitrate: 112000
      codecProfile: CBR
      numberOfSamples: 3384576
      duration: 76.74775510204081
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TALB
          value: Tracker Dust 96-01 Vol 1
        - id: TRCK
          value: '6'
        - id: TPE1
          value: Panix
        - id: TIT2
          value: Hipphappbitzyoyozwei
        - id: TYER
          value: '2005'
    quality:
      warnings: []
    common:
      track:
        'no': 6
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      album: Tracker Dust 96-01 Vol 1
      artists:
        - Panix
      artist: Panix
      title: Hipphappbitzyoyozwei
      year: 2005
    transformed:
      ID3v2.3:
        TCON: Electronic
        TALB: Tracker Dust 96-01 Vol 1
        TRCK: '6'
        TPE1: Panix
        TIT2: Hipphappbitzyoyozwei
        TYER: '2005'
    all:
      TCON: Electronic
      TALB: Tracker Dust 96-01 Vol 1
      TRCK: '6'
      TPE1: Panix
      TIT2: Hipphappbitzyoyozwei
      TYER: '2005'
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/tracker-dust-96-01-vol-1/7-de-kuyper.mp3
    audio: /assets/albums/tracker-dust-96-01-vol-1/7-de-kuyper.mp3
    slug: tracker-dust-96-01-vol-1/7-de-kuyper
    albumSlug: tracker-dust-96-01-vol-1
    trackSlug: 7-de-kuyper
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/tracker-dust-96-01-vol-1/7-de-kuyper.jpeg
    cover: /assets/albums/tracker-dust-96-01-vol-1/7-de-kuyper.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 1
      bitrate: 256000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 112.90122448979592
    native:
      ID3v2.3:
        - id: TALB
          value: Tracker Dust 96-01 Vol 1
        - id: TRCK
          value: '7'
        - id: TCON
          value: Electronic
        - id: TPE1
          value: Panix
        - id: TIT2
          value: De Kuyper
        - id: TYER
          value: '2005'
    quality:
      warnings: []
    common:
      track:
        'no': 7
        of: null
      disk:
        'no': null
        of: null
      album: Tracker Dust 96-01 Vol 1
      genre:
        - Electronic
      artists:
        - Panix
      artist: Panix
      title: De Kuyper
      year: 2005
    transformed:
      ID3v2.3:
        TALB: Tracker Dust 96-01 Vol 1
        TRCK: '7'
        TCON: Electronic
        TPE1: Panix
        TIT2: De Kuyper
        TYER: '2005'
    all:
      TALB: Tracker Dust 96-01 Vol 1
      TRCK: '7'
      TCON: Electronic
      TPE1: Panix
      TIT2: De Kuyper
      TYER: '2005'
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/tracker-dust-96-01-vol-1/8-future.mp3
    audio: /assets/albums/tracker-dust-96-01-vol-1/8-future.mp3
    slug: tracker-dust-96-01-vol-1/8-future
    albumSlug: tracker-dust-96-01-vol-1
    trackSlug: 8-future
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/tracker-dust-96-01-vol-1/8-future.jpeg
    cover: /assets/albums/tracker-dust-96-01-vol-1/8-future.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 1
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 195.10857142857142
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TALB
          value: Tracker Dust 96-01 Vol 1
        - id: TRCK
          value: '8'
        - id: TPE1
          value: Panix
        - id: TIT2
          value: Future
        - id: TYER
          value: '2005'
    quality:
      warnings: []
    common:
      track:
        'no': 8
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      album: Tracker Dust 96-01 Vol 1
      artists:
        - Panix
      artist: Panix
      title: Future
      year: 2005
    transformed:
      ID3v2.3:
        TCON: Electronic
        TALB: Tracker Dust 96-01 Vol 1
        TRCK: '8'
        TPE1: Panix
        TIT2: Future
        TYER: '2005'
    all:
      TCON: Electronic
      TALB: Tracker Dust 96-01 Vol 1
      TRCK: '8'
      TPE1: Panix
      TIT2: Future
      TYER: '2005'
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/tracker-dust-96-01-vol-1/9-minichip-eins.mp3
    audio: /assets/albums/tracker-dust-96-01-vol-1/9-minichip-eins.mp3
    slug: tracker-dust-96-01-vol-1/9-minichip-eins
    albumSlug: tracker-dust-96-01-vol-1
    trackSlug: 9-minichip-eins
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/tracker-dust-96-01-vol-1/9-minichip-eins.jpeg
    cover: /assets/albums/tracker-dust-96-01-vol-1/9-minichip-eins.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 1
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 70.4
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TALB
          value: Tracker Dust 96-01 Vol 1
        - id: TRCK
          value: '9'
        - id: TPE1
          value: Panix
        - id: TIT2
          value: Minichip Eins
        - id: TYER
          value: '2005'
    quality:
      warnings: []
    common:
      track:
        'no': 9
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      album: Tracker Dust 96-01 Vol 1
      artists:
        - Panix
      artist: Panix
      title: Minichip Eins
      year: 2005
    transformed:
      ID3v2.3:
        TCON: Electronic
        TALB: Tracker Dust 96-01 Vol 1
        TRCK: '9'
        TPE1: Panix
        TIT2: Minichip Eins
        TYER: '2005'
    all:
      TCON: Electronic
      TALB: Tracker Dust 96-01 Vol 1
      TRCK: '9'
      TPE1: Panix
      TIT2: Minichip Eins
      TYER: '2005'
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/tracker-dust-96-01-vol-1/10-minichip-zwei.mp3
    audio: /assets/albums/tracker-dust-96-01-vol-1/10-minichip-zwei.mp3
    slug: tracker-dust-96-01-vol-1/10-minichip-zwei
    albumSlug: tracker-dust-96-01-vol-1
    trackSlug: 10-minichip-zwei
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/tracker-dust-96-01-vol-1/10-minichip-zwei.jpeg
    cover: /assets/albums/tracker-dust-96-01-vol-1/10-minichip-zwei.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 1
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 115.35673469387756
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TRCK
          value: '10'
        - id: TALB
          value: Tracker Dust 96-01 Vol 1
        - id: TPE1
          value: Panix
        - id: TIT2
          value: Minichip Zwei
        - id: TYER
          value: '2005'
    quality:
      warnings: []
    common:
      track:
        'no': 10
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      album: Tracker Dust 96-01 Vol 1
      artists:
        - Panix
      artist: Panix
      title: Minichip Zwei
      year: 2005
    transformed:
      ID3v2.3:
        TCON: Electronic
        TRCK: '10'
        TALB: Tracker Dust 96-01 Vol 1
        TPE1: Panix
        TIT2: Minichip Zwei
        TYER: '2005'
    all:
      TCON: Electronic
      TRCK: '10'
      TALB: Tracker Dust 96-01 Vol 1
      TPE1: Panix
      TIT2: Minichip Zwei
      TYER: '2005'
---
