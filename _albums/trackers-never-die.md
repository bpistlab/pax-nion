---
layout: album
slug: trackers-never-die
name: Trackers Never Die
artists: Panix
bitrate:
  - 256000
  - 112000
  - 192000
  - 320000
trackCount: 8
cover: /assets/albums/trackers-never-die/1-diptbiit.jpeg
date: 2005-1-1
tracks:
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/trackers-never-die/1-diptbiit.mp3
    audio: /assets/albums/trackers-never-die/1-diptbiit.mp3
    slug: trackers-never-die/1-diptbiit
    albumSlug: trackers-never-die
    trackSlug: 1-diptbiit
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/trackers-never-die/1-diptbiit.jpeg
    cover: /assets/albums/trackers-never-die/1-diptbiit.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 256000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 149.26367346938775
    native:
      ID3v2.3:
        - id: TALB
          value: Trackers Never Die
        - id: TRCK
          value: '1'
        - id: WXXX
          value: &ref_0
            description: ''
            url: ''
        - id: TCON
          value: Electronic
        - id: TPE1
          value: Panix
        - id: TIT2
          value: Diptbiit
        - id: TYER
          value: '2005'
    quality:
      warnings: []
    common:
      track:
        'no': 1
        of: null
      disk:
        'no': null
        of: null
      album: Trackers Never Die
      genre:
        - Electronic
      artists:
        - Panix
      artist: Panix
      title: Diptbiit
      year: 2005
    transformed:
      ID3v2.3:
        TALB: Trackers Never Die
        TRCK: '1'
        WXXX: *ref_0
        TCON: Electronic
        TPE1: Panix
        TIT2: Diptbiit
        TYER: '2005'
    all:
      TALB: Trackers Never Die
      TRCK: '1'
      WXXX: *ref_0
      TCON: Electronic
      TPE1: Panix
      TIT2: Diptbiit
      TYER: '2005'
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/trackers-never-die/2-kyddbiitz.mp3
    audio: /assets/albums/trackers-never-die/2-kyddbiitz.mp3
    slug: trackers-never-die/2-kyddbiitz
    albumSlug: trackers-never-die
    trackSlug: 2-kyddbiitz
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/trackers-never-die/2-kyddbiitz.jpeg
    cover: /assets/albums/trackers-never-die/2-kyddbiitz.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 1
      bitrate: 112000
      codecProfile: CBR
      numberOfSamples: 6342912
      duration: 143.83020408163264
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TALB
          value: Trackers Never Die
        - id: TRCK
          value: '2'
        - id: TPE1
          value: Panix
        - id: TIT2
          value: Kyddbiitz
        - id: TYER
          value: '2005'
    quality:
      warnings: []
    common:
      track:
        'no': 2
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      album: Trackers Never Die
      artists:
        - Panix
      artist: Panix
      title: Kyddbiitz
      year: 2005
    transformed:
      ID3v2.3:
        TCON: Electronic
        TALB: Trackers Never Die
        TRCK: '2'
        TPE1: Panix
        TIT2: Kyddbiitz
        TYER: '2005'
    all:
      TCON: Electronic
      TALB: Trackers Never Die
      TRCK: '2'
      TPE1: Panix
      TIT2: Kyddbiitz
      TYER: '2005'
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/trackers-never-die/3-our-love.mp3
    audio: /assets/albums/trackers-never-die/3-our-love.mp3
    slug: trackers-never-die/3-our-love
    albumSlug: trackers-never-die
    trackSlug: 3-our-love
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/trackers-never-die/3-our-love.jpeg
    cover: /assets/albums/trackers-never-die/3-our-love.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 1
      bitrate: 112000
      codecProfile: CBR
      tool: LAME3.96r
      duration: 96
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TALB
          value: Trackers Never Die
        - id: TRCK
          value: '3'
        - id: TPE1
          value: Panix
        - id: TIT2
          value: Our Love
        - id: TYER
          value: '2005'
    quality:
      warnings: []
    common:
      track:
        'no': 3
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      album: Trackers Never Die
      artists:
        - Panix
      artist: Panix
      title: Our Love
      year: 2005
    transformed:
      ID3v2.3:
        TCON: Electronic
        TALB: Trackers Never Die
        TRCK: '3'
        TPE1: Panix
        TIT2: Our Love
        TYER: '2005'
    all:
      TCON: Electronic
      TALB: Trackers Never Die
      TRCK: '3'
      TPE1: Panix
      TIT2: Our Love
      TYER: '2005'
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/trackers-never-die/4-ropparnintendoyxmord.mp3
    audio: /assets/albums/trackers-never-die/4-ropparnintendoyxmord.mp3
    slug: trackers-never-die/4-ropparnintendoyxmord
    albumSlug: trackers-never-die
    trackSlug: 4-ropparnintendoyxmord
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/trackers-never-die/4-ropparnintendoyxmord.jpeg
    cover: /assets/albums/trackers-never-die/4-ropparnintendoyxmord.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 192000
      codecProfile: CBR
      tool: 'LAME3.95 '
      duration: 78.83755102040816
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TALB
          value: Trackers Never Die
        - id: TRCK
          value: '4'
        - id: TPE1
          value: Panix
        - id: TIT2
          value: Ropparnintendoyxmord
        - id: TYER
          value: '2005'
    quality:
      warnings: []
    common:
      track:
        'no': 4
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      album: Trackers Never Die
      artists:
        - Panix
      artist: Panix
      title: Ropparnintendoyxmord
      year: 2005
    transformed:
      ID3v2.3:
        TCON: Electronic
        TALB: Trackers Never Die
        TRCK: '4'
        TPE1: Panix
        TIT2: Ropparnintendoyxmord
        TYER: '2005'
    all:
      TCON: Electronic
      TALB: Trackers Never Die
      TRCK: '4'
      TPE1: Panix
      TIT2: Ropparnintendoyxmord
      TYER: '2005'
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/trackers-never-die/5-shufflethatass.mp3
    audio: /assets/albums/trackers-never-die/5-shufflethatass.mp3
    slug: trackers-never-die/5-shufflethatass
    albumSlug: trackers-never-die
    trackSlug: 5-shufflethatass
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/trackers-never-die/5-shufflethatass.jpeg
    cover: /assets/albums/trackers-never-die/5-shufflethatass.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 192000
      codecProfile: CBR
      tool: 'LAME3.95 '
      duration: 76.95673469387755
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TALB
          value: Trackers Never Die
        - id: TRCK
          value: '05'
        - id: TPE1
          value: Panix
        - id: TIT2
          value: Shufflethatass
        - id: TYER
          value: '2005'
    quality:
      warnings: []
    common:
      track:
        'no': 5
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      album: Trackers Never Die
      artists:
        - Panix
      artist: Panix
      title: Shufflethatass
      year: 2005
    transformed:
      ID3v2.3:
        TCON: Electronic
        TALB: Trackers Never Die
        TRCK: '05'
        TPE1: Panix
        TIT2: Shufflethatass
        TYER: '2005'
    all:
      TCON: Electronic
      TALB: Trackers Never Die
      TRCK: '05'
      TPE1: Panix
      TIT2: Shufflethatass
      TYER: '2005'
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/trackers-never-die/6-so-fucking-money.mp3
    audio: /assets/albums/trackers-never-die/6-so-fucking-money.mp3
    slug: trackers-never-die/6-so-fucking-money
    albumSlug: trackers-never-die
    trackSlug: 6-so-fucking-money
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/trackers-never-die/6-so-fucking-money.jpeg
    cover: /assets/albums/trackers-never-die/6-so-fucking-money.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 192000
      codecProfile: CBR
      tool: 'LAME3.95 '
      duration: 126.79836734693878
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TALB
          value: Trackers Never Die
        - id: TRCK
          value: '06'
        - id: TPE1
          value: Panix
        - id: TIT2
          value: So Fucking Money
        - id: TYER
          value: '2005'
    quality:
      warnings: []
    common:
      track:
        'no': 6
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      album: Trackers Never Die
      artists:
        - Panix
      artist: Panix
      title: So Fucking Money
      year: 2005
    transformed:
      ID3v2.3:
        TCON: Electronic
        TALB: Trackers Never Die
        TRCK: '06'
        TPE1: Panix
        TIT2: So Fucking Money
        TYER: '2005'
    all:
      TCON: Electronic
      TALB: Trackers Never Die
      TRCK: '06'
      TPE1: Panix
      TIT2: So Fucking Money
      TYER: '2005'
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/trackers-never-die/7-siddstah.mp3
    audio: /assets/albums/trackers-never-die/7-siddstah.mp3
    slug: trackers-never-die/7-siddstah
    albumSlug: trackers-never-die
    trackSlug: 7-siddstah
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/trackers-never-die/7-siddstah.jpeg
    cover: /assets/albums/trackers-never-die/7-siddstah.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 1
      bitrate: 192000
      codecProfile: CBR
      tool: 'LAME3.97 '
      duration: 128.15673469387755
    native:
      ID3v2.3:
        - id: TRCK
          value: '7'
        - id: WXXX
          value: &ref_1
            description: ''
            url: ''
        - id: TOPE
          value: ''
        - id: TCOM
          value: ''
        - id: TCON
          value: Electronic
        - id: TALB
          value: Trackers Never Die
        - id: TPE1
          value: Panix
        - id: TIT2
          value: Siddstah
        - id: TYER
          value: '2005'
    quality:
      warnings: []
    common:
      track:
        'no': 7
        of: null
      disk:
        'no': null
        of: null
      originalartist: ''
      composer:
        - ''
      genre:
        - Electronic
      album: Trackers Never Die
      artists:
        - Panix
      artist: Panix
      title: Siddstah
      year: 2005
    transformed:
      ID3v2.3:
        TRCK: '7'
        WXXX: *ref_1
        TOPE: ''
        TCOM: ''
        TCON: Electronic
        TALB: Trackers Never Die
        TPE1: Panix
        TIT2: Siddstah
        TYER: '2005'
    all:
      TRCK: '7'
      WXXX: *ref_1
      TOPE: ''
      TCOM: ''
      TCON: Electronic
      TALB: Trackers Never Die
      TPE1: Panix
      TIT2: Siddstah
      TYER: '2005'
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/trackers-never-die/8-spiel.mp3
    audio: /assets/albums/trackers-never-die/8-spiel.mp3
    slug: trackers-never-die/8-spiel
    albumSlug: trackers-never-die
    trackSlug: 8-spiel
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/trackers-never-die/8-spiel.jpeg
    cover: /assets/albums/trackers-never-die/8-spiel.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME 3.97UU
      duration: 68.54530612244898
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TALB
          value: Trackers Never Die
        - id: TRCK
          value: '8'
        - id: TPE1
          value: Panix
        - id: TIT2
          value: Spiel
        - id: TYER
          value: '2005'
    quality:
      warnings: []
    common:
      track:
        'no': 8
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      album: Trackers Never Die
      artists:
        - Panix
      artist: Panix
      title: Spiel
      year: 2005
    transformed:
      ID3v2.3:
        TCON: Electronic
        TALB: Trackers Never Die
        TRCK: '8'
        TPE1: Panix
        TIT2: Spiel
        TYER: '2005'
    all:
      TCON: Electronic
      TALB: Trackers Never Die
      TRCK: '8'
      TPE1: Panix
      TIT2: Spiel
      TYER: '2005'
---
