---
layout: album
slug: broken-tracks
name: Broken Tracks
artists: Panik
bitrate:
  - 128000
  - 192000
trackCount: 4
cover: /assets/albums/broken-tracks/1-coming-back.jpeg
date: 1997-1-1
tracks:
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/broken-tracks/1-coming-back.mp3
    audio: /assets/albums/broken-tracks/1-coming-back.mp3
    slug: broken-tracks/1-coming-back
    albumSlug: broken-tracks
    trackSlug: 1-coming-back
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/broken-tracks/1-coming-back.jpeg
    cover: /assets/albums/broken-tracks/1-coming-back.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 1
      bitrate: 128000
      codecProfile: CBR
      tool: 'LAME3.97 '
      duration: 136.9338775510204
    native:
      ID3v2.3:
        - id: TRCK
          value: '1'
        - id: TCOM
          value: ''
        - id: TCON
          value: Breakbeat
        - id: TALB
          value: Broken Tracks
        - id: TPE1
          value: Panik
        - id: TIT2
          value: Coming Back
        - id: TYER
          value: '1997'
    quality:
      warnings: []
    common:
      track:
        'no': 1
        of: null
      disk:
        'no': null
        of: null
      composer:
        - ''
      genre:
        - Breakbeat
      album: Broken Tracks
      artists:
        - Panik
      artist: Panik
      title: Coming Back
      year: 1997
    transformed:
      ID3v2.3:
        TRCK: '1'
        TCOM: ''
        TCON: Breakbeat
        TALB: Broken Tracks
        TPE1: Panik
        TIT2: Coming Back
        TYER: '1997'
    all:
      TRCK: '1'
      TCOM: ''
      TCON: Breakbeat
      TALB: Broken Tracks
      TPE1: Panik
      TIT2: Coming Back
      TYER: '1997'
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/broken-tracks/1-shaolin-teknique.mp3
    audio: /assets/albums/broken-tracks/1-shaolin-teknique.mp3
    slug: broken-tracks/1-shaolin-teknique
    albumSlug: broken-tracks
    trackSlug: 1-shaolin-teknique
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/broken-tracks/1-shaolin-teknique.jpeg
    cover: /assets/albums/broken-tracks/1-shaolin-teknique.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 1
      bitrate: 192000
      codecProfile: CBR
      tool: 'LAME3.97 '
      duration: 229.51183673469387
    native:
      ID3v2.3:
        - id: TRCK
          value: '1'
        - id: TCOM
          value: ''
        - id: TCON
          value: Breakbeat
        - id: TALB
          value: Broken Tracks
        - id: TPE1
          value: Panik
        - id: TIT2
          value: Shaolin Teknique
        - id: TYER
          value: '1997'
    quality:
      warnings: []
    common:
      track:
        'no': 1
        of: null
      disk:
        'no': null
        of: null
      composer:
        - ''
      genre:
        - Breakbeat
      album: Broken Tracks
      artists:
        - Panik
      artist: Panik
      title: Shaolin Teknique
      year: 1997
    transformed:
      ID3v2.3:
        TRCK: '1'
        TCOM: ''
        TCON: Breakbeat
        TALB: Broken Tracks
        TPE1: Panik
        TIT2: Shaolin Teknique
        TYER: '1997'
    all:
      TRCK: '1'
      TCOM: ''
      TCON: Breakbeat
      TALB: Broken Tracks
      TPE1: Panik
      TIT2: Shaolin Teknique
      TYER: '1997'
  - path: /mnt/usb32gb/organized-pax-nion/assets/albums/broken-tracks/3-hcmf.mp3
    audio: /assets/albums/broken-tracks/3-hcmf.mp3
    slug: broken-tracks/3-hcmf
    albumSlug: broken-tracks
    trackSlug: 3-hcmf
    coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/broken-tracks/3-hcmf.jpeg
    cover: /assets/albums/broken-tracks/3-hcmf.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 1
      bitrate: 192000
      codecProfile: CBR
      tool: 'LAME3.97 '
      duration: 244.97632653061225
    native:
      ID3v2.3:
        - id: TCON
          value: Breakbeat
        - id: TRCK
          value: '3'
        - id: TALB
          value: Broken Tracks
        - id: TCOM
          value: ''
        - id: TPE1
          value: Panik
        - id: TIT2
          value: HCMF
        - id: TYER
          value: '1997'
    quality:
      warnings: []
    common:
      track:
        'no': 3
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Breakbeat
      album: Broken Tracks
      composer:
        - ''
      artists:
        - Panik
      artist: Panik
      title: HCMF
      year: 1997
    transformed:
      ID3v2.3:
        TCON: Breakbeat
        TRCK: '3'
        TALB: Broken Tracks
        TCOM: ''
        TPE1: Panik
        TIT2: HCMF
        TYER: '1997'
    all:
      TCON: Breakbeat
      TRCK: '3'
      TALB: Broken Tracks
      TCOM: ''
      TPE1: Panik
      TIT2: HCMF
      TYER: '1997'
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/broken-tracks/4-your-love.mp3
    audio: /assets/albums/broken-tracks/4-your-love.mp3
    slug: broken-tracks/4-your-love
    albumSlug: broken-tracks
    trackSlug: 4-your-love
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/broken-tracks/4-your-love.jpeg
    cover: /assets/albums/broken-tracks/4-your-love.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 1
      bitrate: 192000
      codecProfile: CBR
      tool: 'LAME3.97 '
      duration: 199.0008163265306
    native:
      ID3v2.3:
        - id: TCON
          value: Breakbeat
        - id: TRCK
          value: '4'
        - id: TALB
          value: Broken Tracks
        - id: TCOM
          value: ''
        - id: TPE1
          value: Panik
        - id: TIT2
          value: Your Love
        - id: TYER
          value: '1997'
    quality:
      warnings: []
    common:
      track:
        'no': 4
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Breakbeat
      album: Broken Tracks
      composer:
        - ''
      artists:
        - Panik
      artist: Panik
      title: Your Love
      year: 1997
    transformed:
      ID3v2.3:
        TCON: Breakbeat
        TRCK: '4'
        TALB: Broken Tracks
        TCOM: ''
        TPE1: Panik
        TIT2: Your Love
        TYER: '1997'
    all:
      TCON: Breakbeat
      TRCK: '4'
      TALB: Broken Tracks
      TCOM: ''
      TPE1: Panik
      TIT2: Your Love
      TYER: '1997'
---
