---
layout: album
slug: rabiat-pop
name: Rabiat Pop
artists:
  - Panix
  - Panix Hilton
  - Panydzix
  - Rabiat
bitrate:
  - 192000
  - 320000
trackCount: 8
cover: /assets/albums/rabiat-pop/3-don-t-leave-me.jpeg
date: 2013-1-1
tracks:
  - path: /mnt/usb32gb/organized-pax-nion/assets/albums/rabiat-pop/1-ofrivillig.mp3
    audio: /assets/albums/rabiat-pop/1-ofrivillig.mp3
    slug: rabiat-pop/1-ofrivillig
    albumSlug: rabiat-pop
    trackSlug: 1-ofrivillig
    coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/rabiat-pop/1-ofrivillig.jpeg
    cover: /assets/albums/rabiat-pop/1-ofrivillig.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME 3.97UU
      duration: 96.02612244897959
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TALB
          value: Rabiat Pop
        - id: TRCK
          value: '1'
        - id: TPE1
          value: Rabiat
        - id: TIT2
          value: Ofrivillig
        - id: TOPE
          value: panyxd
        - id: TYER
          value: '2013'
    quality:
      warnings: []
    common:
      track:
        'no': 1
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      album: Rabiat Pop
      artists:
        - Rabiat
      artist: Rabiat
      title: Ofrivillig
      originalartist: panyxd
      year: 2013
    transformed:
      ID3v2.3:
        TCON: Electronic
        TALB: Rabiat Pop
        TRCK: '1'
        TPE1: Rabiat
        TIT2: Ofrivillig
        TOPE: panyxd
        TYER: '2013'
    all:
      TCON: Electronic
      TALB: Rabiat Pop
      TRCK: '1'
      TPE1: Rabiat
      TIT2: Ofrivillig
      TOPE: panyxd
      TYER: '2013'
  - path: /mnt/usb32gb/organized-pax-nion/assets/albums/rabiat-pop/2-fisheye.mp3
    audio: /assets/albums/rabiat-pop/2-fisheye.mp3
    slug: rabiat-pop/2-fisheye
    albumSlug: rabiat-pop
    trackSlug: 2-fisheye
    coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/rabiat-pop/2-fisheye.jpeg
    cover: /assets/albums/rabiat-pop/2-fisheye.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 97.07102040816326
    native:
      ID3v2.3:
        - id: TALB
          value: Rabiat Pop
        - id: TCON
          value: Electronic
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: Fisheye
        - id: TRCK
          value: '2'
        - id: TOPE
          value: panyxd
        - id: TYER
          value: '2013'
    quality:
      warnings: []
    common:
      track:
        'no': 2
        of: null
      disk:
        'no': null
        of: null
      album: Rabiat Pop
      genre:
        - Electronic
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: Fisheye
      originalartist: panyxd
      year: 2013
    transformed:
      ID3v2.3:
        TALB: Rabiat Pop
        TCON: Electronic
        TPE1: Panix Hilton
        TIT2: Fisheye
        TRCK: '2'
        TOPE: panyxd
        TYER: '2013'
    all:
      TALB: Rabiat Pop
      TCON: Electronic
      TPE1: Panix Hilton
      TIT2: Fisheye
      TRCK: '2'
      TOPE: panyxd
      TYER: '2013'
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/rabiat-pop/3-don-t-leave-me.mp3
    audio: /assets/albums/rabiat-pop/3-don-t-leave-me.mp3
    slug: rabiat-pop/3-don-t-leave-me
    albumSlug: rabiat-pop
    trackSlug: 3-don-t-leave-me
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/rabiat-pop/3-don-t-leave-me.jpeg
    cover: /assets/albums/rabiat-pop/3-don-t-leave-me.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 192000
      codecProfile: CBR
      tool: 'LAME3.95 '
      duration: 160.13061224489795
    native:
      ID3v2.3:
        - id: TRCK
          value: '3'
        - id: TALB
          value: Rabiat Pop
        - id: TCON
          value: Electronic
        - id: TIT2
          value: Don't leave me
        - id: TPE1
          value: Panix
        - id: TOPE
          value: panyxd
        - id: TYER
          value: '2013'
    quality:
      warnings: []
    common:
      track:
        'no': 3
        of: null
      disk:
        'no': null
        of: null
      album: Rabiat Pop
      genre:
        - Electronic
      title: Don't leave me
      artists:
        - Panix
      artist: Panix
      originalartist: panyxd
      year: 2013
    transformed:
      ID3v2.3:
        TRCK: '3'
        TALB: Rabiat Pop
        TCON: Electronic
        TIT2: Don't leave me
        TPE1: Panix
        TOPE: panyxd
        TYER: '2013'
    all:
      TRCK: '3'
      TALB: Rabiat Pop
      TCON: Electronic
      TIT2: Don't leave me
      TPE1: Panix
      TOPE: panyxd
      TYER: '2013'
  - path: /mnt/usb32gb/organized-pax-nion/assets/albums/rabiat-pop/4-fyllepops.mp3
    audio: /assets/albums/rabiat-pop/4-fyllepops.mp3
    slug: rabiat-pop/4-fyllepops
    albumSlug: rabiat-pop
    trackSlug: 4-fyllepops
    coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/rabiat-pop/4-fyllepops.jpeg
    cover: /assets/albums/rabiat-pop/4-fyllepops.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 94.22367346938775
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TALB
          value: Rabiat Pop
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: Fyllepops
        - id: TRCK
          value: '4'
        - id: TOPE
          value: panyxd
        - id: TYER
          value: '2013'
    quality:
      warnings: []
    common:
      track:
        'no': 4
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      album: Rabiat Pop
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: Fyllepops
      originalartist: panyxd
      year: 2013
    transformed:
      ID3v2.3:
        TCON: Electronic
        TALB: Rabiat Pop
        TPE1: Panix Hilton
        TIT2: Fyllepops
        TRCK: '4'
        TOPE: panyxd
        TYER: '2013'
    all:
      TCON: Electronic
      TALB: Rabiat Pop
      TPE1: Panix Hilton
      TIT2: Fyllepops
      TRCK: '4'
      TOPE: panyxd
      TYER: '2013'
  - path: /mnt/usb32gb/organized-pax-nion/assets/albums/rabiat-pop/5-incorrect.mp3
    audio: /assets/albums/rabiat-pop/5-incorrect.mp3
    slug: rabiat-pop/5-incorrect
    albumSlug: rabiat-pop
    trackSlug: 5-incorrect
    coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/rabiat-pop/5-incorrect.jpeg
    cover: /assets/albums/rabiat-pop/5-incorrect.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 1
      bitrate: 320000
      codecProfile: CBR
      tool: 'LAME3.97 '
      duration: 109.68816326530612
    native:
      ID3v2.3:
        - id: TRCK
          value: '5'
        - id: TOPE
          value: panyxd
        - id: TCON
          value: Electronic
        - id: TALB
          value: Rabiat Pop
        - id: TPE1
          value: Panydzix
        - id: TIT2
          value: Incorrect
        - id: TYER
          value: '2013'
    quality:
      warnings: []
    common:
      track:
        'no': 5
        of: null
      disk:
        'no': null
        of: null
      originalartist: panyxd
      genre:
        - Electronic
      album: Rabiat Pop
      artists:
        - Panydzix
      artist: Panydzix
      title: Incorrect
      year: 2013
    transformed:
      ID3v2.3:
        TRCK: '5'
        TOPE: panyxd
        TCON: Electronic
        TALB: Rabiat Pop
        TPE1: Panydzix
        TIT2: Incorrect
        TYER: '2013'
    all:
      TRCK: '5'
      TOPE: panyxd
      TCON: Electronic
      TALB: Rabiat Pop
      TPE1: Panydzix
      TIT2: Incorrect
      TYER: '2013'
  - path: /mnt/usb32gb/organized-pax-nion/assets/albums/rabiat-pop/6-doublejump.mp3
    audio: /assets/albums/rabiat-pop/6-doublejump.mp3
    slug: rabiat-pop/6-doublejump
    albumSlug: rabiat-pop
    trackSlug: 6-doublejump
    coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/rabiat-pop/6-doublejump.jpeg
    cover: /assets/albums/rabiat-pop/6-doublejump.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 69.25061224489797
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TALB
          value: Rabiat Pop
        - id: TRCK
          value: '6'
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: Doublejump
        - id: TOPE
          value: panyxd
        - id: TYER
          value: '2013'
    quality:
      warnings: []
    common:
      track:
        'no': 6
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      album: Rabiat Pop
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: Doublejump
      originalartist: panyxd
      year: 2013
    transformed:
      ID3v2.3:
        TCON: Electronic
        TALB: Rabiat Pop
        TRCK: '6'
        TPE1: Panix Hilton
        TIT2: Doublejump
        TOPE: panyxd
        TYER: '2013'
    all:
      TCON: Electronic
      TALB: Rabiat Pop
      TRCK: '6'
      TPE1: Panix Hilton
      TIT2: Doublejump
      TOPE: panyxd
      TYER: '2013'
  - path: /mnt/usb32gb/organized-pax-nion/assets/albums/rabiat-pop/7-floatershy.mp3
    audio: /assets/albums/rabiat-pop/7-floatershy.mp3
    slug: rabiat-pop/7-floatershy
    albumSlug: rabiat-pop
    trackSlug: 7-floatershy
    coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/rabiat-pop/7-floatershy.jpeg
    cover: /assets/albums/rabiat-pop/7-floatershy.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 48000
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.99r
      duration: 102.888
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TALB
          value: Rabiat Pop
        - id: TRCK
          value: '7'
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: Floatershy
        - id: TOPE
          value: panyxd
        - id: TYER
          value: '2013'
    quality:
      warnings: []
    common:
      track:
        'no': 7
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      album: Rabiat Pop
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: Floatershy
      originalartist: panyxd
      year: 2013
    transformed:
      ID3v2.3:
        TCON: Electronic
        TALB: Rabiat Pop
        TRCK: '7'
        TPE1: Panix Hilton
        TIT2: Floatershy
        TOPE: panyxd
        TYER: '2013'
    all:
      TCON: Electronic
      TALB: Rabiat Pop
      TRCK: '7'
      TPE1: Panix Hilton
      TIT2: Floatershy
      TOPE: panyxd
      TYER: '2013'
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/rabiat-pop/8-kill-me-mickey.mp3
    audio: /assets/albums/rabiat-pop/8-kill-me-mickey.mp3
    slug: rabiat-pop/8-kill-me-mickey
    albumSlug: rabiat-pop
    trackSlug: 8-kill-me-mickey
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/rabiat-pop/8-kill-me-mickey.jpeg
    cover: /assets/albums/rabiat-pop/8-kill-me-mickey.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 1
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.99r
      duration: 113.31918367346938
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TALB
          value: Rabiat Pop
        - id: TRCK
          value: '8'
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: Kill Me Mickey
        - id: TOPE
          value: panyxd
        - id: TYER
          value: '2013'
    quality:
      warnings: []
    common:
      track:
        'no': 8
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      album: Rabiat Pop
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: Kill Me Mickey
      originalartist: panyxd
      year: 2013
    transformed:
      ID3v2.3:
        TCON: Electronic
        TALB: Rabiat Pop
        TRCK: '8'
        TPE1: Panix Hilton
        TIT2: Kill Me Mickey
        TOPE: panyxd
        TYER: '2013'
    all:
      TCON: Electronic
      TALB: Rabiat Pop
      TRCK: '8'
      TPE1: Panix Hilton
      TIT2: Kill Me Mickey
      TOPE: panyxd
      TYER: '2013'
---
