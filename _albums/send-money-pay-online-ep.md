---
layout: album
slug: send-money-pay-online-ep
name: 'Send Money, Pay Online EP'
artists: Paypal
bitrate: 320000
trackCount: 3
cover: /assets/albums/send-money-pay-online-ep/1-buy-sell-transfer.jpeg
date: 2013-1-1
tracks:
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/send-money-pay-online-ep/1-buy-sell-transfer.mp3
    audio: /assets/albums/send-money-pay-online-ep/1-buy-sell-transfer.mp3
    slug: send-money-pay-online-ep/1-buy-sell-transfer
    albumSlug: send-money-pay-online-ep
    trackSlug: 1-buy-sell-transfer
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/send-money-pay-online-ep/1-buy-sell-transfer.jpeg
    cover: /assets/albums/send-money-pay-online-ep/1-buy-sell-transfer.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 48000
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.99r
      duration: 91.224
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TYER
          value: '2013'
        - id: TALB
          value: 'Send Money, Pay Online EP'
        - id: TRCK
          value: '1'
        - id: TPE1
          value: Paypal
        - id: TIT2
          value: Buy Sell Transfer
    quality:
      warnings: []
    common:
      track:
        'no': 1
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2013
      album: 'Send Money, Pay Online EP'
      artists:
        - Paypal
      artist: Paypal
      title: Buy Sell Transfer
    transformed:
      ID3v2.3:
        TCON: Electronic
        TYER: '2013'
        TALB: 'Send Money, Pay Online EP'
        TRCK: '1'
        TPE1: Paypal
        TIT2: Buy Sell Transfer
    all:
      TCON: Electronic
      TYER: '2013'
      TALB: 'Send Money, Pay Online EP'
      TRCK: '1'
      TPE1: Paypal
      TIT2: Buy Sell Transfer
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/send-money-pay-online-ep/2-buy-into-being-safer.mp3
    audio: /assets/albums/send-money-pay-online-ep/2-buy-into-being-safer.mp3
    slug: send-money-pay-online-ep/2-buy-into-being-safer
    albumSlug: send-money-pay-online-ep
    trackSlug: 2-buy-into-being-safer
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/send-money-pay-online-ep/2-buy-into-being-safer.jpeg
    cover: /assets/albums/send-money-pay-online-ep/2-buy-into-being-safer.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 48000
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.99r
      duration: 293.16
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TYER
          value: '2013'
        - id: TALB
          value: 'Send Money, Pay Online EP'
        - id: TRCK
          value: '2'
        - id: TIT2
          value: Buy into being safer
        - id: TPE1
          value: Paypal
    quality:
      warnings: []
    common:
      track:
        'no': 2
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2013
      album: 'Send Money, Pay Online EP'
      title: Buy into being safer
      artists:
        - Paypal
      artist: Paypal
    transformed:
      ID3v2.3:
        TCON: Electronic
        TYER: '2013'
        TALB: 'Send Money, Pay Online EP'
        TRCK: '2'
        TIT2: Buy into being safer
        TPE1: Paypal
    all:
      TCON: Electronic
      TYER: '2013'
      TALB: 'Send Money, Pay Online EP'
      TRCK: '2'
      TIT2: Buy into being safer
      TPE1: Paypal
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/send-money-pay-online-ep/3-sign-up-for-free.mp3
    audio: /assets/albums/send-money-pay-online-ep/3-sign-up-for-free.mp3
    slug: send-money-pay-online-ep/3-sign-up-for-free
    albumSlug: send-money-pay-online-ep
    trackSlug: 3-sign-up-for-free
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/send-money-pay-online-ep/3-sign-up-for-free.jpeg
    cover: /assets/albums/send-money-pay-online-ep/3-sign-up-for-free.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 48000
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.99r
      duration: 182.904
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TYER
          value: '2013'
        - id: TALB
          value: 'Send Money, Pay Online EP'
        - id: TRCK
          value: '3'
        - id: TPE1
          value: Paypal
        - id: TIT2
          value: Sign Up for Free
    quality:
      warnings: []
    common:
      track:
        'no': 3
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2013
      album: 'Send Money, Pay Online EP'
      artists:
        - Paypal
      artist: Paypal
      title: Sign Up for Free
    transformed:
      ID3v2.3:
        TCON: Electronic
        TYER: '2013'
        TALB: 'Send Money, Pay Online EP'
        TRCK: '3'
        TPE1: Paypal
        TIT2: Sign Up for Free
    all:
      TCON: Electronic
      TYER: '2013'
      TALB: 'Send Money, Pay Online EP'
      TRCK: '3'
      TPE1: Paypal
      TIT2: Sign Up for Free
---
