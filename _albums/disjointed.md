---
layout: album
slug: disjointed
name: Disjointed
artists: Panix Hilton
bitrate: 320000
trackCount: 6
cover: /assets/albums/disjointed/1-disjointed.jpeg
date: 2013-1-1
tracks:
  - path: /mnt/usb32gb/organized-pax-nion/assets/albums/disjointed/1-disjointed.mp3
    audio: /assets/albums/disjointed/1-disjointed.mp3
    slug: disjointed/1-disjointed
    albumSlug: disjointed
    trackSlug: 1-disjointed
    coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/disjointed/1-disjointed.jpeg
    cover: /assets/albums/disjointed/1-disjointed.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.99r
      duration: 74.2138775510204
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TYER
          value: '2013'
        - id: TALB
          value: Disjointed
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: Disjointed
        - id: TRCK
          value: '1'
    quality:
      warnings: []
    common:
      track:
        'no': 1
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2013
      album: Disjointed
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: Disjointed
    transformed:
      ID3v2.3:
        TCON: Electronic
        TYER: '2013'
        TALB: Disjointed
        TPE1: Panix Hilton
        TIT2: Disjointed
        TRCK: '1'
    all:
      TCON: Electronic
      TYER: '2013'
      TALB: Disjointed
      TPE1: Panix Hilton
      TIT2: Disjointed
      TRCK: '1'
  - path: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/disjointed/2-ze-morning-j-m.mp3
    audio: /assets/albums/disjointed/2-ze-morning-j-m.mp3
    slug: disjointed/2-ze-morning-j-m
    albumSlug: disjointed
    trackSlug: 2-ze-morning-j-m
    coverPath: >-
      /mnt/usb32gb/organized-pax-nion/assets/albums/disjointed/2-ze-morning-j-m.jpeg
    cover: /assets/albums/disjointed/2-ze-morning-j-m.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 106.57959183673469
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TYER
          value: '2013'
        - id: TALB
          value: Disjointed
        - id: TRCK
          value: '2'
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: Ze Morning Jäm
    quality:
      warnings: []
    common:
      track:
        'no': 2
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2013
      album: Disjointed
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: Ze Morning Jäm
    transformed:
      ID3v2.3:
        TCON: Electronic
        TYER: '2013'
        TALB: Disjointed
        TRCK: '2'
        TPE1: Panix Hilton
        TIT2: Ze Morning Jäm
    all:
      TCON: Electronic
      TYER: '2013'
      TALB: Disjointed
      TRCK: '2'
      TPE1: Panix Hilton
      TIT2: Ze Morning Jäm
  - path: /mnt/usb32gb/organized-pax-nion/assets/albums/disjointed/3-oh-yeah.mp3
    audio: /assets/albums/disjointed/3-oh-yeah.mp3
    slug: disjointed/3-oh-yeah
    albumSlug: disjointed
    trackSlug: 3-oh-yeah
    coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/disjointed/3-oh-yeah.jpeg
    cover: /assets/albums/disjointed/3-oh-yeah.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.99r
      duration: 143.90857142857143
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TYER
          value: '2013'
        - id: TALB
          value: Disjointed
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: Oh Yeah
        - id: TRCK
          value: '3'
    quality:
      warnings: []
    common:
      track:
        'no': 3
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2013
      album: Disjointed
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: Oh Yeah
    transformed:
      ID3v2.3:
        TCON: Electronic
        TYER: '2013'
        TALB: Disjointed
        TPE1: Panix Hilton
        TIT2: Oh Yeah
        TRCK: '3'
    all:
      TCON: Electronic
      TYER: '2013'
      TALB: Disjointed
      TPE1: Panix Hilton
      TIT2: Oh Yeah
      TRCK: '3'
  - path: /mnt/usb32gb/organized-pax-nion/assets/albums/disjointed/4-fluting.mp3
    audio: /assets/albums/disjointed/4-fluting.mp3
    slug: disjointed/4-fluting
    albumSlug: disjointed
    trackSlug: 4-fluting
    coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/disjointed/4-fluting.jpeg
    cover: /assets/albums/disjointed/4-fluting.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 100.88489795918368
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TYER
          value: '2013'
        - id: TALB
          value: Disjointed
        - id: TRCK
          value: '4'
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: Fluting
    quality:
      warnings: []
    common:
      track:
        'no': 4
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2013
      album: Disjointed
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: Fluting
    transformed:
      ID3v2.3:
        TCON: Electronic
        TYER: '2013'
        TALB: Disjointed
        TRCK: '4'
        TPE1: Panix Hilton
        TIT2: Fluting
    all:
      TCON: Electronic
      TYER: '2013'
      TALB: Disjointed
      TRCK: '4'
      TPE1: Panix Hilton
      TIT2: Fluting
  - path: /mnt/usb32gb/organized-pax-nion/assets/albums/disjointed/5-stoekbroet.mp3
    audio: /assets/albums/disjointed/5-stoekbroet.mp3
    slug: disjointed/5-stoekbroet
    albumSlug: disjointed
    trackSlug: 5-stoekbroet
    coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/disjointed/5-stoekbroet.jpeg
    cover: /assets/albums/disjointed/5-stoekbroet.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 110.68081632653062
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TYER
          value: '2013'
        - id: TALB
          value: Disjointed
        - id: TRCK
          value: '5'
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: Stoekbroet
    quality:
      warnings: []
    common:
      track:
        'no': 5
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2013
      album: Disjointed
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: Stoekbroet
    transformed:
      ID3v2.3:
        TCON: Electronic
        TYER: '2013'
        TALB: Disjointed
        TRCK: '5'
        TPE1: Panix Hilton
        TIT2: Stoekbroet
    all:
      TCON: Electronic
      TYER: '2013'
      TALB: Disjointed
      TRCK: '5'
      TPE1: Panix Hilton
      TIT2: Stoekbroet
  - path: /mnt/usb32gb/organized-pax-nion/assets/albums/disjointed/6-pure-shite.mp3
    audio: /assets/albums/disjointed/6-pure-shite.mp3
    slug: disjointed/6-pure-shite
    albumSlug: disjointed
    trackSlug: 6-pure-shite
    coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/disjointed/6-pure-shite.jpeg
    cover: /assets/albums/disjointed/6-pure-shite.jpeg
    format:
      tagTypes:
        - ID3v2.3
      trackInfo: []
      lossless: false
      container: MPEG
      codec: MPEG 1 Layer 3
      sampleRate: 44100
      numberOfChannels: 2
      bitrate: 320000
      codecProfile: CBR
      tool: LAME3.98r
      duration: 118.0734693877551
    native:
      ID3v2.3:
        - id: TCON
          value: Electronic
        - id: TYER
          value: '2013'
        - id: TALB
          value: Disjointed
        - id: TRCK
          value: '6'
        - id: TPE1
          value: Panix Hilton
        - id: TIT2
          value: Pure Shite
    quality:
      warnings: []
    common:
      track:
        'no': 6
        of: null
      disk:
        'no': null
        of: null
      genre:
        - Electronic
      year: 2013
      album: Disjointed
      artists:
        - Panix Hilton
      artist: Panix Hilton
      title: Pure Shite
    transformed:
      ID3v2.3:
        TCON: Electronic
        TYER: '2013'
        TALB: Disjointed
        TRCK: '6'
        TPE1: Panix Hilton
        TIT2: Pure Shite
    all:
      TCON: Electronic
      TYER: '2013'
      TALB: Disjointed
      TRCK: '6'
      TPE1: Panix Hilton
      TIT2: Pure Shite
---
