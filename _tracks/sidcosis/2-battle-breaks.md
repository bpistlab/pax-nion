---
layout: track
path: /mnt/usb32gb/organized-pax-nion/assets/albums/sidcosis/2-battle-breaks.mp3
audio: /assets/albums/sidcosis/2-battle-breaks.mp3
slug: sidcosis/2-battle-breaks
albumSlug: sidcosis
trackSlug: 2-battle-breaks
coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/sidcosis/2-battle-breaks.jpeg
cover: /assets/albums/sidcosis/2-battle-breaks.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 1
  bitrate: 256000
  codecProfile: CBR
  tool: LAME3.97b
  duration: 107.57224489795918
native:
  ID3v2.3:
    - id: TCON
      value: Electronic
    - id: TRCK
      value: '2'
    - id: TALB
      value: SIDcosis
    - id: TPE1
      value: Skizm
    - id: TIT2
      value: Battle Breaks
    - id: TYER
      value: '2008'
quality:
  warnings: []
common:
  track:
    'no': 2
    of: null
  disk:
    'no': null
    of: null
  genre:
    - Electronic
  album: SIDcosis
  artists:
    - Skizm
  artist: Skizm
  title: Battle Breaks
  year: 2008
transformed:
  ID3v2.3:
    TCON: Electronic
    TRCK: '2'
    TALB: SIDcosis
    TPE1: Skizm
    TIT2: Battle Breaks
    TYER: '2008'
all:
  TCON: Electronic
  TRCK: '2'
  TALB: SIDcosis
  TPE1: Skizm
  TIT2: Battle Breaks
  TYER: '2008'
nextTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/sidcosis/3-depressure.mp3
  audio: /assets/albums/sidcosis/3-depressure.mp3
  slug: sidcosis/3-depressure
  albumSlug: sidcosis
  trackSlug: 3-depressure
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/sidcosis/3-depressure.jpeg
  cover: /assets/albums/sidcosis/3-depressure.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 1
    bitrate: 256000
    codecProfile: CBR
    tool: LAME3.97b
    duration: 132.04897959183674
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TIT2
        value: Depressure
      - id: TRCK
        value: '3'
      - id: TALB
        value: SIDcosis
      - id: TPE1
        value: Skizm
      - id: TYER
        value: '2008'
  quality:
    warnings: []
  common:
    track:
      'no': 3
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    title: Depressure
    album: SIDcosis
    artists:
      - Skizm
    artist: Skizm
    year: 2008
  transformed:
    ID3v2.3:
      TCON: Electronic
      TIT2: Depressure
      TRCK: '3'
      TALB: SIDcosis
      TPE1: Skizm
      TYER: '2008'
  all:
    TCON: Electronic
    TIT2: Depressure
    TRCK: '3'
    TALB: SIDcosis
    TPE1: Skizm
    TYER: '2008'
previousTrack:
  path: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/sidcosis/1-duracell-duck-invasion.mp3
  audio: /assets/albums/sidcosis/1-duracell-duck-invasion.mp3
  slug: sidcosis/1-duracell-duck-invasion
  albumSlug: sidcosis
  trackSlug: 1-duracell-duck-invasion
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/sidcosis/1-duracell-duck-invasion.jpeg
  cover: /assets/albums/sidcosis/1-duracell-duck-invasion.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 1
    bitrate: 128000
    codecProfile: CBR
    tool: LAME3.97b
    duration: 122.93224489795918
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2008'
      - id: TALB
        value: SIDcosis
      - id: TRCK
        value: '1'
      - id: TPE1
        value: Skizm
      - id: TIT2
        value: Duracell Duck Invasion
  quality:
    warnings: []
  common:
    track:
      'no': 1
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2008
    album: SIDcosis
    artists:
      - Skizm
    artist: Skizm
    title: Duracell Duck Invasion
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2008'
      TALB: SIDcosis
      TRCK: '1'
      TPE1: Skizm
      TIT2: Duracell Duck Invasion
  all:
    TCON: Electronic
    TYER: '2008'
    TALB: SIDcosis
    TRCK: '1'
    TPE1: Skizm
    TIT2: Duracell Duck Invasion
---
