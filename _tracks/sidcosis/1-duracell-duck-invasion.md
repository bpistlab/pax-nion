---
layout: track
path: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/sidcosis/1-duracell-duck-invasion.mp3
audio: /assets/albums/sidcosis/1-duracell-duck-invasion.mp3
slug: sidcosis/1-duracell-duck-invasion
albumSlug: sidcosis
trackSlug: 1-duracell-duck-invasion
coverPath: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/sidcosis/1-duracell-duck-invasion.jpeg
cover: /assets/albums/sidcosis/1-duracell-duck-invasion.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 1
  bitrate: 128000
  codecProfile: CBR
  tool: LAME3.97b
  duration: 122.93224489795918
native:
  ID3v2.3:
    - id: TCON
      value: Electronic
    - id: TYER
      value: '2008'
    - id: TALB
      value: SIDcosis
    - id: TRCK
      value: '1'
    - id: TPE1
      value: Skizm
    - id: TIT2
      value: Duracell Duck Invasion
quality:
  warnings: []
common:
  track:
    'no': 1
    of: null
  disk:
    'no': null
    of: null
  genre:
    - Electronic
  year: 2008
  album: SIDcosis
  artists:
    - Skizm
  artist: Skizm
  title: Duracell Duck Invasion
transformed:
  ID3v2.3:
    TCON: Electronic
    TYER: '2008'
    TALB: SIDcosis
    TRCK: '1'
    TPE1: Skizm
    TIT2: Duracell Duck Invasion
all:
  TCON: Electronic
  TYER: '2008'
  TALB: SIDcosis
  TRCK: '1'
  TPE1: Skizm
  TIT2: Duracell Duck Invasion
nextTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/sidcosis/2-battle-breaks.mp3
  audio: /assets/albums/sidcosis/2-battle-breaks.mp3
  slug: sidcosis/2-battle-breaks
  albumSlug: sidcosis
  trackSlug: 2-battle-breaks
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/sidcosis/2-battle-breaks.jpeg
  cover: /assets/albums/sidcosis/2-battle-breaks.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 1
    bitrate: 256000
    codecProfile: CBR
    tool: LAME3.97b
    duration: 107.57224489795918
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TRCK
        value: '2'
      - id: TALB
        value: SIDcosis
      - id: TPE1
        value: Skizm
      - id: TIT2
        value: Battle Breaks
      - id: TYER
        value: '2008'
  quality:
    warnings: []
  common:
    track:
      'no': 2
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    album: SIDcosis
    artists:
      - Skizm
    artist: Skizm
    title: Battle Breaks
    year: 2008
  transformed:
    ID3v2.3:
      TCON: Electronic
      TRCK: '2'
      TALB: SIDcosis
      TPE1: Skizm
      TIT2: Battle Breaks
      TYER: '2008'
  all:
    TCON: Electronic
    TRCK: '2'
    TALB: SIDcosis
    TPE1: Skizm
    TIT2: Battle Breaks
    TYER: '2008'
previousTrack:
  path: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/sidcosis/4-raw-like-sushi-drunk-like-clam.mp3
  audio: /assets/albums/sidcosis/4-raw-like-sushi-drunk-like-clam.mp3
  slug: sidcosis/4-raw-like-sushi-drunk-like-clam
  albumSlug: sidcosis
  trackSlug: 4-raw-like-sushi-drunk-like-clam
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/sidcosis/4-raw-like-sushi-drunk-like-clam.jpeg
  cover: /assets/albums/sidcosis/4-raw-like-sushi-drunk-like-clam.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 1
    bitrate: 256000
    codecProfile: CBR
    tool: LAME3.97b
    duration: 153.5477551020408
  native:
    ID3v2.3:
      - id: TRCK
        value: '4'
      - id: TCON
        value: Electronic
      - id: TALB
        value: SIDcosis
      - id: TPE1
        value: Skizm
      - id: TIT2
        value: Raw Like Sushi Drunk Like Clam
      - id: TYER
        value: '2008'
  quality:
    warnings: []
  common:
    track:
      'no': 4
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    album: SIDcosis
    artists:
      - Skizm
    artist: Skizm
    title: Raw Like Sushi Drunk Like Clam
    year: 2008
  transformed:
    ID3v2.3:
      TRCK: '4'
      TCON: Electronic
      TALB: SIDcosis
      TPE1: Skizm
      TIT2: Raw Like Sushi Drunk Like Clam
      TYER: '2008'
  all:
    TRCK: '4'
    TCON: Electronic
    TALB: SIDcosis
    TPE1: Skizm
    TIT2: Raw Like Sushi Drunk Like Clam
    TYER: '2008'
---
