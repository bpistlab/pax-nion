---
layout: track
path: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/8-montag001.mp3
audio: /assets/albums/netbook-lofi-beatheads/8-montag001.mp3
slug: netbook-lofi-beatheads/8-montag001
albumSlug: netbook-lofi-beatheads
trackSlug: 8-montag001
coverPath: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/8-montag001.jpeg
cover: /assets/albums/netbook-lofi-beatheads/8-montag001.jpeg
format:
  tagTypes:
    - ID3v2.4
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 320000
  codecProfile: CBR
  tool: LAME3.98r
  duration: 117.39428571428572
native:
  ID3v2.4:
    - id: TCON
      value: Electronic
    - id: TDRC
      value: '2011'
    - id: TYER
      value: '2011'
    - id: TALB
      value: Netbook Lofi Beatheads
    - id: TRCK
      value: '8'
    - id: TPE1
      value: Panix Hilton
    - id: TIT2
      value: Montag001
quality:
  warnings: []
common:
  track:
    'no': 8
    of: null
  disk:
    'no': null
    of: null
  genre:
    - Electronic
  year: 2011
  date: '2011'
  album: Netbook Lofi Beatheads
  artists:
    - Panix Hilton
  artist: Panix Hilton
  title: Montag001
transformed:
  ID3v2.4:
    TCON: Electronic
    TDRC: '2011'
    TYER: '2011'
    TALB: Netbook Lofi Beatheads
    TRCK: '8'
    TPE1: Panix Hilton
    TIT2: Montag001
all:
  TCON: Electronic
  TDRC: '2011'
  TYER: '2011'
  TALB: Netbook Lofi Beatheads
  TRCK: '8'
  TPE1: Panix Hilton
  TIT2: Montag001
nextTrack:
  path: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/9-neckhurt.mp3
  audio: /assets/albums/netbook-lofi-beatheads/9-neckhurt.mp3
  slug: netbook-lofi-beatheads/9-neckhurt
  albumSlug: netbook-lofi-beatheads
  trackSlug: 9-neckhurt
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/9-neckhurt.jpeg
  cover: /assets/albums/netbook-lofi-beatheads/9-neckhurt.jpeg
  format:
    tagTypes:
      - ID3v2.4
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 172.8522448979592
  native:
    ID3v2.4:
      - id: TCON
        value: Electronic
      - id: TDRC
        value: '2011'
      - id: TYER
        value: '2011'
      - id: TALB
        value: Netbook Lofi Beatheads
      - id: TRCK
        value: '9'
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: Neckhurt
  quality:
    warnings: []
  common:
    track:
      'no': 9
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2011
    date: '2011'
    album: Netbook Lofi Beatheads
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: Neckhurt
  transformed:
    ID3v2.4:
      TCON: Electronic
      TDRC: '2011'
      TYER: '2011'
      TALB: Netbook Lofi Beatheads
      TRCK: '9'
      TPE1: Panix Hilton
      TIT2: Neckhurt
  all:
    TCON: Electronic
    TDRC: '2011'
    TYER: '2011'
    TALB: Netbook Lofi Beatheads
    TRCK: '9'
    TPE1: Panix Hilton
    TIT2: Neckhurt
previousTrack:
  path: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/7-montag000.mp3
  audio: /assets/albums/netbook-lofi-beatheads/7-montag000.mp3
  slug: netbook-lofi-beatheads/7-montag000
  albumSlug: netbook-lofi-beatheads
  trackSlug: 7-montag000
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/7-montag000.jpeg
  cover: /assets/albums/netbook-lofi-beatheads/7-montag000.jpeg
  format:
    tagTypes:
      - ID3v2.4
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 128.05224489795918
  native:
    ID3v2.4:
      - id: TCON
        value: Electronic
      - id: TDRC
        value: '2011'
      - id: TYER
        value: '2011'
      - id: TALB
        value: Netbook Lofi Beatheads
      - id: TRCK
        value: '7'
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: Montag000
  quality:
    warnings: []
  common:
    track:
      'no': 7
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2011
    date: '2011'
    album: Netbook Lofi Beatheads
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: Montag000
  transformed:
    ID3v2.4:
      TCON: Electronic
      TDRC: '2011'
      TYER: '2011'
      TALB: Netbook Lofi Beatheads
      TRCK: '7'
      TPE1: Panix Hilton
      TIT2: Montag000
  all:
    TCON: Electronic
    TDRC: '2011'
    TYER: '2011'
    TALB: Netbook Lofi Beatheads
    TRCK: '7'
    TPE1: Panix Hilton
    TIT2: Montag000
---
