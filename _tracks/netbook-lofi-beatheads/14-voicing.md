---
layout: track
path: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/14-voicing.mp3
audio: /assets/albums/netbook-lofi-beatheads/14-voicing.mp3
slug: netbook-lofi-beatheads/14-voicing
albumSlug: netbook-lofi-beatheads
trackSlug: 14-voicing
coverPath: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/14-voicing.jpeg
cover: /assets/albums/netbook-lofi-beatheads/14-voicing.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 320000
  codecProfile: CBR
  tool: LAME3.98r
  duration: 101.79918367346939
native:
  ID3v2.3:
    - id: TCON
      value: Electronic
    - id: TYER
      value: '2011'
    - id: TALB
      value: Netbook Lofi Beatheads
    - id: TRCK
      value: '14'
    - id: TPE1
      value: Panix Hilton
    - id: TIT2
      value: Voicing
quality:
  warnings: []
common:
  track:
    'no': 14
    of: null
  disk:
    'no': null
    of: null
  genre:
    - Electronic
  year: 2011
  album: Netbook Lofi Beatheads
  artists:
    - Panix Hilton
  artist: Panix Hilton
  title: Voicing
transformed:
  ID3v2.3:
    TCON: Electronic
    TYER: '2011'
    TALB: Netbook Lofi Beatheads
    TRCK: '14'
    TPE1: Panix Hilton
    TIT2: Voicing
all:
  TCON: Electronic
  TYER: '2011'
  TALB: Netbook Lofi Beatheads
  TRCK: '14'
  TPE1: Panix Hilton
  TIT2: Voicing
nextTrack:
  path: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/1-wtf.mp3
  audio: /assets/albums/netbook-lofi-beatheads/1-wtf.mp3
  slug: netbook-lofi-beatheads/1-wtf
  albumSlug: netbook-lofi-beatheads
  trackSlug: 1-wtf
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/1-wtf.jpeg
  cover: /assets/albums/netbook-lofi-beatheads/1-wtf.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 130.11591836734695
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2011'
      - id: TALB
        value: Netbook Lofi Beatheads
      - id: TRCK
        value: '1'
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: WTF
  quality:
    warnings: []
  common:
    track:
      'no': 1
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2011
    album: Netbook Lofi Beatheads
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: WTF
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2011'
      TALB: Netbook Lofi Beatheads
      TRCK: '1'
      TPE1: Panix Hilton
      TIT2: WTF
  all:
    TCON: Electronic
    TYER: '2011'
    TALB: Netbook Lofi Beatheads
    TRCK: '1'
    TPE1: Panix Hilton
    TIT2: WTF
previousTrack:
  path: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/13-waapper.mp3
  audio: /assets/albums/netbook-lofi-beatheads/13-waapper.mp3
  slug: netbook-lofi-beatheads/13-waapper
  albumSlug: netbook-lofi-beatheads
  trackSlug: 13-waapper
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/13-waapper.jpeg
  cover: /assets/albums/netbook-lofi-beatheads/13-waapper.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 115.25224489795919
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2011'
      - id: TALB
        value: Netbook Lofi Beatheads
      - id: TRCK
        value: '13'
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: Waapper
  quality:
    warnings: []
  common:
    track:
      'no': 13
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2011
    album: Netbook Lofi Beatheads
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: Waapper
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2011'
      TALB: Netbook Lofi Beatheads
      TRCK: '13'
      TPE1: Panix Hilton
      TIT2: Waapper
  all:
    TCON: Electronic
    TYER: '2011'
    TALB: Netbook Lofi Beatheads
    TRCK: '13'
    TPE1: Panix Hilton
    TIT2: Waapper
---
