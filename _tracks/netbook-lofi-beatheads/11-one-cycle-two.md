---
layout: track
path: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/11-one-cycle-two.mp3
audio: /assets/albums/netbook-lofi-beatheads/11-one-cycle-two.mp3
slug: netbook-lofi-beatheads/11-one-cycle-two
albumSlug: netbook-lofi-beatheads
trackSlug: 11-one-cycle-two
coverPath: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/11-one-cycle-two.jpeg
cover: /assets/albums/netbook-lofi-beatheads/11-one-cycle-two.jpeg
format:
  tagTypes:
    - ID3v2.4
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 320000
  codecProfile: CBR
  tool: LAME3.98r
  duration: 157.77959183673468
native:
  ID3v2.4:
    - id: TCON
      value: Electronic
    - id: TDRC
      value: '2011'
    - id: TYER
      value: '2011'
    - id: TALB
      value: Netbook Lofi Beatheads
    - id: TRCK
      value: '11'
    - id: TPE1
      value: Panix Hilton
    - id: TIT2
      value: One Cycle Two
quality:
  warnings: []
common:
  track:
    'no': 11
    of: null
  disk:
    'no': null
    of: null
  genre:
    - Electronic
  year: 2011
  date: '2011'
  album: Netbook Lofi Beatheads
  artists:
    - Panix Hilton
  artist: Panix Hilton
  title: One Cycle Two
transformed:
  ID3v2.4:
    TCON: Electronic
    TDRC: '2011'
    TYER: '2011'
    TALB: Netbook Lofi Beatheads
    TRCK: '11'
    TPE1: Panix Hilton
    TIT2: One Cycle Two
all:
  TCON: Electronic
  TDRC: '2011'
  TYER: '2011'
  TALB: Netbook Lofi Beatheads
  TRCK: '11'
  TPE1: Panix Hilton
  TIT2: One Cycle Two
nextTrack:
  path: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/12-show-you-love.mp3
  audio: /assets/albums/netbook-lofi-beatheads/12-show-you-love.mp3
  slug: netbook-lofi-beatheads/12-show-you-love
  albumSlug: netbook-lofi-beatheads
  trackSlug: 12-show-you-love
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/12-show-you-love.jpeg
  cover: /assets/albums/netbook-lofi-beatheads/12-show-you-love.jpeg
  format:
    tagTypes:
      - ID3v2.4
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 145.97224489795917
  native:
    ID3v2.4:
      - id: TCON
        value: Electronic
      - id: TDRC
        value: '2011'
      - id: TYER
        value: '2011'
      - id: TALB
        value: Netbook Lofi Beatheads
      - id: TRCK
        value: '12'
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: Show You Love
  quality:
    warnings: []
  common:
    track:
      'no': 12
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2011
    date: '2011'
    album: Netbook Lofi Beatheads
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: Show You Love
  transformed:
    ID3v2.4:
      TCON: Electronic
      TDRC: '2011'
      TYER: '2011'
      TALB: Netbook Lofi Beatheads
      TRCK: '12'
      TPE1: Panix Hilton
      TIT2: Show You Love
  all:
    TCON: Electronic
    TDRC: '2011'
    TYER: '2011'
    TALB: Netbook Lofi Beatheads
    TRCK: '12'
    TPE1: Panix Hilton
    TIT2: Show You Love
previousTrack:
  path: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/10-one-cycle.mp3
  audio: /assets/albums/netbook-lofi-beatheads/10-one-cycle.mp3
  slug: netbook-lofi-beatheads/10-one-cycle
  albumSlug: netbook-lofi-beatheads
  trackSlug: 10-one-cycle
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/10-one-cycle.jpeg
  cover: /assets/albums/netbook-lofi-beatheads/10-one-cycle.jpeg
  format:
    tagTypes:
      - ID3v2.4
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 93.17877551020408
  native:
    ID3v2.4:
      - id: TCON
        value: Electronic
      - id: TDRC
        value: '2011'
      - id: TYER
        value: '2011'
      - id: TALB
        value: Netbook Lofi Beatheads
      - id: TRCK
        value: '10'
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: One Cycle
  quality:
    warnings: []
  common:
    track:
      'no': 10
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2011
    date: '2011'
    album: Netbook Lofi Beatheads
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: One Cycle
  transformed:
    ID3v2.4:
      TCON: Electronic
      TDRC: '2011'
      TYER: '2011'
      TALB: Netbook Lofi Beatheads
      TRCK: '10'
      TPE1: Panix Hilton
      TIT2: One Cycle
  all:
    TCON: Electronic
    TDRC: '2011'
    TYER: '2011'
    TALB: Netbook Lofi Beatheads
    TRCK: '10'
    TPE1: Panix Hilton
    TIT2: One Cycle
---
