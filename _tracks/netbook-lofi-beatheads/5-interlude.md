---
layout: track
path: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/5-interlude.mp3
audio: /assets/albums/netbook-lofi-beatheads/5-interlude.mp3
slug: netbook-lofi-beatheads/5-interlude
albumSlug: netbook-lofi-beatheads
trackSlug: 5-interlude
coverPath: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/5-interlude.jpeg
cover: /assets/albums/netbook-lofi-beatheads/5-interlude.jpeg
format:
  tagTypes:
    - ID3v2.4
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 320000
  codecProfile: CBR
  tool: LAME3.98r
  duration: 121.10367346938776
native:
  ID3v2.4:
    - id: TCON
      value: Electronic
    - id: TDRC
      value: '2011'
    - id: TYER
      value: '2011'
    - id: TALB
      value: Netbook Lofi Beatheads
    - id: TRCK
      value: '5'
    - id: TPE1
      value: Panix Hilton
    - id: TIT2
      value: Interlude
quality:
  warnings: []
common:
  track:
    'no': 5
    of: null
  disk:
    'no': null
    of: null
  genre:
    - Electronic
  year: 2011
  date: '2011'
  album: Netbook Lofi Beatheads
  artists:
    - Panix Hilton
  artist: Panix Hilton
  title: Interlude
transformed:
  ID3v2.4:
    TCON: Electronic
    TDRC: '2011'
    TYER: '2011'
    TALB: Netbook Lofi Beatheads
    TRCK: '5'
    TPE1: Panix Hilton
    TIT2: Interlude
all:
  TCON: Electronic
  TDRC: '2011'
  TYER: '2011'
  TALB: Netbook Lofi Beatheads
  TRCK: '5'
  TPE1: Panix Hilton
  TIT2: Interlude
nextTrack:
  path: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/6-lukewarm.mp3
  audio: /assets/albums/netbook-lofi-beatheads/6-lukewarm.mp3
  slug: netbook-lofi-beatheads/6-lukewarm
  albumSlug: netbook-lofi-beatheads
  trackSlug: 6-lukewarm
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/6-lukewarm.jpeg
  cover: /assets/albums/netbook-lofi-beatheads/6-lukewarm.jpeg
  format:
    tagTypes:
      - ID3v2.4
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 138.2922448979592
  native:
    ID3v2.4:
      - id: TCON
        value: Electronic
      - id: TDRC
        value: '2011'
      - id: TYER
        value: '2011'
      - id: TALB
        value: Netbook Lofi Beatheads
      - id: TRCK
        value: '6'
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: Lukewarm
  quality:
    warnings: []
  common:
    track:
      'no': 6
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2011
    date: '2011'
    album: Netbook Lofi Beatheads
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: Lukewarm
  transformed:
    ID3v2.4:
      TCON: Electronic
      TDRC: '2011'
      TYER: '2011'
      TALB: Netbook Lofi Beatheads
      TRCK: '6'
      TPE1: Panix Hilton
      TIT2: Lukewarm
  all:
    TCON: Electronic
    TDRC: '2011'
    TYER: '2011'
    TALB: Netbook Lofi Beatheads
    TRCK: '6'
    TPE1: Panix Hilton
    TIT2: Lukewarm
previousTrack:
  path: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/4-frankentune.mp3
  audio: /assets/albums/netbook-lofi-beatheads/4-frankentune.mp3
  slug: netbook-lofi-beatheads/4-frankentune
  albumSlug: netbook-lofi-beatheads
  trackSlug: 4-frankentune
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/4-frankentune.jpeg
  cover: /assets/albums/netbook-lofi-beatheads/4-frankentune.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 328.385306122449
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2011'
      - id: TALB
        value: Netbook Lofi Beatheads
      - id: TRCK
        value: '4'
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: Frankentune
  quality:
    warnings: []
  common:
    track:
      'no': 4
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2011
    album: Netbook Lofi Beatheads
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: Frankentune
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2011'
      TALB: Netbook Lofi Beatheads
      TRCK: '4'
      TPE1: Panix Hilton
      TIT2: Frankentune
  all:
    TCON: Electronic
    TYER: '2011'
    TALB: Netbook Lofi Beatheads
    TRCK: '4'
    TPE1: Panix Hilton
    TIT2: Frankentune
---
