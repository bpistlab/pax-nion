---
layout: track
path: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/3-conscious.mp3
audio: /assets/albums/netbook-lofi-beatheads/3-conscious.mp3
slug: netbook-lofi-beatheads/3-conscious
albumSlug: netbook-lofi-beatheads
trackSlug: 3-conscious
coverPath: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/3-conscious.jpeg
cover: /assets/albums/netbook-lofi-beatheads/3-conscious.jpeg
format:
  tagTypes:
    - ID3v2.4
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 320000
  codecProfile: CBR
  tool: LAME3.98r
  duration: 115.25224489795919
native:
  ID3v2.4:
    - id: TCON
      value: Electronic
    - id: TDRC
      value: '2011'
    - id: TYER
      value: '2011'
    - id: TALB
      value: Netbook Lofi Beatheads
    - id: TRCK
      value: '3'
    - id: TPE1
      value: Panix Hilton
    - id: TIT2
      value: Conscious
quality:
  warnings: []
common:
  track:
    'no': 3
    of: null
  disk:
    'no': null
    of: null
  genre:
    - Electronic
  year: 2011
  date: '2011'
  album: Netbook Lofi Beatheads
  artists:
    - Panix Hilton
  artist: Panix Hilton
  title: Conscious
transformed:
  ID3v2.4:
    TCON: Electronic
    TDRC: '2011'
    TYER: '2011'
    TALB: Netbook Lofi Beatheads
    TRCK: '3'
    TPE1: Panix Hilton
    TIT2: Conscious
all:
  TCON: Electronic
  TDRC: '2011'
  TYER: '2011'
  TALB: Netbook Lofi Beatheads
  TRCK: '3'
  TPE1: Panix Hilton
  TIT2: Conscious
nextTrack:
  path: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/4-frankentune.mp3
  audio: /assets/albums/netbook-lofi-beatheads/4-frankentune.mp3
  slug: netbook-lofi-beatheads/4-frankentune
  albumSlug: netbook-lofi-beatheads
  trackSlug: 4-frankentune
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/4-frankentune.jpeg
  cover: /assets/albums/netbook-lofi-beatheads/4-frankentune.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 328.385306122449
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2011'
      - id: TALB
        value: Netbook Lofi Beatheads
      - id: TRCK
        value: '4'
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: Frankentune
  quality:
    warnings: []
  common:
    track:
      'no': 4
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2011
    album: Netbook Lofi Beatheads
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: Frankentune
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2011'
      TALB: Netbook Lofi Beatheads
      TRCK: '4'
      TPE1: Panix Hilton
      TIT2: Frankentune
  all:
    TCON: Electronic
    TYER: '2011'
    TALB: Netbook Lofi Beatheads
    TRCK: '4'
    TPE1: Panix Hilton
    TIT2: Frankentune
previousTrack:
  path: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/2-blockerad.mp3
  audio: /assets/albums/netbook-lofi-beatheads/2-blockerad.mp3
  slug: netbook-lofi-beatheads/2-blockerad
  albumSlug: netbook-lofi-beatheads
  trackSlug: 2-blockerad
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/netbook-lofi-beatheads/2-blockerad.jpeg
  cover: /assets/albums/netbook-lofi-beatheads/2-blockerad.jpeg
  format:
    tagTypes:
      - ID3v2.4
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 133.90367346938777
  native:
    ID3v2.4:
      - id: TCON
        value: Electronic
      - id: TDRC
        value: '2011'
      - id: TYER
        value: '2011'
      - id: TALB
        value: Netbook Lofi Beatheads
      - id: TRCK
        value: '2'
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: Blockerad
  quality:
    warnings: []
  common:
    track:
      'no': 2
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2011
    date: '2011'
    album: Netbook Lofi Beatheads
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: Blockerad
  transformed:
    ID3v2.4:
      TCON: Electronic
      TDRC: '2011'
      TYER: '2011'
      TALB: Netbook Lofi Beatheads
      TRCK: '2'
      TPE1: Panix Hilton
      TIT2: Blockerad
  all:
    TCON: Electronic
    TDRC: '2011'
    TYER: '2011'
    TALB: Netbook Lofi Beatheads
    TRCK: '2'
    TPE1: Panix Hilton
    TIT2: Blockerad
---
