---
layout: track
path: /mnt/usb32gb/organized-pax-nion/assets/albums/mula-ep/1-mula.mp3
audio: /assets/albums/mula-ep/1-mula.mp3
slug: mula-ep/1-mula
albumSlug: mula-ep
trackSlug: 1-mula
coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/mula-ep/1-mula.jpeg
cover: /assets/albums/mula-ep/1-mula.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 1
  bitrate: 112000
  codecProfile: CBR
  numberOfSamples: 7485696
  duration: 169.74367346938774
native:
  ID3v2.3:
    - id: TCON
      value: Electronic
    - id: TYER
      value: '2001'
    - id: TALB
      value: Mula EP
    - id: TPE1
      value: Panix
    - id: TIT2
      value: Mula
    - id: TRCK
      value: '1'
quality:
  warnings: []
common:
  track:
    'no': 1
    of: null
  disk:
    'no': null
    of: null
  genre:
    - Electronic
  year: 2001
  album: Mula EP
  artists:
    - Panix
  artist: Panix
  title: Mula
transformed:
  ID3v2.3:
    TCON: Electronic
    TYER: '2001'
    TALB: Mula EP
    TPE1: Panix
    TIT2: Mula
    TRCK: '1'
all:
  TCON: Electronic
  TYER: '2001'
  TALB: Mula EP
  TPE1: Panix
  TIT2: Mula
  TRCK: '1'
nextTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/mula-ep/2-fax.mp3
  audio: /assets/albums/mula-ep/2-fax.mp3
  slug: mula-ep/2-fax
  albumSlug: mula-ep
  trackSlug: 2-fax
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/mula-ep/2-fax.jpeg
  cover: /assets/albums/mula-ep/2-fax.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 1
    bitrate: 112000
    codecProfile: CBR
    numberOfSamples: 7785216
    duration: 176.53551020408165
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2001'
      - id: TALB
        value: Mula EP
      - id: TPE1
        value: Panix
      - id: TIT2
        value: Fax
      - id: TRCK
        value: '2'
  quality:
    warnings: []
  common:
    track:
      'no': 2
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2001
    album: Mula EP
    artists:
      - Panix
    artist: Panix
    title: Fax
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2001'
      TALB: Mula EP
      TPE1: Panix
      TIT2: Fax
      TRCK: '2'
  all:
    TCON: Electronic
    TYER: '2001'
    TALB: Mula EP
    TPE1: Panix
    TIT2: Fax
    TRCK: '2'
previousTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/mula-ep/3-cowboy.mp3
  audio: /assets/albums/mula-ep/3-cowboy.mp3
  slug: mula-ep/3-cowboy
  albumSlug: mula-ep
  trackSlug: 3-cowboy
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/mula-ep/3-cowboy.jpeg
  cover: /assets/albums/mula-ep/3-cowboy.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 1
    bitrate: 112000
    codecProfile: CBR
    numberOfSamples: 3255552
    duration: 73.82204081632653
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2001'
      - id: TALB
        value: Mula EP
      - id: TPE1
        value: Panix
      - id: TIT2
        value: Cowboy
      - id: TRCK
        value: '3'
  quality:
    warnings: []
  common:
    track:
      'no': 3
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2001
    album: Mula EP
    artists:
      - Panix
    artist: Panix
    title: Cowboy
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2001'
      TALB: Mula EP
      TPE1: Panix
      TIT2: Cowboy
      TRCK: '3'
  all:
    TCON: Electronic
    TYER: '2001'
    TALB: Mula EP
    TPE1: Panix
    TIT2: Cowboy
    TRCK: '3'
---
