---
layout: track
path: /mnt/usb32gb/organized-pax-nion/assets/albums/andra-stuk/3-temptemp.mp3
audio: /assets/albums/andra-stuk/3-temptemp.mp3
slug: andra-stuk/3-temptemp
albumSlug: andra-stuk
trackSlug: 3-temptemp
coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/andra-stuk/3-temptemp.jpeg
cover: /assets/albums/andra-stuk/3-temptemp.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 256000
  codecProfile: CBR
  tool: LAME3.98r
  duration: 65.35836734693878
native:
  ID3v2.3:
    - id: TCON
      value: Electronic
    - id: TYER
      value: '2010'
    - id: TALB
      value: Andra Stuk
    - id: TRCK
      value: '3'
    - id: TPE1
      value: Panixonik
    - id: TIT2
      value: Temptemp
quality:
  warnings: []
common:
  track:
    'no': 3
    of: null
  disk:
    'no': null
    of: null
  genre:
    - Electronic
  year: 2010
  album: Andra Stuk
  artists:
    - Panixonik
  artist: Panixonik
  title: Temptemp
transformed:
  ID3v2.3:
    TCON: Electronic
    TYER: '2010'
    TALB: Andra Stuk
    TRCK: '3'
    TPE1: Panixonik
    TIT2: Temptemp
all:
  TCON: Electronic
  TYER: '2010'
  TALB: Andra Stuk
  TRCK: '3'
  TPE1: Panixonik
  TIT2: Temptemp
nextTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/andra-stuk/4-matlagning.mp3
  audio: /assets/albums/andra-stuk/4-matlagning.mp3
  slug: andra-stuk/4-matlagning
  albumSlug: andra-stuk
  trackSlug: 4-matlagning
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/andra-stuk/4-matlagning.jpeg
  cover: /assets/albums/andra-stuk/4-matlagning.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 256000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 58.984489795918364
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2010'
      - id: TALB
        value: Andra Stuk
      - id: TRCK
        value: '4'
      - id: TPE1
        value: Panixonik
      - id: TIT2
        value: Matlagning
  quality:
    warnings: []
  common:
    track:
      'no': 4
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2010
    album: Andra Stuk
    artists:
      - Panixonik
    artist: Panixonik
    title: Matlagning
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2010'
      TALB: Andra Stuk
      TRCK: '4'
      TPE1: Panixonik
      TIT2: Matlagning
  all:
    TCON: Electronic
    TYER: '2010'
    TALB: Andra Stuk
    TRCK: '4'
    TPE1: Panixonik
    TIT2: Matlagning
previousTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/andra-stuk/2-brustabletter.mp3
  audio: /assets/albums/andra-stuk/2-brustabletter.mp3
  slug: andra-stuk/2-brustabletter
  albumSlug: andra-stuk
  trackSlug: 2-brustabletter
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/andra-stuk/2-brustabletter.jpeg
  cover: /assets/albums/andra-stuk/2-brustabletter.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 256000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 151.92816326530613
  native:
    ID3v2.3:
      - id: TALB
        value: Andra Stuk
      - id: TRCK
        value: '2'
      - id: TCON
        value: Electronic
      - id: TPE1
        value: Panixonik
      - id: TIT2
        value: Brustabletter
      - id: TYER
        value: '2010'
  quality:
    warnings: []
  common:
    track:
      'no': 2
      of: null
    disk:
      'no': null
      of: null
    album: Andra Stuk
    genre:
      - Electronic
    artists:
      - Panixonik
    artist: Panixonik
    title: Brustabletter
    year: 2010
  transformed:
    ID3v2.3:
      TALB: Andra Stuk
      TRCK: '2'
      TCON: Electronic
      TPE1: Panixonik
      TIT2: Brustabletter
      TYER: '2010'
  all:
    TALB: Andra Stuk
    TRCK: '2'
    TCON: Electronic
    TPE1: Panixonik
    TIT2: Brustabletter
    TYER: '2010'
---
