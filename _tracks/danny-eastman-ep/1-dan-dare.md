---
layout: track
path: /mnt/usb32gb/organized-pax-nion/assets/albums/danny-eastman-ep/1-dan-dare.mp3
audio: /assets/albums/danny-eastman-ep/1-dan-dare.mp3
slug: danny-eastman-ep/1-dan-dare
albumSlug: danny-eastman-ep
trackSlug: 1-dan-dare
coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/danny-eastman-ep/1-dan-dare.jpeg
cover: /assets/albums/danny-eastman-ep/1-dan-dare.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 320000
  codecProfile: CBR
  tool: LAME 3.97UU
  duration: 159.3730612244898
native:
  ID3v2.3:
    - id: TRCK
      value: '1'
    - id: TCON
      value: Electronic
    - id: TALB
      value: Danny Eastman EP
    - id: TPE1
      value: Panixonik
    - id: TIT2
      value: Dan Dare
    - id: TYER
      value: '2009'
quality:
  warnings: []
common:
  track:
    'no': 1
    of: null
  disk:
    'no': null
    of: null
  genre:
    - Electronic
  album: Danny Eastman EP
  artists:
    - Panixonik
  artist: Panixonik
  title: Dan Dare
  year: 2009
transformed:
  ID3v2.3:
    TRCK: '1'
    TCON: Electronic
    TALB: Danny Eastman EP
    TPE1: Panixonik
    TIT2: Dan Dare
    TYER: '2009'
all:
  TRCK: '1'
  TCON: Electronic
  TALB: Danny Eastman EP
  TPE1: Panixonik
  TIT2: Dan Dare
  TYER: '2009'
nextTrack: &ref_0
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/danny-eastman-ep/2-eastman.mp3
  audio: /assets/albums/danny-eastman-ep/2-eastman.mp3
  slug: danny-eastman-ep/2-eastman
  albumSlug: danny-eastman-ep
  trackSlug: 2-eastman
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/danny-eastman-ep/2-eastman.jpeg
  cover: /assets/albums/danny-eastman-ep/2-eastman.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME 3.97UU
    duration: 185.9918367346939
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2009'
      - id: TALB
        value: Danny Eastman EP
      - id: TPE1
        value: Panixonik
      - id: TIT2
        value: Eastman
      - id: TRCK
        value: '2'
  quality:
    warnings: []
  common:
    track:
      'no': 2
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2009
    album: Danny Eastman EP
    artists:
      - Panixonik
    artist: Panixonik
    title: Eastman
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2009'
      TALB: Danny Eastman EP
      TPE1: Panixonik
      TIT2: Eastman
      TRCK: '2'
  all:
    TCON: Electronic
    TYER: '2009'
    TALB: Danny Eastman EP
    TPE1: Panixonik
    TIT2: Eastman
    TRCK: '2'
previousTrack: *ref_0
---
