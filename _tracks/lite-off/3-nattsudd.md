---
layout: track
path: /mnt/usb32gb/organized-pax-nion/assets/albums/lite-off/3-nattsudd.mp3
audio: /assets/albums/lite-off/3-nattsudd.mp3
slug: lite-off/3-nattsudd
albumSlug: lite-off
trackSlug: 3-nattsudd
coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/lite-off/3-nattsudd.jpeg
cover: /assets/albums/lite-off/3-nattsudd.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 320000
  codecProfile: CBR
  tool: 'LAME3.97 '
  duration: 101.25061224489797
native:
  ID3v2.3:
    - id: TCON
      value: Electronic
    - id: TYER
      value: '2010'
    - id: TALB
      value: Lite Off
    - id: TRCK
      value: '3'
    - id: TPE1
      value: Panixon
    - id: TIT2
      value: Nattsudd
quality:
  warnings: []
common:
  track:
    'no': 3
    of: null
  disk:
    'no': null
    of: null
  genre:
    - Electronic
  year: 2010
  album: Lite Off
  artists:
    - Panixon
  artist: Panixon
  title: Nattsudd
transformed:
  ID3v2.3:
    TCON: Electronic
    TYER: '2010'
    TALB: Lite Off
    TRCK: '3'
    TPE1: Panixon
    TIT2: Nattsudd
all:
  TCON: Electronic
  TYER: '2010'
  TALB: Lite Off
  TRCK: '3'
  TPE1: Panixon
  TIT2: Nattsudd
nextTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/lite-off/1-no-disco-day.mp3
  audio: /assets/albums/lite-off/1-no-disco-day.mp3
  slug: lite-off/1-no-disco-day
  albumSlug: lite-off
  trackSlug: 1-no-disco-day
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/lite-off/1-no-disco-day.jpeg
  cover: /assets/albums/lite-off/1-no-disco-day.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 256000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 68.4930612244898
  native:
    ID3v2.3:
      - id: TALB
        value: Lite Off
      - id: TRCK
        value: '1'
      - id: TCON
        value: Electronic
      - id: TPE1
        value: Panixonik
      - id: TIT2
        value: No Disco Day
      - id: TYER
        value: '2010'
  quality:
    warnings: []
  common:
    track:
      'no': 1
      of: null
    disk:
      'no': null
      of: null
    album: Lite Off
    genre:
      - Electronic
    artists:
      - Panixonik
    artist: Panixonik
    title: No Disco Day
    year: 2010
  transformed:
    ID3v2.3:
      TALB: Lite Off
      TRCK: '1'
      TCON: Electronic
      TPE1: Panixonik
      TIT2: No Disco Day
      TYER: '2010'
  all:
    TALB: Lite Off
    TRCK: '1'
    TCON: Electronic
    TPE1: Panixonik
    TIT2: No Disco Day
    TYER: '2010'
previousTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/lite-off/2-invaders.mp3
  audio: /assets/albums/lite-off/2-invaders.mp3
  slug: lite-off/2-invaders
  albumSlug: lite-off
  trackSlug: 2-invaders
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/lite-off/2-invaders.jpeg
  cover: /assets/albums/lite-off/2-invaders.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME 3.97UU
    duration: 167.57551020408164
  native:
    ID3v2.3:
      - id: TRCK
        value: '2'
      - id: TCON
        value: Electronic
      - id: TALB
        value: Lite Off
      - id: TPE1
        value: Panixonik
      - id: TIT2
        value: Invaders
      - id: TYER
        value: '2010'
  quality:
    warnings: []
  common:
    track:
      'no': 2
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    album: Lite Off
    artists:
      - Panixonik
    artist: Panixonik
    title: Invaders
    year: 2010
  transformed:
    ID3v2.3:
      TRCK: '2'
      TCON: Electronic
      TALB: Lite Off
      TPE1: Panixonik
      TIT2: Invaders
      TYER: '2010'
  all:
    TRCK: '2'
    TCON: Electronic
    TALB: Lite Off
    TPE1: Panixonik
    TIT2: Invaders
    TYER: '2010'
---
