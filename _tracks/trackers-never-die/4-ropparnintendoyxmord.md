---
layout: track
path: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/trackers-never-die/4-ropparnintendoyxmord.mp3
audio: /assets/albums/trackers-never-die/4-ropparnintendoyxmord.mp3
slug: trackers-never-die/4-ropparnintendoyxmord
albumSlug: trackers-never-die
trackSlug: 4-ropparnintendoyxmord
coverPath: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/trackers-never-die/4-ropparnintendoyxmord.jpeg
cover: /assets/albums/trackers-never-die/4-ropparnintendoyxmord.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 192000
  codecProfile: CBR
  tool: 'LAME3.95 '
  duration: 78.83755102040816
native:
  ID3v2.3:
    - id: TCON
      value: Electronic
    - id: TALB
      value: Trackers Never Die
    - id: TRCK
      value: '4'
    - id: TPE1
      value: Panix
    - id: TIT2
      value: Ropparnintendoyxmord
    - id: TYER
      value: '2005'
quality:
  warnings: []
common:
  track:
    'no': 4
    of: null
  disk:
    'no': null
    of: null
  genre:
    - Electronic
  album: Trackers Never Die
  artists:
    - Panix
  artist: Panix
  title: Ropparnintendoyxmord
  year: 2005
transformed:
  ID3v2.3:
    TCON: Electronic
    TALB: Trackers Never Die
    TRCK: '4'
    TPE1: Panix
    TIT2: Ropparnintendoyxmord
    TYER: '2005'
all:
  TCON: Electronic
  TALB: Trackers Never Die
  TRCK: '4'
  TPE1: Panix
  TIT2: Ropparnintendoyxmord
  TYER: '2005'
nextTrack:
  path: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/trackers-never-die/5-shufflethatass.mp3
  audio: /assets/albums/trackers-never-die/5-shufflethatass.mp3
  slug: trackers-never-die/5-shufflethatass
  albumSlug: trackers-never-die
  trackSlug: 5-shufflethatass
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/trackers-never-die/5-shufflethatass.jpeg
  cover: /assets/albums/trackers-never-die/5-shufflethatass.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 192000
    codecProfile: CBR
    tool: 'LAME3.95 '
    duration: 76.95673469387755
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TALB
        value: Trackers Never Die
      - id: TRCK
        value: '05'
      - id: TPE1
        value: Panix
      - id: TIT2
        value: Shufflethatass
      - id: TYER
        value: '2005'
  quality:
    warnings: []
  common:
    track:
      'no': 5
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    album: Trackers Never Die
    artists:
      - Panix
    artist: Panix
    title: Shufflethatass
    year: 2005
  transformed:
    ID3v2.3:
      TCON: Electronic
      TALB: Trackers Never Die
      TRCK: '05'
      TPE1: Panix
      TIT2: Shufflethatass
      TYER: '2005'
  all:
    TCON: Electronic
    TALB: Trackers Never Die
    TRCK: '05'
    TPE1: Panix
    TIT2: Shufflethatass
    TYER: '2005'
previousTrack:
  path: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/trackers-never-die/3-our-love.mp3
  audio: /assets/albums/trackers-never-die/3-our-love.mp3
  slug: trackers-never-die/3-our-love
  albumSlug: trackers-never-die
  trackSlug: 3-our-love
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/trackers-never-die/3-our-love.jpeg
  cover: /assets/albums/trackers-never-die/3-our-love.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 1
    bitrate: 112000
    codecProfile: CBR
    tool: LAME3.96r
    duration: 96
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TALB
        value: Trackers Never Die
      - id: TRCK
        value: '3'
      - id: TPE1
        value: Panix
      - id: TIT2
        value: Our Love
      - id: TYER
        value: '2005'
  quality:
    warnings: []
  common:
    track:
      'no': 3
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    album: Trackers Never Die
    artists:
      - Panix
    artist: Panix
    title: Our Love
    year: 2005
  transformed:
    ID3v2.3:
      TCON: Electronic
      TALB: Trackers Never Die
      TRCK: '3'
      TPE1: Panix
      TIT2: Our Love
      TYER: '2005'
  all:
    TCON: Electronic
    TALB: Trackers Never Die
    TRCK: '3'
    TPE1: Panix
    TIT2: Our Love
    TYER: '2005'
---
