---
layout: track
path: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/trackers-never-die/1-diptbiit.mp3
audio: /assets/albums/trackers-never-die/1-diptbiit.mp3
slug: trackers-never-die/1-diptbiit
albumSlug: trackers-never-die
trackSlug: 1-diptbiit
coverPath: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/trackers-never-die/1-diptbiit.jpeg
cover: /assets/albums/trackers-never-die/1-diptbiit.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 256000
  codecProfile: CBR
  tool: LAME3.98r
  duration: 149.26367346938775
native:
  ID3v2.3:
    - id: TALB
      value: Trackers Never Die
    - id: TRCK
      value: '1'
    - id: WXXX
      value: &ref_0
        description: ''
        url: ''
    - id: TCON
      value: Electronic
    - id: TPE1
      value: Panix
    - id: TIT2
      value: Diptbiit
    - id: TYER
      value: '2005'
quality:
  warnings: []
common:
  track:
    'no': 1
    of: null
  disk:
    'no': null
    of: null
  album: Trackers Never Die
  genre:
    - Electronic
  artists:
    - Panix
  artist: Panix
  title: Diptbiit
  year: 2005
transformed:
  ID3v2.3:
    TALB: Trackers Never Die
    TRCK: '1'
    WXXX: *ref_0
    TCON: Electronic
    TPE1: Panix
    TIT2: Diptbiit
    TYER: '2005'
all:
  TALB: Trackers Never Die
  TRCK: '1'
  WXXX: *ref_0
  TCON: Electronic
  TPE1: Panix
  TIT2: Diptbiit
  TYER: '2005'
nextTrack:
  path: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/trackers-never-die/2-kyddbiitz.mp3
  audio: /assets/albums/trackers-never-die/2-kyddbiitz.mp3
  slug: trackers-never-die/2-kyddbiitz
  albumSlug: trackers-never-die
  trackSlug: 2-kyddbiitz
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/trackers-never-die/2-kyddbiitz.jpeg
  cover: /assets/albums/trackers-never-die/2-kyddbiitz.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 1
    bitrate: 112000
    codecProfile: CBR
    numberOfSamples: 6342912
    duration: 143.83020408163264
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TALB
        value: Trackers Never Die
      - id: TRCK
        value: '2'
      - id: TPE1
        value: Panix
      - id: TIT2
        value: Kyddbiitz
      - id: TYER
        value: '2005'
  quality:
    warnings: []
  common:
    track:
      'no': 2
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    album: Trackers Never Die
    artists:
      - Panix
    artist: Panix
    title: Kyddbiitz
    year: 2005
  transformed:
    ID3v2.3:
      TCON: Electronic
      TALB: Trackers Never Die
      TRCK: '2'
      TPE1: Panix
      TIT2: Kyddbiitz
      TYER: '2005'
  all:
    TCON: Electronic
    TALB: Trackers Never Die
    TRCK: '2'
    TPE1: Panix
    TIT2: Kyddbiitz
    TYER: '2005'
previousTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/trackers-never-die/8-spiel.mp3
  audio: /assets/albums/trackers-never-die/8-spiel.mp3
  slug: trackers-never-die/8-spiel
  albumSlug: trackers-never-die
  trackSlug: 8-spiel
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/trackers-never-die/8-spiel.jpeg
  cover: /assets/albums/trackers-never-die/8-spiel.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME 3.97UU
    duration: 68.54530612244898
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TALB
        value: Trackers Never Die
      - id: TRCK
        value: '8'
      - id: TPE1
        value: Panix
      - id: TIT2
        value: Spiel
      - id: TYER
        value: '2005'
  quality:
    warnings: []
  common:
    track:
      'no': 8
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    album: Trackers Never Die
    artists:
      - Panix
    artist: Panix
    title: Spiel
    year: 2005
  transformed:
    ID3v2.3:
      TCON: Electronic
      TALB: Trackers Never Die
      TRCK: '8'
      TPE1: Panix
      TIT2: Spiel
      TYER: '2005'
  all:
    TCON: Electronic
    TALB: Trackers Never Die
    TRCK: '8'
    TPE1: Panix
    TIT2: Spiel
    TYER: '2005'
---
