---
layout: track
path: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/trackers-never-die/6-so-fucking-money.mp3
audio: /assets/albums/trackers-never-die/6-so-fucking-money.mp3
slug: trackers-never-die/6-so-fucking-money
albumSlug: trackers-never-die
trackSlug: 6-so-fucking-money
coverPath: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/trackers-never-die/6-so-fucking-money.jpeg
cover: /assets/albums/trackers-never-die/6-so-fucking-money.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 192000
  codecProfile: CBR
  tool: 'LAME3.95 '
  duration: 126.79836734693878
native:
  ID3v2.3:
    - id: TCON
      value: Electronic
    - id: TALB
      value: Trackers Never Die
    - id: TRCK
      value: '06'
    - id: TPE1
      value: Panix
    - id: TIT2
      value: So Fucking Money
    - id: TYER
      value: '2005'
quality:
  warnings: []
common:
  track:
    'no': 6
    of: null
  disk:
    'no': null
    of: null
  genre:
    - Electronic
  album: Trackers Never Die
  artists:
    - Panix
  artist: Panix
  title: So Fucking Money
  year: 2005
transformed:
  ID3v2.3:
    TCON: Electronic
    TALB: Trackers Never Die
    TRCK: '06'
    TPE1: Panix
    TIT2: So Fucking Money
    TYER: '2005'
all:
  TCON: Electronic
  TALB: Trackers Never Die
  TRCK: '06'
  TPE1: Panix
  TIT2: So Fucking Money
  TYER: '2005'
nextTrack:
  path: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/trackers-never-die/7-siddstah.mp3
  audio: /assets/albums/trackers-never-die/7-siddstah.mp3
  slug: trackers-never-die/7-siddstah
  albumSlug: trackers-never-die
  trackSlug: 7-siddstah
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/trackers-never-die/7-siddstah.jpeg
  cover: /assets/albums/trackers-never-die/7-siddstah.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 1
    bitrate: 192000
    codecProfile: CBR
    tool: 'LAME3.97 '
    duration: 128.15673469387755
  native:
    ID3v2.3:
      - id: TRCK
        value: '7'
      - id: WXXX
        value: &ref_0
          description: ''
          url: ''
      - id: TOPE
        value: ''
      - id: TCOM
        value: ''
      - id: TCON
        value: Electronic
      - id: TALB
        value: Trackers Never Die
      - id: TPE1
        value: Panix
      - id: TIT2
        value: Siddstah
      - id: TYER
        value: '2005'
  quality:
    warnings: []
  common:
    track:
      'no': 7
      of: null
    disk:
      'no': null
      of: null
    originalartist: ''
    composer:
      - ''
    genre:
      - Electronic
    album: Trackers Never Die
    artists:
      - Panix
    artist: Panix
    title: Siddstah
    year: 2005
  transformed:
    ID3v2.3:
      TRCK: '7'
      WXXX: *ref_0
      TOPE: ''
      TCOM: ''
      TCON: Electronic
      TALB: Trackers Never Die
      TPE1: Panix
      TIT2: Siddstah
      TYER: '2005'
  all:
    TRCK: '7'
    WXXX: *ref_0
    TOPE: ''
    TCOM: ''
    TCON: Electronic
    TALB: Trackers Never Die
    TPE1: Panix
    TIT2: Siddstah
    TYER: '2005'
previousTrack:
  path: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/trackers-never-die/5-shufflethatass.mp3
  audio: /assets/albums/trackers-never-die/5-shufflethatass.mp3
  slug: trackers-never-die/5-shufflethatass
  albumSlug: trackers-never-die
  trackSlug: 5-shufflethatass
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/trackers-never-die/5-shufflethatass.jpeg
  cover: /assets/albums/trackers-never-die/5-shufflethatass.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 192000
    codecProfile: CBR
    tool: 'LAME3.95 '
    duration: 76.95673469387755
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TALB
        value: Trackers Never Die
      - id: TRCK
        value: '05'
      - id: TPE1
        value: Panix
      - id: TIT2
        value: Shufflethatass
      - id: TYER
        value: '2005'
  quality:
    warnings: []
  common:
    track:
      'no': 5
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    album: Trackers Never Die
    artists:
      - Panix
    artist: Panix
    title: Shufflethatass
    year: 2005
  transformed:
    ID3v2.3:
      TCON: Electronic
      TALB: Trackers Never Die
      TRCK: '05'
      TPE1: Panix
      TIT2: Shufflethatass
      TYER: '2005'
  all:
    TCON: Electronic
    TALB: Trackers Never Die
    TRCK: '05'
    TPE1: Panix
    TIT2: Shufflethatass
    TYER: '2005'
---
