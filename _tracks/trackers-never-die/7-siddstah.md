---
layout: track
path: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/trackers-never-die/7-siddstah.mp3
audio: /assets/albums/trackers-never-die/7-siddstah.mp3
slug: trackers-never-die/7-siddstah
albumSlug: trackers-never-die
trackSlug: 7-siddstah
coverPath: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/trackers-never-die/7-siddstah.jpeg
cover: /assets/albums/trackers-never-die/7-siddstah.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 1
  bitrate: 192000
  codecProfile: CBR
  tool: 'LAME3.97 '
  duration: 128.15673469387755
native:
  ID3v2.3:
    - id: TRCK
      value: '7'
    - id: WXXX
      value: &ref_0
        description: ''
        url: ''
    - id: TOPE
      value: ''
    - id: TCOM
      value: ''
    - id: TCON
      value: Electronic
    - id: TALB
      value: Trackers Never Die
    - id: TPE1
      value: Panix
    - id: TIT2
      value: Siddstah
    - id: TYER
      value: '2005'
quality:
  warnings: []
common:
  track:
    'no': 7
    of: null
  disk:
    'no': null
    of: null
  originalartist: ''
  composer:
    - ''
  genre:
    - Electronic
  album: Trackers Never Die
  artists:
    - Panix
  artist: Panix
  title: Siddstah
  year: 2005
transformed:
  ID3v2.3:
    TRCK: '7'
    WXXX: *ref_0
    TOPE: ''
    TCOM: ''
    TCON: Electronic
    TALB: Trackers Never Die
    TPE1: Panix
    TIT2: Siddstah
    TYER: '2005'
all:
  TRCK: '7'
  WXXX: *ref_0
  TOPE: ''
  TCOM: ''
  TCON: Electronic
  TALB: Trackers Never Die
  TPE1: Panix
  TIT2: Siddstah
  TYER: '2005'
nextTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/trackers-never-die/8-spiel.mp3
  audio: /assets/albums/trackers-never-die/8-spiel.mp3
  slug: trackers-never-die/8-spiel
  albumSlug: trackers-never-die
  trackSlug: 8-spiel
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/trackers-never-die/8-spiel.jpeg
  cover: /assets/albums/trackers-never-die/8-spiel.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME 3.97UU
    duration: 68.54530612244898
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TALB
        value: Trackers Never Die
      - id: TRCK
        value: '8'
      - id: TPE1
        value: Panix
      - id: TIT2
        value: Spiel
      - id: TYER
        value: '2005'
  quality:
    warnings: []
  common:
    track:
      'no': 8
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    album: Trackers Never Die
    artists:
      - Panix
    artist: Panix
    title: Spiel
    year: 2005
  transformed:
    ID3v2.3:
      TCON: Electronic
      TALB: Trackers Never Die
      TRCK: '8'
      TPE1: Panix
      TIT2: Spiel
      TYER: '2005'
  all:
    TCON: Electronic
    TALB: Trackers Never Die
    TRCK: '8'
    TPE1: Panix
    TIT2: Spiel
    TYER: '2005'
previousTrack:
  path: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/trackers-never-die/6-so-fucking-money.mp3
  audio: /assets/albums/trackers-never-die/6-so-fucking-money.mp3
  slug: trackers-never-die/6-so-fucking-money
  albumSlug: trackers-never-die
  trackSlug: 6-so-fucking-money
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/trackers-never-die/6-so-fucking-money.jpeg
  cover: /assets/albums/trackers-never-die/6-so-fucking-money.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 192000
    codecProfile: CBR
    tool: 'LAME3.95 '
    duration: 126.79836734693878
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TALB
        value: Trackers Never Die
      - id: TRCK
        value: '06'
      - id: TPE1
        value: Panix
      - id: TIT2
        value: So Fucking Money
      - id: TYER
        value: '2005'
  quality:
    warnings: []
  common:
    track:
      'no': 6
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    album: Trackers Never Die
    artists:
      - Panix
    artist: Panix
    title: So Fucking Money
    year: 2005
  transformed:
    ID3v2.3:
      TCON: Electronic
      TALB: Trackers Never Die
      TRCK: '06'
      TPE1: Panix
      TIT2: So Fucking Money
      TYER: '2005'
  all:
    TCON: Electronic
    TALB: Trackers Never Die
    TRCK: '06'
    TPE1: Panix
    TIT2: So Fucking Money
    TYER: '2005'
---
