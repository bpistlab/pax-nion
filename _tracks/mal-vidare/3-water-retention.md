---
layout: track
path: /mnt/usb32gb/organized-pax-nion/assets/albums/mal-vidare/3-water-retention.mp3
audio: /assets/albums/mal-vidare/3-water-retention.mp3
slug: mal-vidare/3-water-retention
albumSlug: mal-vidare
trackSlug: 3-water-retention
coverPath: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/mal-vidare/3-water-retention.jpeg
cover: /assets/albums/mal-vidare/3-water-retention.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 320000
  codecProfile: CBR
  tool: 'LAME3.97 '
  duration: 106.60571428571428
native:
  ID3v2.3:
    - id: TRCK
      value: '3'
    - id: TCON
      value: Electronic
    - id: TALB
      value: Mal Vidare
    - id: TPE1
      value: Panixonik
    - id: TIT2
      value: Water Retention
    - id: TYER
      value: '2010'
quality:
  warnings: []
common:
  track:
    'no': 3
    of: null
  disk:
    'no': null
    of: null
  genre:
    - Electronic
  album: Mal Vidare
  artists:
    - Panixonik
  artist: Panixonik
  title: Water Retention
  year: 2010
transformed:
  ID3v2.3:
    TRCK: '3'
    TCON: Electronic
    TALB: Mal Vidare
    TPE1: Panixonik
    TIT2: Water Retention
    TYER: '2010'
all:
  TRCK: '3'
  TCON: Electronic
  TALB: Mal Vidare
  TPE1: Panixonik
  TIT2: Water Retention
  TYER: '2010'
nextTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/mal-vidare/1-reflux-column.mp3
  audio: /assets/albums/mal-vidare/1-reflux-column.mp3
  slug: mal-vidare/1-reflux-column
  albumSlug: mal-vidare
  trackSlug: 1-reflux-column
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/mal-vidare/1-reflux-column.jpeg
  cover: /assets/albums/mal-vidare/1-reflux-column.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: 'LAME3.97 '
    duration: 152.3461224489796
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TALB
        value: Mal Vidare
      - id: TRCK
        value: '1'
      - id: TIT2
        value: Reflux Column
      - id: TPE1
        value: Panixonik
      - id: TYER
        value: '2010'
  quality:
    warnings: []
  common:
    track:
      'no': 1
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    album: Mal Vidare
    title: Reflux Column
    artists:
      - Panixonik
    artist: Panixonik
    year: 2010
  transformed:
    ID3v2.3:
      TCON: Electronic
      TALB: Mal Vidare
      TRCK: '1'
      TIT2: Reflux Column
      TPE1: Panixonik
      TYER: '2010'
  all:
    TCON: Electronic
    TALB: Mal Vidare
    TRCK: '1'
    TIT2: Reflux Column
    TPE1: Panixonik
    TYER: '2010'
previousTrack:
  path: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/mal-vidare/2-kafferegression.mp3
  audio: /assets/albums/mal-vidare/2-kafferegression.mp3
  slug: mal-vidare/2-kafferegression
  albumSlug: mal-vidare
  trackSlug: 2-kafferegression
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/mal-vidare/2-kafferegression.jpeg
  cover: /assets/albums/mal-vidare/2-kafferegression.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: 'LAME3.97 '
    duration: 93.36163265306122
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TRCK
        value: '2'
      - id: TALB
        value: Mal Vidare
      - id: TPE1
        value: Panixonik
      - id: TIT2
        value: Kafferegression
      - id: TYER
        value: '2010'
  quality:
    warnings: []
  common:
    track:
      'no': 2
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    album: Mal Vidare
    artists:
      - Panixonik
    artist: Panixonik
    title: Kafferegression
    year: 2010
  transformed:
    ID3v2.3:
      TCON: Electronic
      TRCK: '2'
      TALB: Mal Vidare
      TPE1: Panixonik
      TIT2: Kafferegression
      TYER: '2010'
  all:
    TCON: Electronic
    TRCK: '2'
    TALB: Mal Vidare
    TPE1: Panixonik
    TIT2: Kafferegression
    TYER: '2010'
---
