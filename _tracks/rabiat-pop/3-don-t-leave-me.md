---
layout: track
path: /mnt/usb32gb/organized-pax-nion/assets/albums/rabiat-pop/3-don-t-leave-me.mp3
audio: /assets/albums/rabiat-pop/3-don-t-leave-me.mp3
slug: rabiat-pop/3-don-t-leave-me
albumSlug: rabiat-pop
trackSlug: 3-don-t-leave-me
coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/rabiat-pop/3-don-t-leave-me.jpeg
cover: /assets/albums/rabiat-pop/3-don-t-leave-me.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 192000
  codecProfile: CBR
  tool: 'LAME3.95 '
  duration: 160.13061224489795
native:
  ID3v2.3:
    - id: TRCK
      value: '3'
    - id: TALB
      value: Rabiat Pop
    - id: TCON
      value: Electronic
    - id: TIT2
      value: Don't leave me
    - id: TPE1
      value: Panix
    - id: TOPE
      value: panyxd
    - id: TYER
      value: '2013'
quality:
  warnings: []
common:
  track:
    'no': 3
    of: null
  disk:
    'no': null
    of: null
  album: Rabiat Pop
  genre:
    - Electronic
  title: Don't leave me
  artists:
    - Panix
  artist: Panix
  originalartist: panyxd
  year: 2013
transformed:
  ID3v2.3:
    TRCK: '3'
    TALB: Rabiat Pop
    TCON: Electronic
    TIT2: Don't leave me
    TPE1: Panix
    TOPE: panyxd
    TYER: '2013'
all:
  TRCK: '3'
  TALB: Rabiat Pop
  TCON: Electronic
  TIT2: Don't leave me
  TPE1: Panix
  TOPE: panyxd
  TYER: '2013'
nextTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/rabiat-pop/4-fyllepops.mp3
  audio: /assets/albums/rabiat-pop/4-fyllepops.mp3
  slug: rabiat-pop/4-fyllepops
  albumSlug: rabiat-pop
  trackSlug: 4-fyllepops
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/rabiat-pop/4-fyllepops.jpeg
  cover: /assets/albums/rabiat-pop/4-fyllepops.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 94.22367346938775
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TALB
        value: Rabiat Pop
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: Fyllepops
      - id: TRCK
        value: '4'
      - id: TOPE
        value: panyxd
      - id: TYER
        value: '2013'
  quality:
    warnings: []
  common:
    track:
      'no': 4
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    album: Rabiat Pop
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: Fyllepops
    originalartist: panyxd
    year: 2013
  transformed:
    ID3v2.3:
      TCON: Electronic
      TALB: Rabiat Pop
      TPE1: Panix Hilton
      TIT2: Fyllepops
      TRCK: '4'
      TOPE: panyxd
      TYER: '2013'
  all:
    TCON: Electronic
    TALB: Rabiat Pop
    TPE1: Panix Hilton
    TIT2: Fyllepops
    TRCK: '4'
    TOPE: panyxd
    TYER: '2013'
previousTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/rabiat-pop/2-fisheye.mp3
  audio: /assets/albums/rabiat-pop/2-fisheye.mp3
  slug: rabiat-pop/2-fisheye
  albumSlug: rabiat-pop
  trackSlug: 2-fisheye
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/rabiat-pop/2-fisheye.jpeg
  cover: /assets/albums/rabiat-pop/2-fisheye.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 97.07102040816326
  native:
    ID3v2.3:
      - id: TALB
        value: Rabiat Pop
      - id: TCON
        value: Electronic
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: Fisheye
      - id: TRCK
        value: '2'
      - id: TOPE
        value: panyxd
      - id: TYER
        value: '2013'
  quality:
    warnings: []
  common:
    track:
      'no': 2
      of: null
    disk:
      'no': null
      of: null
    album: Rabiat Pop
    genre:
      - Electronic
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: Fisheye
    originalartist: panyxd
    year: 2013
  transformed:
    ID3v2.3:
      TALB: Rabiat Pop
      TCON: Electronic
      TPE1: Panix Hilton
      TIT2: Fisheye
      TRCK: '2'
      TOPE: panyxd
      TYER: '2013'
  all:
    TALB: Rabiat Pop
    TCON: Electronic
    TPE1: Panix Hilton
    TIT2: Fisheye
    TRCK: '2'
    TOPE: panyxd
    TYER: '2013'
---
