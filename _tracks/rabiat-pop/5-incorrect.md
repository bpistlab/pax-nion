---
layout: track
path: /mnt/usb32gb/organized-pax-nion/assets/albums/rabiat-pop/5-incorrect.mp3
audio: /assets/albums/rabiat-pop/5-incorrect.mp3
slug: rabiat-pop/5-incorrect
albumSlug: rabiat-pop
trackSlug: 5-incorrect
coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/rabiat-pop/5-incorrect.jpeg
cover: /assets/albums/rabiat-pop/5-incorrect.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 1
  bitrate: 320000
  codecProfile: CBR
  tool: 'LAME3.97 '
  duration: 109.68816326530612
native:
  ID3v2.3:
    - id: TRCK
      value: '5'
    - id: TOPE
      value: panyxd
    - id: TCON
      value: Electronic
    - id: TALB
      value: Rabiat Pop
    - id: TPE1
      value: Panydzix
    - id: TIT2
      value: Incorrect
    - id: TYER
      value: '2013'
quality:
  warnings: []
common:
  track:
    'no': 5
    of: null
  disk:
    'no': null
    of: null
  originalartist: panyxd
  genre:
    - Electronic
  album: Rabiat Pop
  artists:
    - Panydzix
  artist: Panydzix
  title: Incorrect
  year: 2013
transformed:
  ID3v2.3:
    TRCK: '5'
    TOPE: panyxd
    TCON: Electronic
    TALB: Rabiat Pop
    TPE1: Panydzix
    TIT2: Incorrect
    TYER: '2013'
all:
  TRCK: '5'
  TOPE: panyxd
  TCON: Electronic
  TALB: Rabiat Pop
  TPE1: Panydzix
  TIT2: Incorrect
  TYER: '2013'
nextTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/rabiat-pop/6-doublejump.mp3
  audio: /assets/albums/rabiat-pop/6-doublejump.mp3
  slug: rabiat-pop/6-doublejump
  albumSlug: rabiat-pop
  trackSlug: 6-doublejump
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/rabiat-pop/6-doublejump.jpeg
  cover: /assets/albums/rabiat-pop/6-doublejump.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 69.25061224489797
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TALB
        value: Rabiat Pop
      - id: TRCK
        value: '6'
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: Doublejump
      - id: TOPE
        value: panyxd
      - id: TYER
        value: '2013'
  quality:
    warnings: []
  common:
    track:
      'no': 6
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    album: Rabiat Pop
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: Doublejump
    originalartist: panyxd
    year: 2013
  transformed:
    ID3v2.3:
      TCON: Electronic
      TALB: Rabiat Pop
      TRCK: '6'
      TPE1: Panix Hilton
      TIT2: Doublejump
      TOPE: panyxd
      TYER: '2013'
  all:
    TCON: Electronic
    TALB: Rabiat Pop
    TRCK: '6'
    TPE1: Panix Hilton
    TIT2: Doublejump
    TOPE: panyxd
    TYER: '2013'
previousTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/rabiat-pop/4-fyllepops.mp3
  audio: /assets/albums/rabiat-pop/4-fyllepops.mp3
  slug: rabiat-pop/4-fyllepops
  albumSlug: rabiat-pop
  trackSlug: 4-fyllepops
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/rabiat-pop/4-fyllepops.jpeg
  cover: /assets/albums/rabiat-pop/4-fyllepops.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 94.22367346938775
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TALB
        value: Rabiat Pop
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: Fyllepops
      - id: TRCK
        value: '4'
      - id: TOPE
        value: panyxd
      - id: TYER
        value: '2013'
  quality:
    warnings: []
  common:
    track:
      'no': 4
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    album: Rabiat Pop
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: Fyllepops
    originalartist: panyxd
    year: 2013
  transformed:
    ID3v2.3:
      TCON: Electronic
      TALB: Rabiat Pop
      TPE1: Panix Hilton
      TIT2: Fyllepops
      TRCK: '4'
      TOPE: panyxd
      TYER: '2013'
  all:
    TCON: Electronic
    TALB: Rabiat Pop
    TPE1: Panix Hilton
    TIT2: Fyllepops
    TRCK: '4'
    TOPE: panyxd
    TYER: '2013'
---
