---
layout: track
path: /mnt/usb32gb/organized-pax-nion/assets/albums/rabiat-pop/2-fisheye.mp3
audio: /assets/albums/rabiat-pop/2-fisheye.mp3
slug: rabiat-pop/2-fisheye
albumSlug: rabiat-pop
trackSlug: 2-fisheye
coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/rabiat-pop/2-fisheye.jpeg
cover: /assets/albums/rabiat-pop/2-fisheye.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 320000
  codecProfile: CBR
  tool: LAME3.98r
  duration: 97.07102040816326
native:
  ID3v2.3:
    - id: TALB
      value: Rabiat Pop
    - id: TCON
      value: Electronic
    - id: TPE1
      value: Panix Hilton
    - id: TIT2
      value: Fisheye
    - id: TRCK
      value: '2'
    - id: TOPE
      value: panyxd
    - id: TYER
      value: '2013'
quality:
  warnings: []
common:
  track:
    'no': 2
    of: null
  disk:
    'no': null
    of: null
  album: Rabiat Pop
  genre:
    - Electronic
  artists:
    - Panix Hilton
  artist: Panix Hilton
  title: Fisheye
  originalartist: panyxd
  year: 2013
transformed:
  ID3v2.3:
    TALB: Rabiat Pop
    TCON: Electronic
    TPE1: Panix Hilton
    TIT2: Fisheye
    TRCK: '2'
    TOPE: panyxd
    TYER: '2013'
all:
  TALB: Rabiat Pop
  TCON: Electronic
  TPE1: Panix Hilton
  TIT2: Fisheye
  TRCK: '2'
  TOPE: panyxd
  TYER: '2013'
nextTrack:
  path: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/rabiat-pop/3-don-t-leave-me.mp3
  audio: /assets/albums/rabiat-pop/3-don-t-leave-me.mp3
  slug: rabiat-pop/3-don-t-leave-me
  albumSlug: rabiat-pop
  trackSlug: 3-don-t-leave-me
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/rabiat-pop/3-don-t-leave-me.jpeg
  cover: /assets/albums/rabiat-pop/3-don-t-leave-me.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 192000
    codecProfile: CBR
    tool: 'LAME3.95 '
    duration: 160.13061224489795
  native:
    ID3v2.3:
      - id: TRCK
        value: '3'
      - id: TALB
        value: Rabiat Pop
      - id: TCON
        value: Electronic
      - id: TIT2
        value: Don't leave me
      - id: TPE1
        value: Panix
      - id: TOPE
        value: panyxd
      - id: TYER
        value: '2013'
  quality:
    warnings: []
  common:
    track:
      'no': 3
      of: null
    disk:
      'no': null
      of: null
    album: Rabiat Pop
    genre:
      - Electronic
    title: Don't leave me
    artists:
      - Panix
    artist: Panix
    originalartist: panyxd
    year: 2013
  transformed:
    ID3v2.3:
      TRCK: '3'
      TALB: Rabiat Pop
      TCON: Electronic
      TIT2: Don't leave me
      TPE1: Panix
      TOPE: panyxd
      TYER: '2013'
  all:
    TRCK: '3'
    TALB: Rabiat Pop
    TCON: Electronic
    TIT2: Don't leave me
    TPE1: Panix
    TOPE: panyxd
    TYER: '2013'
previousTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/rabiat-pop/1-ofrivillig.mp3
  audio: /assets/albums/rabiat-pop/1-ofrivillig.mp3
  slug: rabiat-pop/1-ofrivillig
  albumSlug: rabiat-pop
  trackSlug: 1-ofrivillig
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/rabiat-pop/1-ofrivillig.jpeg
  cover: /assets/albums/rabiat-pop/1-ofrivillig.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME 3.97UU
    duration: 96.02612244897959
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TALB
        value: Rabiat Pop
      - id: TRCK
        value: '1'
      - id: TPE1
        value: Rabiat
      - id: TIT2
        value: Ofrivillig
      - id: TOPE
        value: panyxd
      - id: TYER
        value: '2013'
  quality:
    warnings: []
  common:
    track:
      'no': 1
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    album: Rabiat Pop
    artists:
      - Rabiat
    artist: Rabiat
    title: Ofrivillig
    originalartist: panyxd
    year: 2013
  transformed:
    ID3v2.3:
      TCON: Electronic
      TALB: Rabiat Pop
      TRCK: '1'
      TPE1: Rabiat
      TIT2: Ofrivillig
      TOPE: panyxd
      TYER: '2013'
  all:
    TCON: Electronic
    TALB: Rabiat Pop
    TRCK: '1'
    TPE1: Rabiat
    TIT2: Ofrivillig
    TOPE: panyxd
    TYER: '2013'
---
