---
layout: track
path: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/end2010-start2011/1-zoega-megadrive.mp3
audio: /assets/albums/end2010-start2011/1-zoega-megadrive.mp3
slug: end2010-start2011/1-zoega-megadrive
albumSlug: end2010-start2011
trackSlug: 1-zoega-megadrive
coverPath: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/end2010-start2011/1-zoega-megadrive.jpeg
cover: /assets/albums/end2010-start2011/1-zoega-megadrive.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 320000
  codecProfile: CBR
  tool: LAME3.98r
  duration: 158.14530612244897
native:
  ID3v2.3:
    - id: TCON
      value: Electronic
    - id: TALB
      value: End2010 - Start2011
    - id: TRCK
      value: '1'
    - id: TIT2
      value: Zoega Megadrive
    - id: TPE1
      value: Panixonik
    - id: TYER
      value: '2010'
quality:
  warnings: []
common:
  track:
    'no': 1
    of: null
  disk:
    'no': null
    of: null
  genre:
    - Electronic
  album: End2010 - Start2011
  title: Zoega Megadrive
  artists:
    - Panixonik
  artist: Panixonik
  year: 2010
transformed:
  ID3v2.3:
    TCON: Electronic
    TALB: End2010 - Start2011
    TRCK: '1'
    TIT2: Zoega Megadrive
    TPE1: Panixonik
    TYER: '2010'
all:
  TCON: Electronic
  TALB: End2010 - Start2011
  TRCK: '1'
  TIT2: Zoega Megadrive
  TPE1: Panixonik
  TYER: '2010'
nextTrack: &ref_0
  path: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/end2010-start2011/2-our-pal.mp3
  audio: /assets/albums/end2010-start2011/2-our-pal.mp3
  slug: end2010-start2011/2-our-pal
  albumSlug: end2010-start2011
  trackSlug: 2-our-pal
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/end2010-start2011/2-our-pal.jpeg
  cover: /assets/albums/end2010-start2011/2-our-pal.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 98.97795918367346
  native:
    ID3v2.3:
      - id: TRCK
        value: '2'
      - id: TCON
        value: Electronic
      - id: TALB
        value: End2010 - Start2011
      - id: TIT2
        value: Our Pal
      - id: TPE1
        value: Panixonik
      - id: TYER
        value: '2010'
  quality:
    warnings: []
  common:
    track:
      'no': 2
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    album: End2010 - Start2011
    title: Our Pal
    artists:
      - Panixonik
    artist: Panixonik
    year: 2010
  transformed:
    ID3v2.3:
      TRCK: '2'
      TCON: Electronic
      TALB: End2010 - Start2011
      TIT2: Our Pal
      TPE1: Panixonik
      TYER: '2010'
  all:
    TRCK: '2'
    TCON: Electronic
    TALB: End2010 - Start2011
    TIT2: Our Pal
    TPE1: Panixonik
    TYER: '2010'
previousTrack: *ref_0
---
