---
layout: track
path: /mnt/usb32gb/organized-pax-nion/assets/albums/morning-moods/6-nejuschfy.mp3
audio: /assets/albums/morning-moods/6-nejuschfy.mp3
slug: morning-moods/6-nejuschfy
albumSlug: morning-moods
trackSlug: 6-nejuschfy
coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/morning-moods/6-nejuschfy.jpeg
cover: /assets/albums/morning-moods/6-nejuschfy.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 320000
  codecProfile: CBR
  tool: LAME 3.98.4
  duration: 190.4065306122449
native:
  ID3v2.3:
    - id: TCON
      value: Electronic
    - id: TYER
      value: '2012'
    - id: TALB
      value: Morning Moods
    - id: TRCK
      value: '6'
    - id: TPE1
      value: Panix Hilton
    - id: TIT2
      value: Nejuschfy
quality:
  warnings: []
common:
  track:
    'no': 6
    of: null
  disk:
    'no': null
    of: null
  genre:
    - Electronic
  year: 2012
  album: Morning Moods
  artists:
    - Panix Hilton
  artist: Panix Hilton
  title: Nejuschfy
transformed:
  ID3v2.3:
    TCON: Electronic
    TYER: '2012'
    TALB: Morning Moods
    TRCK: '6'
    TPE1: Panix Hilton
    TIT2: Nejuschfy
all:
  TCON: Electronic
  TYER: '2012'
  TALB: Morning Moods
  TRCK: '6'
  TPE1: Panix Hilton
  TIT2: Nejuschfy
nextTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/morning-moods/7-redemption.mp3
  audio: /assets/albums/morning-moods/7-redemption.mp3
  slug: morning-moods/7-redemption
  albumSlug: morning-moods
  trackSlug: 7-redemption
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/morning-moods/7-redemption.jpeg
  cover: /assets/albums/morning-moods/7-redemption.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 129.43673469387755
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TALB
        value: Morning Moods
      - id: TRCK
        value: '7'
      - id: TIT2
        value: Redemption
      - id: TPE1
        value: Panix Hilton
      - id: TYER
        value: '2012'
  quality:
    warnings: []
  common:
    track:
      'no': 7
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    album: Morning Moods
    title: Redemption
    artists:
      - Panix Hilton
    artist: Panix Hilton
    year: 2012
  transformed:
    ID3v2.3:
      TCON: Electronic
      TALB: Morning Moods
      TRCK: '7'
      TIT2: Redemption
      TPE1: Panix Hilton
      TYER: '2012'
  all:
    TCON: Electronic
    TALB: Morning Moods
    TRCK: '7'
    TIT2: Redemption
    TPE1: Panix Hilton
    TYER: '2012'
previousTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/morning-moods/5-roller.mp3
  audio: /assets/albums/morning-moods/5-roller.mp3
  slug: morning-moods/5-roller
  albumSlug: morning-moods
  trackSlug: 5-roller
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/morning-moods/5-roller.jpeg
  cover: /assets/albums/morning-moods/5-roller.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 95.16408163265307
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2012'
      - id: TALB
        value: Morning Moods
      - id: TRCK
        value: '5'
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: Roller
  quality:
    warnings: []
  common:
    track:
      'no': 5
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2012
    album: Morning Moods
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: Roller
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2012'
      TALB: Morning Moods
      TRCK: '5'
      TPE1: Panix Hilton
      TIT2: Roller
  all:
    TCON: Electronic
    TYER: '2012'
    TALB: Morning Moods
    TRCK: '5'
    TPE1: Panix Hilton
    TIT2: Roller
---
