---
layout: track
path: /mnt/usb32gb/organized-pax-nion/assets/albums/morning-moods/7-redemption.mp3
audio: /assets/albums/morning-moods/7-redemption.mp3
slug: morning-moods/7-redemption
albumSlug: morning-moods
trackSlug: 7-redemption
coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/morning-moods/7-redemption.jpeg
cover: /assets/albums/morning-moods/7-redemption.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 320000
  codecProfile: CBR
  tool: LAME3.98r
  duration: 129.43673469387755
native:
  ID3v2.3:
    - id: TCON
      value: Electronic
    - id: TALB
      value: Morning Moods
    - id: TRCK
      value: '7'
    - id: TIT2
      value: Redemption
    - id: TPE1
      value: Panix Hilton
    - id: TYER
      value: '2012'
quality:
  warnings: []
common:
  track:
    'no': 7
    of: null
  disk:
    'no': null
    of: null
  genre:
    - Electronic
  album: Morning Moods
  title: Redemption
  artists:
    - Panix Hilton
  artist: Panix Hilton
  year: 2012
transformed:
  ID3v2.3:
    TCON: Electronic
    TALB: Morning Moods
    TRCK: '7'
    TIT2: Redemption
    TPE1: Panix Hilton
    TYER: '2012'
all:
  TCON: Electronic
  TALB: Morning Moods
  TRCK: '7'
  TIT2: Redemption
  TPE1: Panix Hilton
  TYER: '2012'
nextTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/morning-moods/1-inhale.mp3
  audio: /assets/albums/morning-moods/1-inhale.mp3
  slug: morning-moods/1-inhale
  albumSlug: morning-moods
  trackSlug: 1-inhale
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/morning-moods/1-inhale.jpeg
  cover: /assets/albums/morning-moods/1-inhale.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 87.53632653061224
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2012'
      - id: TALB
        value: Morning Moods
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: Inhale
      - id: TRCK
        value: '1'
  quality:
    warnings: []
  common:
    track:
      'no': 1
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2012
    album: Morning Moods
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: Inhale
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2012'
      TALB: Morning Moods
      TPE1: Panix Hilton
      TIT2: Inhale
      TRCK: '1'
  all:
    TCON: Electronic
    TYER: '2012'
    TALB: Morning Moods
    TPE1: Panix Hilton
    TIT2: Inhale
    TRCK: '1'
previousTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/morning-moods/6-nejuschfy.mp3
  audio: /assets/albums/morning-moods/6-nejuschfy.mp3
  slug: morning-moods/6-nejuschfy
  albumSlug: morning-moods
  trackSlug: 6-nejuschfy
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/morning-moods/6-nejuschfy.jpeg
  cover: /assets/albums/morning-moods/6-nejuschfy.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME 3.98.4
    duration: 190.4065306122449
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2012'
      - id: TALB
        value: Morning Moods
      - id: TRCK
        value: '6'
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: Nejuschfy
  quality:
    warnings: []
  common:
    track:
      'no': 6
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2012
    album: Morning Moods
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: Nejuschfy
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2012'
      TALB: Morning Moods
      TRCK: '6'
      TPE1: Panix Hilton
      TIT2: Nejuschfy
  all:
    TCON: Electronic
    TYER: '2012'
    TALB: Morning Moods
    TRCK: '6'
    TPE1: Panix Hilton
    TIT2: Nejuschfy
---
