---
layout: track
path: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/morning-moods/4-humorn-p-topp.mp3
audio: /assets/albums/morning-moods/4-humorn-p-topp.mp3
slug: morning-moods/4-humorn-p-topp
albumSlug: morning-moods
trackSlug: 4-humorn-p-topp
coverPath: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/morning-moods/4-humorn-p-topp.jpeg
cover: /assets/albums/morning-moods/4-humorn-p-topp.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 320000
  codecProfile: CBR
  tool: LAME3.98r
  duration: 112.3004081632653
native:
  ID3v2.3:
    - id: TCON
      value: Electronic
    - id: TYER
      value: '2012'
    - id: TALB
      value: Morning Moods
    - id: TRCK
      value: '4'
    - id: TPE1
      value: Panix Hilton
    - id: TIT2
      value: Humorn På Topp
quality:
  warnings: []
common:
  track:
    'no': 4
    of: null
  disk:
    'no': null
    of: null
  genre:
    - Electronic
  year: 2012
  album: Morning Moods
  artists:
    - Panix Hilton
  artist: Panix Hilton
  title: Humorn På Topp
transformed:
  ID3v2.3:
    TCON: Electronic
    TYER: '2012'
    TALB: Morning Moods
    TRCK: '4'
    TPE1: Panix Hilton
    TIT2: Humorn På Topp
all:
  TCON: Electronic
  TYER: '2012'
  TALB: Morning Moods
  TRCK: '4'
  TPE1: Panix Hilton
  TIT2: Humorn På Topp
nextTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/morning-moods/5-roller.mp3
  audio: /assets/albums/morning-moods/5-roller.mp3
  slug: morning-moods/5-roller
  albumSlug: morning-moods
  trackSlug: 5-roller
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/morning-moods/5-roller.jpeg
  cover: /assets/albums/morning-moods/5-roller.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 95.16408163265307
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2012'
      - id: TALB
        value: Morning Moods
      - id: TRCK
        value: '5'
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: Roller
  quality:
    warnings: []
  common:
    track:
      'no': 5
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2012
    album: Morning Moods
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: Roller
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2012'
      TALB: Morning Moods
      TRCK: '5'
      TPE1: Panix Hilton
      TIT2: Roller
  all:
    TCON: Electronic
    TYER: '2012'
    TALB: Morning Moods
    TRCK: '5'
    TPE1: Panix Hilton
    TIT2: Roller
previousTrack:
  path: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/morning-moods/3-holy-mother-of-bread.mp3
  audio: /assets/albums/morning-moods/3-holy-mother-of-bread.mp3
  slug: morning-moods/3-holy-mother-of-bread
  albumSlug: morning-moods
  trackSlug: 3-holy-mother-of-bread
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/morning-moods/3-holy-mother-of-bread.jpeg
  cover: /assets/albums/morning-moods/3-holy-mother-of-bread.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 118.0734693877551
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2012'
      - id: TRCK
        value: '3'
      - id: TALB
        value: Morning Moods
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: Holy Mother Of Bread
  quality:
    warnings: []
  common:
    track:
      'no': 3
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2012
    album: Morning Moods
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: Holy Mother Of Bread
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2012'
      TRCK: '3'
      TALB: Morning Moods
      TPE1: Panix Hilton
      TIT2: Holy Mother Of Bread
  all:
    TCON: Electronic
    TYER: '2012'
    TRCK: '3'
    TALB: Morning Moods
    TPE1: Panix Hilton
    TIT2: Holy Mother Of Bread
---
