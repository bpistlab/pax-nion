---
layout: track
path: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/too-much-ep/2-too-much-stook-in-the-kook.mp3
audio: /assets/albums/too-much-ep/2-too-much-stook-in-the-kook.mp3
slug: too-much-ep/2-too-much-stook-in-the-kook
albumSlug: too-much-ep
trackSlug: 2-too-much-stook-in-the-kook
coverPath: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/too-much-ep/2-too-much-stook-in-the-kook.jpeg
cover: /assets/albums/too-much-ep/2-too-much-stook-in-the-kook.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 320000
  codecProfile: CBR
  tool: LAME 3.97UU
  duration: 220.94367346938776
native:
  ID3v2.3:
    - id: TRCK
      value: '2'
    - id: TCON
      value: Electronic
    - id: TALB
      value: Too Much EP
    - id: TPE1
      value: Panixonik
    - id: TIT2
      value: Too much stook in the kook
    - id: TYER
      value: '2009'
quality:
  warnings: []
common:
  track:
    'no': 2
    of: null
  disk:
    'no': null
    of: null
  genre:
    - Electronic
  album: Too Much EP
  artists:
    - Panixonik
  artist: Panixonik
  title: Too much stook in the kook
  year: 2009
transformed:
  ID3v2.3:
    TRCK: '2'
    TCON: Electronic
    TALB: Too Much EP
    TPE1: Panixonik
    TIT2: Too much stook in the kook
    TYER: '2009'
all:
  TRCK: '2'
  TCON: Electronic
  TALB: Too Much EP
  TPE1: Panixonik
  TIT2: Too much stook in the kook
  TYER: '2009'
nextTrack: &ref_0
  path: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/too-much-ep/1-too-much-krook-or-look.mp3
  audio: /assets/albums/too-much-ep/1-too-much-krook-or-look.mp3
  slug: too-much-ep/1-too-much-krook-or-look
  albumSlug: too-much-ep
  trackSlug: 1-too-much-krook-or-look
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/too-much-ep/1-too-much-krook-or-look.jpeg
  cover: /assets/albums/too-much-ep/1-too-much-krook-or-look.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: 'LAME3.97 '
    duration: 175.17714285714285
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TALB
        value: Too Much EP
      - id: TRCK
        value: '1'
      - id: TIT2
        value: Too much krook or look
      - id: TPE1
        value: Panixonik
      - id: TYER
        value: '2009'
  quality:
    warnings: []
  common:
    track:
      'no': 1
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    album: Too Much EP
    title: Too much krook or look
    artists:
      - Panixonik
    artist: Panixonik
    year: 2009
  transformed:
    ID3v2.3:
      TCON: Electronic
      TALB: Too Much EP
      TRCK: '1'
      TIT2: Too much krook or look
      TPE1: Panixonik
      TYER: '2009'
  all:
    TCON: Electronic
    TALB: Too Much EP
    TRCK: '1'
    TIT2: Too much krook or look
    TPE1: Panixonik
    TYER: '2009'
previousTrack: *ref_0
---
