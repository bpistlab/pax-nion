---
layout: track
path: /mnt/usb32gb/organized-pax-nion/assets/albums/unstep/2-euplotes.mp3
audio: /assets/albums/unstep/2-euplotes.mp3
slug: unstep/2-euplotes
albumSlug: unstep
trackSlug: 2-euplotes
coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/unstep/2-euplotes.jpeg
cover: /assets/albums/unstep/2-euplotes.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 48000
  numberOfChannels: 2
  bitrate: 320000
  codecProfile: CBR
  tool: LAME3.99r
  duration: 112.416
native:
  ID3v2.3:
    - id: TCON
      value: Electronic
    - id: TYER
      value: '2013'
    - id: TALB
      value: Unstep
    - id: TRCK
      value: '2'
    - id: TPE1
      value: Panix Hilton
    - id: TIT2
      value: Euplotes
quality:
  warnings: []
common:
  track:
    'no': 2
    of: null
  disk:
    'no': null
    of: null
  genre:
    - Electronic
  year: 2013
  album: Unstep
  artists:
    - Panix Hilton
  artist: Panix Hilton
  title: Euplotes
transformed:
  ID3v2.3:
    TCON: Electronic
    TYER: '2013'
    TALB: Unstep
    TRCK: '2'
    TPE1: Panix Hilton
    TIT2: Euplotes
all:
  TCON: Electronic
  TYER: '2013'
  TALB: Unstep
  TRCK: '2'
  TPE1: Panix Hilton
  TIT2: Euplotes
nextTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/unstep/3--dev-null.mp3
  audio: /assets/albums/unstep/3--dev-null.mp3
  slug: unstep/3--dev-null
  albumSlug: unstep
  trackSlug: 3--dev-null
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/unstep/3--dev-null.jpeg
  cover: /assets/albums/unstep/3--dev-null.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 81.81551020408163
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2013'
      - id: TALB
        value: Unstep
      - id: TRCK
        value: '3'
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: /dev/null
  quality:
    warnings: []
  common:
    track:
      'no': 3
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2013
    album: Unstep
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: /dev/null
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2013'
      TALB: Unstep
      TRCK: '3'
      TPE1: Panix Hilton
      TIT2: /dev/null
  all:
    TCON: Electronic
    TYER: '2013'
    TALB: Unstep
    TRCK: '3'
    TPE1: Panix Hilton
    TIT2: /dev/null
previousTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/unstep/1-add-remove.mp3
  audio: /assets/albums/unstep/1-add-remove.mp3
  slug: unstep/1-add-remove
  albumSlug: unstep
  trackSlug: 1-add-remove
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/unstep/1-add-remove.jpeg
  cover: /assets/albums/unstep/1-add-remove.jpeg
  format:
    tagTypes:
      - ID3v2.4
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 103.52326530612245
  native:
    ID3v2.4:
      - id: TCON
        value: Electronic
      - id: TDRC
        value: '2013'
      - id: TYER
        value: '2013'
      - id: TALB
        value: Unstep
      - id: TRCK
        value: '1'
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: Add Remove
  quality:
    warnings: []
  common:
    track:
      'no': 1
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2013
    date: '2013'
    album: Unstep
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: Add Remove
  transformed:
    ID3v2.4:
      TCON: Electronic
      TDRC: '2013'
      TYER: '2013'
      TALB: Unstep
      TRCK: '1'
      TPE1: Panix Hilton
      TIT2: Add Remove
  all:
    TCON: Electronic
    TDRC: '2013'
    TYER: '2013'
    TALB: Unstep
    TRCK: '1'
    TPE1: Panix Hilton
    TIT2: Add Remove
---
