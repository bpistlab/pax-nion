---
layout: track
path: /mnt/usb32gb/organized-pax-nion/assets/albums/unstep/5-thank-you.mp3
audio: /assets/albums/unstep/5-thank-you.mp3
slug: unstep/5-thank-you
albumSlug: unstep
trackSlug: 5-thank-you
coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/unstep/5-thank-you.jpeg
cover: /assets/albums/unstep/5-thank-you.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 320000
  codecProfile: CBR
  tool: LAME3.98r
  duration: 123.74204081632654
native:
  ID3v2.3:
    - id: TCON
      value: Electronic
    - id: TYER
      value: '2013'
    - id: TALB
      value: Unstep
    - id: TRCK
      value: '5'
    - id: TPE1
      value: Panix Hilton
    - id: TIT2
      value: Thank You
quality:
  warnings: []
common:
  track:
    'no': 5
    of: null
  disk:
    'no': null
    of: null
  genre:
    - Electronic
  year: 2013
  album: Unstep
  artists:
    - Panix Hilton
  artist: Panix Hilton
  title: Thank You
transformed:
  ID3v2.3:
    TCON: Electronic
    TYER: '2013'
    TALB: Unstep
    TRCK: '5'
    TPE1: Panix Hilton
    TIT2: Thank You
all:
  TCON: Electronic
  TYER: '2013'
  TALB: Unstep
  TRCK: '5'
  TPE1: Panix Hilton
  TIT2: Thank You
nextTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/unstep/1-add-remove.mp3
  audio: /assets/albums/unstep/1-add-remove.mp3
  slug: unstep/1-add-remove
  albumSlug: unstep
  trackSlug: 1-add-remove
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/unstep/1-add-remove.jpeg
  cover: /assets/albums/unstep/1-add-remove.jpeg
  format:
    tagTypes:
      - ID3v2.4
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 103.52326530612245
  native:
    ID3v2.4:
      - id: TCON
        value: Electronic
      - id: TDRC
        value: '2013'
      - id: TYER
        value: '2013'
      - id: TALB
        value: Unstep
      - id: TRCK
        value: '1'
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: Add Remove
  quality:
    warnings: []
  common:
    track:
      'no': 1
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2013
    date: '2013'
    album: Unstep
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: Add Remove
  transformed:
    ID3v2.4:
      TCON: Electronic
      TDRC: '2013'
      TYER: '2013'
      TALB: Unstep
      TRCK: '1'
      TPE1: Panix Hilton
      TIT2: Add Remove
  all:
    TCON: Electronic
    TDRC: '2013'
    TYER: '2013'
    TALB: Unstep
    TRCK: '1'
    TPE1: Panix Hilton
    TIT2: Add Remove
previousTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/unstep/4-semidetach.mp3
  audio: /assets/albums/unstep/4-semidetach.mp3
  slug: unstep/4-semidetach
  albumSlug: unstep
  trackSlug: 4-semidetach
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/unstep/4-semidetach.jpeg
  cover: /assets/albums/unstep/4-semidetach.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 76.12081632653062
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2013'
      - id: TALB
        value: Unstep
      - id: TRCK
        value: '4'
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: Semidetach
  quality:
    warnings: []
  common:
    track:
      'no': 4
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2013
    album: Unstep
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: Semidetach
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2013'
      TALB: Unstep
      TRCK: '4'
      TPE1: Panix Hilton
      TIT2: Semidetach
  all:
    TCON: Electronic
    TYER: '2013'
    TALB: Unstep
    TRCK: '4'
    TPE1: Panix Hilton
    TIT2: Semidetach
---
