---
layout: track
path: /mnt/usb32gb/organized-pax-nion/assets/albums/buy-a-vowel/6-atmk-sbmrn.mp3
audio: /assets/albums/buy-a-vowel/6-atmk-sbmrn.mp3
slug: buy-a-vowel/6-atmk-sbmrn
albumSlug: buy-a-vowel
trackSlug: 6-atmk-sbmrn
coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/buy-a-vowel/6-atmk-sbmrn.jpeg
cover: /assets/albums/buy-a-vowel/6-atmk-sbmrn.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 320000
  codecProfile: CBR
  tool: LAME 3.97UU
  duration: 149.52489795918368
native:
  ID3v2.3:
    - id: TCON
      value: Electronic
    - id: TALB
      value: Buy A Vowel
    - id: TRCK
      value: '6'
    - id: TIT2
      value: ATMK SBMRN
    - id: TPE1
      value: Panixonik
    - id: TYER
      value: '2009'
quality:
  warnings: []
common:
  track:
    'no': 6
    of: null
  disk:
    'no': null
    of: null
  genre:
    - Electronic
  album: Buy A Vowel
  title: ATMK SBMRN
  artists:
    - Panixonik
  artist: Panixonik
  year: 2009
transformed:
  ID3v2.3:
    TCON: Electronic
    TALB: Buy A Vowel
    TRCK: '6'
    TIT2: ATMK SBMRN
    TPE1: Panixonik
    TYER: '2009'
all:
  TCON: Electronic
  TALB: Buy A Vowel
  TRCK: '6'
  TIT2: ATMK SBMRN
  TPE1: Panixonik
  TYER: '2009'
nextTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/buy-a-vowel/7-gd-mrgn.mp3
  audio: /assets/albums/buy-a-vowel/7-gd-mrgn.mp3
  slug: buy-a-vowel/7-gd-mrgn
  albumSlug: buy-a-vowel
  trackSlug: 7-gd-mrgn
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/buy-a-vowel/7-gd-mrgn.jpeg
  cover: /assets/albums/buy-a-vowel/7-gd-mrgn.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: 'LAME3.97 '
    duration: 110.7591836734694
  native:
    ID3v2.3:
      - id: TRCK
        value: '7'
      - id: TCON
        value: Electronic
      - id: TALB
        value: Buy A Vowel
      - id: TPE1
        value: Panixonik
      - id: TIT2
        value: GD MRGN
      - id: TYER
        value: '2009'
  quality:
    warnings: []
  common:
    track:
      'no': 7
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    album: Buy A Vowel
    artists:
      - Panixonik
    artist: Panixonik
    title: GD MRGN
    year: 2009
  transformed:
    ID3v2.3:
      TRCK: '7'
      TCON: Electronic
      TALB: Buy A Vowel
      TPE1: Panixonik
      TIT2: GD MRGN
      TYER: '2009'
  all:
    TRCK: '7'
    TCON: Electronic
    TALB: Buy A Vowel
    TPE1: Panixonik
    TIT2: GD MRGN
    TYER: '2009'
previousTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/buy-a-vowel/5-tht-ht-trck.mp3
  audio: /assets/albums/buy-a-vowel/5-tht-ht-trck.mp3
  slug: buy-a-vowel/5-tht-ht-trck
  albumSlug: buy-a-vowel
  trackSlug: 5-tht-ht-trck
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/buy-a-vowel/5-tht-ht-trck.jpeg
  cover: /assets/albums/buy-a-vowel/5-tht-ht-trck.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME 3.97UU
    duration: 121.93959183673469
  native:
    ID3v2.3:
      - id: TRCK
        value: '5'
      - id: TCON
        value: Electronic
      - id: TALB
        value: Buy A Vowel
      - id: TPE1
        value: Panixonik
      - id: TIT2
        value: THT HT TRCK
      - id: TYER
        value: '2009'
  quality:
    warnings: []
  common:
    track:
      'no': 5
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    album: Buy A Vowel
    artists:
      - Panixonik
    artist: Panixonik
    title: THT HT TRCK
    year: 2009
  transformed:
    ID3v2.3:
      TRCK: '5'
      TCON: Electronic
      TALB: Buy A Vowel
      TPE1: Panixonik
      TIT2: THT HT TRCK
      TYER: '2009'
  all:
    TRCK: '5'
    TCON: Electronic
    TALB: Buy A Vowel
    TPE1: Panixonik
    TIT2: THT HT TRCK
    TYER: '2009'
---
