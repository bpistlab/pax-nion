---
layout: track
path: /mnt/usb32gb/organized-pax-nion/assets/albums/buy-a-vowel/4-lss-s-mr.mp3
audio: /assets/albums/buy-a-vowel/4-lss-s-mr.mp3
slug: buy-a-vowel/4-lss-s-mr
albumSlug: buy-a-vowel
trackSlug: 4-lss-s-mr
coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/buy-a-vowel/4-lss-s-mr.jpeg
cover: /assets/albums/buy-a-vowel/4-lss-s-mr.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 320000
  codecProfile: CBR
  tool: LAME 3.97UU
  duration: 101.12
native:
  ID3v2.3:
    - id: TRCK
      value: '4'
    - id: TCON
      value: Electronic
    - id: TALB
      value: Buy A Vowel
    - id: TPE1
      value: Panixonik
    - id: TIT2
      value: LSS S MR
    - id: TYER
      value: '2009'
quality:
  warnings: []
common:
  track:
    'no': 4
    of: null
  disk:
    'no': null
    of: null
  genre:
    - Electronic
  album: Buy A Vowel
  artists:
    - Panixonik
  artist: Panixonik
  title: LSS S MR
  year: 2009
transformed:
  ID3v2.3:
    TRCK: '4'
    TCON: Electronic
    TALB: Buy A Vowel
    TPE1: Panixonik
    TIT2: LSS S MR
    TYER: '2009'
all:
  TRCK: '4'
  TCON: Electronic
  TALB: Buy A Vowel
  TPE1: Panixonik
  TIT2: LSS S MR
  TYER: '2009'
nextTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/buy-a-vowel/5-tht-ht-trck.mp3
  audio: /assets/albums/buy-a-vowel/5-tht-ht-trck.mp3
  slug: buy-a-vowel/5-tht-ht-trck
  albumSlug: buy-a-vowel
  trackSlug: 5-tht-ht-trck
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/buy-a-vowel/5-tht-ht-trck.jpeg
  cover: /assets/albums/buy-a-vowel/5-tht-ht-trck.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME 3.97UU
    duration: 121.93959183673469
  native:
    ID3v2.3:
      - id: TRCK
        value: '5'
      - id: TCON
        value: Electronic
      - id: TALB
        value: Buy A Vowel
      - id: TPE1
        value: Panixonik
      - id: TIT2
        value: THT HT TRCK
      - id: TYER
        value: '2009'
  quality:
    warnings: []
  common:
    track:
      'no': 5
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    album: Buy A Vowel
    artists:
      - Panixonik
    artist: Panixonik
    title: THT HT TRCK
    year: 2009
  transformed:
    ID3v2.3:
      TRCK: '5'
      TCON: Electronic
      TALB: Buy A Vowel
      TPE1: Panixonik
      TIT2: THT HT TRCK
      TYER: '2009'
  all:
    TRCK: '5'
    TCON: Electronic
    TALB: Buy A Vowel
    TPE1: Panixonik
    TIT2: THT HT TRCK
    TYER: '2009'
previousTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/buy-a-vowel/3-snks-r-stpd.mp3
  audio: /assets/albums/buy-a-vowel/3-snks-r-stpd.mp3
  slug: buy-a-vowel/3-snks-r-stpd
  albumSlug: buy-a-vowel
  trackSlug: 3-snks-r-stpd
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/buy-a-vowel/3-snks-r-stpd.jpeg
  cover: /assets/albums/buy-a-vowel/3-snks-r-stpd.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: 'LAME3.97 '
    duration: 110.70693877551021
  native:
    ID3v2.3:
      - id: TRCK
        value: '3'
      - id: TCON
        value: Electronic
      - id: TALB
        value: Buy A Vowel
      - id: TPE1
        value: Panixonik
      - id: TIT2
        value: SNKS R STPD
      - id: TYER
        value: '2009'
  quality:
    warnings: []
  common:
    track:
      'no': 3
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    album: Buy A Vowel
    artists:
      - Panixonik
    artist: Panixonik
    title: SNKS R STPD
    year: 2009
  transformed:
    ID3v2.3:
      TRCK: '3'
      TCON: Electronic
      TALB: Buy A Vowel
      TPE1: Panixonik
      TIT2: SNKS R STPD
      TYER: '2009'
  all:
    TRCK: '3'
    TCON: Electronic
    TALB: Buy A Vowel
    TPE1: Panixonik
    TIT2: SNKS R STPD
    TYER: '2009'
---
