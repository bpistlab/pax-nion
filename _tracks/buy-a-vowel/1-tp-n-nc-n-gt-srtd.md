---
layout: track
path: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/buy-a-vowel/1-tp-n-nc-n-gt-srtd.mp3
audio: /assets/albums/buy-a-vowel/1-tp-n-nc-n-gt-srtd.mp3
slug: buy-a-vowel/1-tp-n-nc-n-gt-srtd
albumSlug: buy-a-vowel
trackSlug: 1-tp-n-nc-n-gt-srtd
coverPath: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/buy-a-vowel/1-tp-n-nc-n-gt-srtd.jpeg
cover: /assets/albums/buy-a-vowel/1-tp-n-nc-n-gt-srtd.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 320000
  codecProfile: CBR
  tool: LAME 3.97UU
  duration: 144.74448979591835
native:
  ID3v2.3:
    - id: TRCK
      value: '1'
    - id: TCON
      value: Electronic
    - id: TALB
      value: Buy A Vowel
    - id: TPE1
      value: Panixonik
    - id: TIT2
      value: TP N NC N GT SRTD
    - id: TYER
      value: '2009'
quality:
  warnings: []
common:
  track:
    'no': 1
    of: null
  disk:
    'no': null
    of: null
  genre:
    - Electronic
  album: Buy A Vowel
  artists:
    - Panixonik
  artist: Panixonik
  title: TP N NC N GT SRTD
  year: 2009
transformed:
  ID3v2.3:
    TRCK: '1'
    TCON: Electronic
    TALB: Buy A Vowel
    TPE1: Panixonik
    TIT2: TP N NC N GT SRTD
    TYER: '2009'
all:
  TRCK: '1'
  TCON: Electronic
  TALB: Buy A Vowel
  TPE1: Panixonik
  TIT2: TP N NC N GT SRTD
  TYER: '2009'
nextTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/buy-a-vowel/2-rptr-mr.mp3
  audio: /assets/albums/buy-a-vowel/2-rptr-mr.mp3
  slug: buy-a-vowel/2-rptr-mr
  albumSlug: buy-a-vowel
  trackSlug: 2-rptr-mr
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/buy-a-vowel/2-rptr-mr.jpeg
  cover: /assets/albums/buy-a-vowel/2-rptr-mr.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: 'LAME3.97 '
    duration: 128.6269387755102
  native:
    ID3v2.3:
      - id: TRCK
        value: '2'
      - id: TCON
        value: Electronic
      - id: TALB
        value: Buy A Vowel
      - id: TPE1
        value: Panixonik
      - id: TIT2
        value: RPTR MR
      - id: TYER
        value: '2009'
  quality:
    warnings: []
  common:
    track:
      'no': 2
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    album: Buy A Vowel
    artists:
      - Panixonik
    artist: Panixonik
    title: RPTR MR
    year: 2009
  transformed:
    ID3v2.3:
      TRCK: '2'
      TCON: Electronic
      TALB: Buy A Vowel
      TPE1: Panixonik
      TIT2: RPTR MR
      TYER: '2009'
  all:
    TRCK: '2'
    TCON: Electronic
    TALB: Buy A Vowel
    TPE1: Panixonik
    TIT2: RPTR MR
    TYER: '2009'
previousTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/buy-a-vowel/7-gd-mrgn.mp3
  audio: /assets/albums/buy-a-vowel/7-gd-mrgn.mp3
  slug: buy-a-vowel/7-gd-mrgn
  albumSlug: buy-a-vowel
  trackSlug: 7-gd-mrgn
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/buy-a-vowel/7-gd-mrgn.jpeg
  cover: /assets/albums/buy-a-vowel/7-gd-mrgn.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: 'LAME3.97 '
    duration: 110.7591836734694
  native:
    ID3v2.3:
      - id: TRCK
        value: '7'
      - id: TCON
        value: Electronic
      - id: TALB
        value: Buy A Vowel
      - id: TPE1
        value: Panixonik
      - id: TIT2
        value: GD MRGN
      - id: TYER
        value: '2009'
  quality:
    warnings: []
  common:
    track:
      'no': 7
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    album: Buy A Vowel
    artists:
      - Panixonik
    artist: Panixonik
    title: GD MRGN
    year: 2009
  transformed:
    ID3v2.3:
      TRCK: '7'
      TCON: Electronic
      TALB: Buy A Vowel
      TPE1: Panixonik
      TIT2: GD MRGN
      TYER: '2009'
  all:
    TRCK: '7'
    TCON: Electronic
    TALB: Buy A Vowel
    TPE1: Panixonik
    TIT2: GD MRGN
    TYER: '2009'
---
