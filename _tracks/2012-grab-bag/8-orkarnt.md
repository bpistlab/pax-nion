---
layout: track
path: /mnt/usb32gb/organized-pax-nion/assets/albums/2012-grab-bag/8-orkarnt.mp3
audio: /assets/albums/2012-grab-bag/8-orkarnt.mp3
slug: 2012-grab-bag/8-orkarnt
albumSlug: 2012-grab-bag
trackSlug: 8-orkarnt
coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/2012-grab-bag/8-orkarnt.jpeg
cover: /assets/albums/2012-grab-bag/8-orkarnt.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 320000
  codecProfile: CBR
  tool: LAME 3.98.4
  duration: 127.32081632653062
native:
  ID3v2.3:
    - id: TCON
      value: Electronic
    - id: TYER
      value: '2012'
    - id: TALB
      value: 2012 Grab Bag
    - id: TRCK
      value: '8'
    - id: TPE1
      value: Panix Hilton
    - id: TIT2
      value: Orkarnt
quality:
  warnings: []
common:
  track:
    'no': 8
    of: null
  disk:
    'no': null
    of: null
  genre:
    - Electronic
  year: 2012
  album: 2012 Grab Bag
  artists:
    - Panix Hilton
  artist: Panix Hilton
  title: Orkarnt
transformed:
  ID3v2.3:
    TCON: Electronic
    TYER: '2012'
    TALB: 2012 Grab Bag
    TRCK: '8'
    TPE1: Panix Hilton
    TIT2: Orkarnt
all:
  TCON: Electronic
  TYER: '2012'
  TALB: 2012 Grab Bag
  TRCK: '8'
  TPE1: Panix Hilton
  TIT2: Orkarnt
nextTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/2012-grab-bag/9-skithink.mp3
  audio: /assets/albums/2012-grab-bag/9-skithink.mp3
  slug: 2012-grab-bag/9-skithink
  albumSlug: 2012-grab-bag
  trackSlug: 9-skithink
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/2012-grab-bag/9-skithink.jpeg
  cover: /assets/albums/2012-grab-bag/9-skithink.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 103.31428571428572
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2012'
      - id: TALB
        value: 2012 Grab Bag
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: Skithink
      - id: TRCK
        value: '9'
  quality:
    warnings: []
  common:
    track:
      'no': 9
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2012
    album: 2012 Grab Bag
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: Skithink
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2012'
      TALB: 2012 Grab Bag
      TPE1: Panix Hilton
      TIT2: Skithink
      TRCK: '9'
  all:
    TCON: Electronic
    TYER: '2012'
    TALB: 2012 Grab Bag
    TPE1: Panix Hilton
    TIT2: Skithink
    TRCK: '9'
previousTrack:
  path: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/2012-grab-bag/7-bored-beyond-belief.mp3
  audio: /assets/albums/2012-grab-bag/7-bored-beyond-belief.mp3
  slug: 2012-grab-bag/7-bored-beyond-belief
  albumSlug: 2012-grab-bag
  trackSlug: 7-bored-beyond-belief
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/2012-grab-bag/7-bored-beyond-belief.jpeg
  cover: /assets/albums/2012-grab-bag/7-bored-beyond-belief.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 85.62938775510204
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2012'
      - id: TALB
        value: 2012 Grab Bag
      - id: TRCK
        value: '7'
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: Bored Beyond Belief
  quality:
    warnings: []
  common:
    track:
      'no': 7
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2012
    album: 2012 Grab Bag
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: Bored Beyond Belief
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2012'
      TALB: 2012 Grab Bag
      TRCK: '7'
      TPE1: Panix Hilton
      TIT2: Bored Beyond Belief
  all:
    TCON: Electronic
    TYER: '2012'
    TALB: 2012 Grab Bag
    TRCK: '7'
    TPE1: Panix Hilton
    TIT2: Bored Beyond Belief
---
