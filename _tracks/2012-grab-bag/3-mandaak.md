---
layout: track
path: /mnt/usb32gb/organized-pax-nion/assets/albums/2012-grab-bag/3-mandaak.mp3
audio: /assets/albums/2012-grab-bag/3-mandaak.mp3
slug: 2012-grab-bag/3-mandaak
albumSlug: 2012-grab-bag
trackSlug: 3-mandaak
coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/2012-grab-bag/3-mandaak.jpeg
cover: /assets/albums/2012-grab-bag/3-mandaak.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 320000
  codecProfile: CBR
  tool: LAME3.98r
  duration: 140.87836734693877
native:
  ID3v2.3:
    - id: TCON
      value: Electronic
    - id: TYER
      value: '2012'
    - id: TALB
      value: 2012 Grab Bag
    - id: TRCK
      value: '3'
    - id: TPE1
      value: Panix Hilton
    - id: TIT2
      value: Mandaak
quality:
  warnings: []
common:
  track:
    'no': 3
    of: null
  disk:
    'no': null
    of: null
  genre:
    - Electronic
  year: 2012
  album: 2012 Grab Bag
  artists:
    - Panix Hilton
  artist: Panix Hilton
  title: Mandaak
transformed:
  ID3v2.3:
    TCON: Electronic
    TYER: '2012'
    TALB: 2012 Grab Bag
    TRCK: '3'
    TPE1: Panix Hilton
    TIT2: Mandaak
all:
  TCON: Electronic
  TYER: '2012'
  TALB: 2012 Grab Bag
  TRCK: '3'
  TPE1: Panix Hilton
  TIT2: Mandaak
nextTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/2012-grab-bag/4-inte-sova.mp3
  audio: /assets/albums/2012-grab-bag/4-inte-sova.mp3
  slug: 2012-grab-bag/4-inte-sova
  albumSlug: 2012-grab-bag
  trackSlug: 4-inte-sova
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/2012-grab-bag/4-inte-sova.jpeg
  cover: /assets/albums/2012-grab-bag/4-inte-sova.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 131.34367346938777
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2012'
      - id: TALB
        value: 2012 Grab Bag
      - id: TRCK
        value: '4'
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: Inte Sova
  quality:
    warnings: []
  common:
    track:
      'no': 4
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2012
    album: 2012 Grab Bag
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: Inte Sova
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2012'
      TALB: 2012 Grab Bag
      TRCK: '4'
      TPE1: Panix Hilton
      TIT2: Inte Sova
  all:
    TCON: Electronic
    TYER: '2012'
    TALB: 2012 Grab Bag
    TRCK: '4'
    TPE1: Panix Hilton
    TIT2: Inte Sova
previousTrack:
  path: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/2012-grab-bag/2-hookin-for-cheeseburgers.mp3
  audio: /assets/albums/2012-grab-bag/2-hookin-for-cheeseburgers.mp3
  slug: 2012-grab-bag/2-hookin-for-cheeseburgers
  albumSlug: 2012-grab-bag
  trackSlug: 2-hookin-for-cheeseburgers
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/2012-grab-bag/2-hookin-for-cheeseburgers.jpeg
  cover: /assets/albums/2012-grab-bag/2-hookin-for-cheeseburgers.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 112.53551020408163
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2012'
      - id: TALB
        value: 2012 Grab Bag
      - id: TRCK
        value: '2'
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: Hookin For Cheeseburgers
  quality:
    warnings: []
  common:
    track:
      'no': 2
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2012
    album: 2012 Grab Bag
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: Hookin For Cheeseburgers
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2012'
      TALB: 2012 Grab Bag
      TRCK: '2'
      TPE1: Panix Hilton
      TIT2: Hookin For Cheeseburgers
  all:
    TCON: Electronic
    TYER: '2012'
    TALB: 2012 Grab Bag
    TRCK: '2'
    TPE1: Panix Hilton
    TIT2: Hookin For Cheeseburgers
---
