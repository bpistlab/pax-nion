---
layout: track
path: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/2012-grab-bag/6-spinal-cord-forever.mp3
audio: /assets/albums/2012-grab-bag/6-spinal-cord-forever.mp3
slug: 2012-grab-bag/6-spinal-cord-forever
albumSlug: 2012-grab-bag
trackSlug: 6-spinal-cord-forever
coverPath: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/2012-grab-bag/6-spinal-cord-forever.jpeg
cover: /assets/albums/2012-grab-bag/6-spinal-cord-forever.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 320000
  codecProfile: CBR
  tool: LAME3.98r
  duration: 142.785306122449
native:
  ID3v2.3:
    - id: TCON
      value: Electronic
    - id: TYER
      value: '2012'
    - id: TALB
      value: 2012 Grab Bag
    - id: TRCK
      value: '6'
    - id: TPE1
      value: Panix Hilton
    - id: TIT2
      value: Spinal Cord Forever
quality:
  warnings: []
common:
  track:
    'no': 6
    of: null
  disk:
    'no': null
    of: null
  genre:
    - Electronic
  year: 2012
  album: 2012 Grab Bag
  artists:
    - Panix Hilton
  artist: Panix Hilton
  title: Spinal Cord Forever
transformed:
  ID3v2.3:
    TCON: Electronic
    TYER: '2012'
    TALB: 2012 Grab Bag
    TRCK: '6'
    TPE1: Panix Hilton
    TIT2: Spinal Cord Forever
all:
  TCON: Electronic
  TYER: '2012'
  TALB: 2012 Grab Bag
  TRCK: '6'
  TPE1: Panix Hilton
  TIT2: Spinal Cord Forever
nextTrack:
  path: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/2012-grab-bag/7-bored-beyond-belief.mp3
  audio: /assets/albums/2012-grab-bag/7-bored-beyond-belief.mp3
  slug: 2012-grab-bag/7-bored-beyond-belief
  albumSlug: 2012-grab-bag
  trackSlug: 7-bored-beyond-belief
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/2012-grab-bag/7-bored-beyond-belief.jpeg
  cover: /assets/albums/2012-grab-bag/7-bored-beyond-belief.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 85.62938775510204
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2012'
      - id: TALB
        value: 2012 Grab Bag
      - id: TRCK
        value: '7'
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: Bored Beyond Belief
  quality:
    warnings: []
  common:
    track:
      'no': 7
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2012
    album: 2012 Grab Bag
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: Bored Beyond Belief
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2012'
      TALB: 2012 Grab Bag
      TRCK: '7'
      TPE1: Panix Hilton
      TIT2: Bored Beyond Belief
  all:
    TCON: Electronic
    TYER: '2012'
    TALB: 2012 Grab Bag
    TRCK: '7'
    TPE1: Panix Hilton
    TIT2: Bored Beyond Belief
previousTrack:
  path: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/2012-grab-bag/5-you-need-it.mp3
  audio: /assets/albums/2012-grab-bag/5-you-need-it.mp3
  slug: 2012-grab-bag/5-you-need-it
  albumSlug: 2012-grab-bag
  trackSlug: 5-you-need-it
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/2012-grab-bag/5-you-need-it.jpeg
  cover: /assets/albums/2012-grab-bag/5-you-need-it.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 146.59918367346938
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2012'
      - id: TALB
        value: 2012 Grab Bag
      - id: TRCK
        value: '5'
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: You Need It
  quality:
    warnings: []
  common:
    track:
      'no': 5
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2012
    album: 2012 Grab Bag
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: You Need It
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2012'
      TALB: 2012 Grab Bag
      TRCK: '5'
      TPE1: Panix Hilton
      TIT2: You Need It
  all:
    TCON: Electronic
    TYER: '2012'
    TALB: 2012 Grab Bag
    TRCK: '5'
    TPE1: Panix Hilton
    TIT2: You Need It
---
