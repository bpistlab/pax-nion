---
layout: track
path: /mnt/usb32gb/organized-pax-nion/assets/albums/2012-grab-bag/4-inte-sova.mp3
audio: /assets/albums/2012-grab-bag/4-inte-sova.mp3
slug: 2012-grab-bag/4-inte-sova
albumSlug: 2012-grab-bag
trackSlug: 4-inte-sova
coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/2012-grab-bag/4-inte-sova.jpeg
cover: /assets/albums/2012-grab-bag/4-inte-sova.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 320000
  codecProfile: CBR
  tool: LAME3.98r
  duration: 131.34367346938777
native:
  ID3v2.3:
    - id: TCON
      value: Electronic
    - id: TYER
      value: '2012'
    - id: TALB
      value: 2012 Grab Bag
    - id: TRCK
      value: '4'
    - id: TPE1
      value: Panix Hilton
    - id: TIT2
      value: Inte Sova
quality:
  warnings: []
common:
  track:
    'no': 4
    of: null
  disk:
    'no': null
    of: null
  genre:
    - Electronic
  year: 2012
  album: 2012 Grab Bag
  artists:
    - Panix Hilton
  artist: Panix Hilton
  title: Inte Sova
transformed:
  ID3v2.3:
    TCON: Electronic
    TYER: '2012'
    TALB: 2012 Grab Bag
    TRCK: '4'
    TPE1: Panix Hilton
    TIT2: Inte Sova
all:
  TCON: Electronic
  TYER: '2012'
  TALB: 2012 Grab Bag
  TRCK: '4'
  TPE1: Panix Hilton
  TIT2: Inte Sova
nextTrack:
  path: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/2012-grab-bag/5-you-need-it.mp3
  audio: /assets/albums/2012-grab-bag/5-you-need-it.mp3
  slug: 2012-grab-bag/5-you-need-it
  albumSlug: 2012-grab-bag
  trackSlug: 5-you-need-it
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/2012-grab-bag/5-you-need-it.jpeg
  cover: /assets/albums/2012-grab-bag/5-you-need-it.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 146.59918367346938
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2012'
      - id: TALB
        value: 2012 Grab Bag
      - id: TRCK
        value: '5'
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: You Need It
  quality:
    warnings: []
  common:
    track:
      'no': 5
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2012
    album: 2012 Grab Bag
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: You Need It
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2012'
      TALB: 2012 Grab Bag
      TRCK: '5'
      TPE1: Panix Hilton
      TIT2: You Need It
  all:
    TCON: Electronic
    TYER: '2012'
    TALB: 2012 Grab Bag
    TRCK: '5'
    TPE1: Panix Hilton
    TIT2: You Need It
previousTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/2012-grab-bag/3-mandaak.mp3
  audio: /assets/albums/2012-grab-bag/3-mandaak.mp3
  slug: 2012-grab-bag/3-mandaak
  albumSlug: 2012-grab-bag
  trackSlug: 3-mandaak
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/2012-grab-bag/3-mandaak.jpeg
  cover: /assets/albums/2012-grab-bag/3-mandaak.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 140.87836734693877
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2012'
      - id: TALB
        value: 2012 Grab Bag
      - id: TRCK
        value: '3'
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: Mandaak
  quality:
    warnings: []
  common:
    track:
      'no': 3
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2012
    album: 2012 Grab Bag
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: Mandaak
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2012'
      TALB: 2012 Grab Bag
      TRCK: '3'
      TPE1: Panix Hilton
      TIT2: Mandaak
  all:
    TCON: Electronic
    TYER: '2012'
    TALB: 2012 Grab Bag
    TRCK: '3'
    TPE1: Panix Hilton
    TIT2: Mandaak
---
