---
layout: track
path: /mnt/usb32gb/organized-pax-nion/assets/albums/br-tbients/3-broet-igen.mp3
audio: /assets/albums/br-tbients/3-broet-igen.mp3
slug: br-tbients/3-broet-igen
albumSlug: br-tbients
trackSlug: 3-broet-igen
coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/br-tbients/3-broet-igen.jpeg
cover: /assets/albums/br-tbients/3-broet-igen.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 320000
  codecProfile: CBR
  tool: LAME3.98r
  duration: 107.88571428571429
native:
  ID3v2.3:
    - id: TCON
      value: Electronic
    - id: TALB
      value: Brötbients
    - id: TRCK
      value: '3'
    - id: TIT2
      value: Broet Igen
    - id: TPE1
      value: Panix Illtone
    - id: TYER
      value: '2011'
quality:
  warnings: []
common:
  track:
    'no': 3
    of: null
  disk:
    'no': null
    of: null
  genre:
    - Electronic
  album: Brötbients
  title: Broet Igen
  artists:
    - Panix Illtone
  artist: Panix Illtone
  year: 2011
transformed:
  ID3v2.3:
    TCON: Electronic
    TALB: Brötbients
    TRCK: '3'
    TIT2: Broet Igen
    TPE1: Panix Illtone
    TYER: '2011'
all:
  TCON: Electronic
  TALB: Brötbients
  TRCK: '3'
  TIT2: Broet Igen
  TPE1: Panix Illtone
  TYER: '2011'
nextTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/br-tbients/1-br-tbient.mp3
  audio: /assets/albums/br-tbients/1-br-tbient.mp3
  slug: br-tbients/1-br-tbient
  albumSlug: br-tbients
  trackSlug: 1-br-tbient
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/br-tbients/1-br-tbient.jpeg
  cover: /assets/albums/br-tbients/1-br-tbient.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 136.6987755102041
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2011'
      - id: TALB
        value: Brötbients
      - id: TRCK
        value: '1'
      - id: TPE1
        value: Panix Illtone
      - id: TIT2
        value: Brötbient
  quality:
    warnings: []
  common:
    track:
      'no': 1
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2011
    album: Brötbients
    artists:
      - Panix Illtone
    artist: Panix Illtone
    title: Brötbient
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2011'
      TALB: Brötbients
      TRCK: '1'
      TPE1: Panix Illtone
      TIT2: Brötbient
  all:
    TCON: Electronic
    TYER: '2011'
    TALB: Brötbients
    TRCK: '1'
    TPE1: Panix Illtone
    TIT2: Brötbient
previousTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/br-tbients/2-br-tbient-too.mp3
  audio: /assets/albums/br-tbients/2-br-tbient-too.mp3
  slug: br-tbients/2-br-tbient-too
  albumSlug: br-tbients
  trackSlug: 2-br-tbient-too
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/br-tbients/2-br-tbient-too.jpeg
  cover: /assets/albums/br-tbients/2-br-tbient-too.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 90.48816326530613
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2011'
      - id: TALB
        value: Brötbients
      - id: TRCK
        value: '2'
      - id: TPE1
        value: Panix Illtone
      - id: TIT2
        value: Brötbient Too
  quality:
    warnings: []
  common:
    track:
      'no': 2
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2011
    album: Brötbients
    artists:
      - Panix Illtone
    artist: Panix Illtone
    title: Brötbient Too
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2011'
      TALB: Brötbients
      TRCK: '2'
      TPE1: Panix Illtone
      TIT2: Brötbient Too
  all:
    TCON: Electronic
    TYER: '2011'
    TALB: Brötbients
    TRCK: '2'
    TPE1: Panix Illtone
    TIT2: Brötbient Too
---
