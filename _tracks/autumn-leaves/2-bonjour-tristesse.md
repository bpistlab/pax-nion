---
layout: track
path: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/autumn-leaves/2-bonjour-tristesse.mp3
audio: /assets/albums/autumn-leaves/2-bonjour-tristesse.mp3
slug: autumn-leaves/2-bonjour-tristesse
albumSlug: autumn-leaves
trackSlug: 2-bonjour-tristesse
coverPath: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/autumn-leaves/2-bonjour-tristesse.jpeg
cover: /assets/albums/autumn-leaves/2-bonjour-tristesse.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 320000
  codecProfile: CBR
  tool: 'LAME3.97 '
  duration: 163.7616326530612
native:
  ID3v2.3:
    - id: TRCK
      value: '2'
    - id: WXXX
      value: &ref_0
        description: ''
        url: ''
    - id: TCON
      value: Electronic
    - id: TALB
      value: Autumn Leaves
    - id: TPE1
      value: Panixonik
    - id: TIT2
      value: Bonjour Tristesse
    - id: TYER
      value: '2009'
quality:
  warnings: []
common:
  track:
    'no': 2
    of: null
  disk:
    'no': null
    of: null
  genre:
    - Electronic
  album: Autumn Leaves
  artists:
    - Panixonik
  artist: Panixonik
  title: Bonjour Tristesse
  year: 2009
transformed:
  ID3v2.3:
    TRCK: '2'
    WXXX: *ref_0
    TCON: Electronic
    TALB: Autumn Leaves
    TPE1: Panixonik
    TIT2: Bonjour Tristesse
    TYER: '2009'
all:
  TRCK: '2'
  WXXX: *ref_0
  TCON: Electronic
  TALB: Autumn Leaves
  TPE1: Panixonik
  TIT2: Bonjour Tristesse
  TYER: '2009'
nextTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/autumn-leaves/3-kafferep.mp3
  audio: /assets/albums/autumn-leaves/3-kafferep.mp3
  slug: autumn-leaves/3-kafferep
  albumSlug: autumn-leaves
  trackSlug: 3-kafferep
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/autumn-leaves/3-kafferep.jpeg
  cover: /assets/albums/autumn-leaves/3-kafferep.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME 3.97UU
    duration: 179.04326530612244
  native:
    ID3v2.3:
      - id: TRCK
        value: '3'
      - id: WXXX
        value: &ref_1
          description: ''
          url: ''
      - id: TCON
        value: Electronic
      - id: TALB
        value: Autumn Leaves
      - id: TPE1
        value: Panixonik
      - id: TIT2
        value: Kafferep
      - id: TYER
        value: '2009'
  quality:
    warnings: []
  common:
    track:
      'no': 3
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    album: Autumn Leaves
    artists:
      - Panixonik
    artist: Panixonik
    title: Kafferep
    year: 2009
  transformed:
    ID3v2.3:
      TRCK: '3'
      WXXX: *ref_1
      TCON: Electronic
      TALB: Autumn Leaves
      TPE1: Panixonik
      TIT2: Kafferep
      TYER: '2009'
  all:
    TRCK: '3'
    WXXX: *ref_1
    TCON: Electronic
    TALB: Autumn Leaves
    TPE1: Panixonik
    TIT2: Kafferep
    TYER: '2009'
previousTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/autumn-leaves/1-hot-ears.mp3
  audio: /assets/albums/autumn-leaves/1-hot-ears.mp3
  slug: autumn-leaves/1-hot-ears
  albumSlug: autumn-leaves
  trackSlug: 1-hot-ears
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/autumn-leaves/1-hot-ears.jpeg
  cover: /assets/albums/autumn-leaves/1-hot-ears.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME 3.97UU
    duration: 165.7208163265306
  native:
    ID3v2.3:
      - id: TRCK
        value: '1'
      - id: WXXX
        value: &ref_2
          description: ''
          url: ''
      - id: TCON
        value: Electronic
      - id: TALB
        value: Autumn Leaves
      - id: TPE1
        value: Panixonik
      - id: TIT2
        value: Hot Ears
      - id: TYER
        value: '2009'
  quality:
    warnings: []
  common:
    track:
      'no': 1
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    album: Autumn Leaves
    artists:
      - Panixonik
    artist: Panixonik
    title: Hot Ears
    year: 2009
  transformed:
    ID3v2.3:
      TRCK: '1'
      WXXX: *ref_2
      TCON: Electronic
      TALB: Autumn Leaves
      TPE1: Panixonik
      TIT2: Hot Ears
      TYER: '2009'
  all:
    TRCK: '1'
    WXXX: *ref_2
    TCON: Electronic
    TALB: Autumn Leaves
    TPE1: Panixonik
    TIT2: Hot Ears
    TYER: '2009'
---
