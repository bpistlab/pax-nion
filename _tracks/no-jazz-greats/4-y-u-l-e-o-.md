---
layout: track
path: /mnt/usb32gb/organized-pax-nion/assets/albums/no-jazz-greats/4-y-u-l-e-o-.mp3
audio: /assets/albums/no-jazz-greats/4-y-u-l-e-o-.mp3
slug: no-jazz-greats/4-y-u-l-e-o-
albumSlug: no-jazz-greats
trackSlug: 4-y-u-l-e-o-
coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/no-jazz-greats/4-y-u-l-e-o-.jpeg
cover: /assets/albums/no-jazz-greats/4-y-u-l-e-o-.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 48000
  numberOfChannels: 2
  bitrate: 320000
  codecProfile: CBR
  tool: LAME3.99r
  duration: 99.072
native:
  ID3v2.3:
    - id: TCON
      value: Electronic
    - id: TYER
      value: '2012'
    - id: TALB
      value: No Jazz Greats
    - id: TRCK
      value: '4'
    - id: TPE1
      value: Panix Hilton
    - id: TIT2
      value: Y.U.L.E.O.
quality:
  warnings: []
common:
  track:
    'no': 4
    of: null
  disk:
    'no': null
    of: null
  genre:
    - Electronic
  year: 2012
  album: No Jazz Greats
  artists:
    - Panix Hilton
  artist: Panix Hilton
  title: Y.U.L.E.O.
transformed:
  ID3v2.3:
    TCON: Electronic
    TYER: '2012'
    TALB: No Jazz Greats
    TRCK: '4'
    TPE1: Panix Hilton
    TIT2: Y.U.L.E.O.
all:
  TCON: Electronic
  TYER: '2012'
  TALB: No Jazz Greats
  TRCK: '4'
  TPE1: Panix Hilton
  TIT2: Y.U.L.E.O.
nextTrack:
  path: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/no-jazz-greats/5-all-my-days.mp3
  audio: /assets/albums/no-jazz-greats/5-all-my-days.mp3
  slug: no-jazz-greats/5-all-my-days
  albumSlug: no-jazz-greats
  trackSlug: 5-all-my-days
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/no-jazz-greats/5-all-my-days.jpeg
  cover: /assets/albums/no-jazz-greats/5-all-my-days.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 116.11428571428571
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2013'
      - id: TALB
        value: No Jazz Greats
      - id: TRCK
        value: '5'
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: All My Days
  quality:
    warnings: []
  common:
    track:
      'no': 5
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2013
    album: No Jazz Greats
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: All My Days
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2013'
      TALB: No Jazz Greats
      TRCK: '5'
      TPE1: Panix Hilton
      TIT2: All My Days
  all:
    TCON: Electronic
    TYER: '2013'
    TALB: No Jazz Greats
    TRCK: '5'
    TPE1: Panix Hilton
    TIT2: All My Days
previousTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/no-jazz-greats/3-pianamas.mp3
  audio: /assets/albums/no-jazz-greats/3-pianamas.mp3
  slug: no-jazz-greats/3-pianamas
  albumSlug: no-jazz-greats
  trackSlug: 3-pianamas
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/no-jazz-greats/3-pianamas.jpeg
  cover: /assets/albums/no-jazz-greats/3-pianamas.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 48000
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.99r
    duration: 116.232
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2013'
      - id: TALB
        value: No Jazz Greats
      - id: TRCK
        value: '3'
      - id: TIT2
        value: Pianamas
      - id: TPE1
        value: Panix Hilton
  quality:
    warnings: []
  common:
    track:
      'no': 3
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2013
    album: No Jazz Greats
    title: Pianamas
    artists:
      - Panix Hilton
    artist: Panix Hilton
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2013'
      TALB: No Jazz Greats
      TRCK: '3'
      TIT2: Pianamas
      TPE1: Panix Hilton
  all:
    TCON: Electronic
    TYER: '2013'
    TALB: No Jazz Greats
    TRCK: '3'
    TIT2: Pianamas
    TPE1: Panix Hilton
---
