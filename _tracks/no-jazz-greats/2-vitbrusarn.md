---
layout: track
path: /mnt/usb32gb/organized-pax-nion/assets/albums/no-jazz-greats/2-vitbrusarn.mp3
audio: /assets/albums/no-jazz-greats/2-vitbrusarn.mp3
slug: no-jazz-greats/2-vitbrusarn
albumSlug: no-jazz-greats
trackSlug: 2-vitbrusarn
coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/no-jazz-greats/2-vitbrusarn.jpeg
cover: /assets/albums/no-jazz-greats/2-vitbrusarn.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 320000
  codecProfile: CBR
  tool: LAME3.98r
  duration: 87.53632653061224
native:
  ID3v2.3:
    - id: TCON
      value: Electronic
    - id: TYER
      value: '2013'
    - id: TALB
      value: No Jazz Greats
    - id: TPE1
      value: Panix Hilton
    - id: TIT2
      value: Vitbrusarn
    - id: TRCK
      value: '2'
quality:
  warnings: []
common:
  track:
    'no': 2
    of: null
  disk:
    'no': null
    of: null
  genre:
    - Electronic
  year: 2013
  album: No Jazz Greats
  artists:
    - Panix Hilton
  artist: Panix Hilton
  title: Vitbrusarn
transformed:
  ID3v2.3:
    TCON: Electronic
    TYER: '2013'
    TALB: No Jazz Greats
    TPE1: Panix Hilton
    TIT2: Vitbrusarn
    TRCK: '2'
all:
  TCON: Electronic
  TYER: '2013'
  TALB: No Jazz Greats
  TPE1: Panix Hilton
  TIT2: Vitbrusarn
  TRCK: '2'
nextTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/no-jazz-greats/3-pianamas.mp3
  audio: /assets/albums/no-jazz-greats/3-pianamas.mp3
  slug: no-jazz-greats/3-pianamas
  albumSlug: no-jazz-greats
  trackSlug: 3-pianamas
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/no-jazz-greats/3-pianamas.jpeg
  cover: /assets/albums/no-jazz-greats/3-pianamas.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 48000
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.99r
    duration: 116.232
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2013'
      - id: TALB
        value: No Jazz Greats
      - id: TRCK
        value: '3'
      - id: TIT2
        value: Pianamas
      - id: TPE1
        value: Panix Hilton
  quality:
    warnings: []
  common:
    track:
      'no': 3
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2013
    album: No Jazz Greats
    title: Pianamas
    artists:
      - Panix Hilton
    artist: Panix Hilton
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2013'
      TALB: No Jazz Greats
      TRCK: '3'
      TIT2: Pianamas
      TPE1: Panix Hilton
  all:
    TCON: Electronic
    TYER: '2013'
    TALB: No Jazz Greats
    TRCK: '3'
    TIT2: Pianamas
    TPE1: Panix Hilton
previousTrack:
  path: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/no-jazz-greats/1-forever-dusted.mp3
  audio: /assets/albums/no-jazz-greats/1-forever-dusted.mp3
  slug: no-jazz-greats/1-forever-dusted
  albumSlug: no-jazz-greats
  trackSlug: 1-forever-dusted
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/no-jazz-greats/1-forever-dusted.jpeg
  cover: /assets/albums/no-jazz-greats/1-forever-dusted.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 112.3004081632653
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2013'
      - id: TALB
        value: No Jazz Greats
      - id: TRCK
        value: '1'
      - id: TIT2
        value: Forever Dusted
      - id: TPE1
        value: Panix Hilton
  quality:
    warnings: []
  common:
    track:
      'no': 1
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2013
    album: No Jazz Greats
    title: Forever Dusted
    artists:
      - Panix Hilton
    artist: Panix Hilton
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2013'
      TALB: No Jazz Greats
      TRCK: '1'
      TIT2: Forever Dusted
      TPE1: Panix Hilton
  all:
    TCON: Electronic
    TYER: '2013'
    TALB: No Jazz Greats
    TRCK: '1'
    TIT2: Forever Dusted
    TPE1: Panix Hilton
---
