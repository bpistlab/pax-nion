---
layout: track
path: /mnt/usb32gb/organized-pax-nion/assets/albums/no-jazz-greats/8-bonus-beats.mp3
audio: /assets/albums/no-jazz-greats/8-bonus-beats.mp3
slug: no-jazz-greats/8-bonus-beats
albumSlug: no-jazz-greats
trackSlug: 8-bonus-beats
coverPath: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/no-jazz-greats/8-bonus-beats.jpeg
cover: /assets/albums/no-jazz-greats/8-bonus-beats.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 320000
  codecProfile: CBR
  tool: LAME3.98r
  duration: 248.32
native:
  ID3v2.3:
    - id: TCON
      value: Electronic
    - id: TYER
      value: '2013'
    - id: TALB
      value: No Jazz Greats
    - id: TRCK
      value: '8'
    - id: TPE1
      value: Panix Hilton
    - id: TIT2
      value: Bonus Beats
quality:
  warnings: []
common:
  track:
    'no': 8
    of: null
  disk:
    'no': null
    of: null
  genre:
    - Electronic
  year: 2013
  album: No Jazz Greats
  artists:
    - Panix Hilton
  artist: Panix Hilton
  title: Bonus Beats
transformed:
  ID3v2.3:
    TCON: Electronic
    TYER: '2013'
    TALB: No Jazz Greats
    TRCK: '8'
    TPE1: Panix Hilton
    TIT2: Bonus Beats
all:
  TCON: Electronic
  TYER: '2013'
  TALB: No Jazz Greats
  TRCK: '8'
  TPE1: Panix Hilton
  TIT2: Bonus Beats
nextTrack:
  path: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/no-jazz-greats/1-forever-dusted.mp3
  audio: /assets/albums/no-jazz-greats/1-forever-dusted.mp3
  slug: no-jazz-greats/1-forever-dusted
  albumSlug: no-jazz-greats
  trackSlug: 1-forever-dusted
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/no-jazz-greats/1-forever-dusted.jpeg
  cover: /assets/albums/no-jazz-greats/1-forever-dusted.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 112.3004081632653
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2013'
      - id: TALB
        value: No Jazz Greats
      - id: TRCK
        value: '1'
      - id: TIT2
        value: Forever Dusted
      - id: TPE1
        value: Panix Hilton
  quality:
    warnings: []
  common:
    track:
      'no': 1
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2013
    album: No Jazz Greats
    title: Forever Dusted
    artists:
      - Panix Hilton
    artist: Panix Hilton
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2013'
      TALB: No Jazz Greats
      TRCK: '1'
      TIT2: Forever Dusted
      TPE1: Panix Hilton
  all:
    TCON: Electronic
    TYER: '2013'
    TALB: No Jazz Greats
    TRCK: '1'
    TIT2: Forever Dusted
    TPE1: Panix Hilton
previousTrack:
  path: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/no-jazz-greats/7-sleepers-ain-t-keepers.mp3
  audio: /assets/albums/no-jazz-greats/7-sleepers-ain-t-keepers.mp3
  slug: no-jazz-greats/7-sleepers-ain-t-keepers
  albumSlug: no-jazz-greats
  trackSlug: 7-sleepers-ain-t-keepers
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/no-jazz-greats/7-sleepers-ain-t-keepers.jpeg
  cover: /assets/albums/no-jazz-greats/7-sleepers-ain-t-keepers.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 48000
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.99r
    duration: 85.752
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2013'
      - id: TALB
        value: No Jazz Greats
      - id: TRCK
        value: '7'
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: Sleepers Ain't Keepers
  quality:
    warnings: []
  common:
    track:
      'no': 7
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2013
    album: No Jazz Greats
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: Sleepers Ain't Keepers
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2013'
      TALB: No Jazz Greats
      TRCK: '7'
      TPE1: Panix Hilton
      TIT2: Sleepers Ain't Keepers
  all:
    TCON: Electronic
    TYER: '2013'
    TALB: No Jazz Greats
    TRCK: '7'
    TPE1: Panix Hilton
    TIT2: Sleepers Ain't Keepers
---
