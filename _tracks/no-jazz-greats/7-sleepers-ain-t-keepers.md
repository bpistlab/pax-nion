---
layout: track
path: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/no-jazz-greats/7-sleepers-ain-t-keepers.mp3
audio: /assets/albums/no-jazz-greats/7-sleepers-ain-t-keepers.mp3
slug: no-jazz-greats/7-sleepers-ain-t-keepers
albumSlug: no-jazz-greats
trackSlug: 7-sleepers-ain-t-keepers
coverPath: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/no-jazz-greats/7-sleepers-ain-t-keepers.jpeg
cover: /assets/albums/no-jazz-greats/7-sleepers-ain-t-keepers.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 48000
  numberOfChannels: 2
  bitrate: 320000
  codecProfile: CBR
  tool: LAME3.99r
  duration: 85.752
native:
  ID3v2.3:
    - id: TCON
      value: Electronic
    - id: TYER
      value: '2013'
    - id: TALB
      value: No Jazz Greats
    - id: TRCK
      value: '7'
    - id: TPE1
      value: Panix Hilton
    - id: TIT2
      value: Sleepers Ain't Keepers
quality:
  warnings: []
common:
  track:
    'no': 7
    of: null
  disk:
    'no': null
    of: null
  genre:
    - Electronic
  year: 2013
  album: No Jazz Greats
  artists:
    - Panix Hilton
  artist: Panix Hilton
  title: Sleepers Ain't Keepers
transformed:
  ID3v2.3:
    TCON: Electronic
    TYER: '2013'
    TALB: No Jazz Greats
    TRCK: '7'
    TPE1: Panix Hilton
    TIT2: Sleepers Ain't Keepers
all:
  TCON: Electronic
  TYER: '2013'
  TALB: No Jazz Greats
  TRCK: '7'
  TPE1: Panix Hilton
  TIT2: Sleepers Ain't Keepers
nextTrack:
  path: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/no-jazz-greats/8-bonus-beats.mp3
  audio: /assets/albums/no-jazz-greats/8-bonus-beats.mp3
  slug: no-jazz-greats/8-bonus-beats
  albumSlug: no-jazz-greats
  trackSlug: 8-bonus-beats
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/no-jazz-greats/8-bonus-beats.jpeg
  cover: /assets/albums/no-jazz-greats/8-bonus-beats.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 248.32
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2013'
      - id: TALB
        value: No Jazz Greats
      - id: TRCK
        value: '8'
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: Bonus Beats
  quality:
    warnings: []
  common:
    track:
      'no': 8
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2013
    album: No Jazz Greats
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: Bonus Beats
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2013'
      TALB: No Jazz Greats
      TRCK: '8'
      TPE1: Panix Hilton
      TIT2: Bonus Beats
  all:
    TCON: Electronic
    TYER: '2013'
    TALB: No Jazz Greats
    TRCK: '8'
    TPE1: Panix Hilton
    TIT2: Bonus Beats
previousTrack:
  path: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/no-jazz-greats/6-2-tired-least-fave-kickdrum-shortmix-.mp3
  audio: /assets/albums/no-jazz-greats/6-2-tired-least-fave-kickdrum-shortmix-.mp3
  slug: no-jazz-greats/6-2-tired-least-fave-kickdrum-shortmix-
  albumSlug: no-jazz-greats
  trackSlug: 6-2-tired-least-fave-kickdrum-shortmix-
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/no-jazz-greats/6-2-tired-least-fave-kickdrum-shortmix-.jpeg
  cover: /assets/albums/no-jazz-greats/6-2-tired-least-fave-kickdrum-shortmix-.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.99r
    duration: 106.71020408163265
  native:
    ID3v2.3:
      - id: TRCK
        value: '6'
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2013'
      - id: TALB
        value: No Jazz Greats
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: 2 Tired (Least fave kickdrum shortmix)
  quality:
    warnings: []
  common:
    track:
      'no': 6
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2013
    album: No Jazz Greats
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: 2 Tired (Least fave kickdrum shortmix)
  transformed:
    ID3v2.3:
      TRCK: '6'
      TCON: Electronic
      TYER: '2013'
      TALB: No Jazz Greats
      TPE1: Panix Hilton
      TIT2: 2 Tired (Least fave kickdrum shortmix)
  all:
    TRCK: '6'
    TCON: Electronic
    TYER: '2013'
    TALB: No Jazz Greats
    TPE1: Panix Hilton
    TIT2: 2 Tired (Least fave kickdrum shortmix)
---
