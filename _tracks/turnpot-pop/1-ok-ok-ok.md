---
layout: track
path: /mnt/usb32gb/organized-pax-nion/assets/albums/turnpot-pop/1-ok-ok-ok.mp3
audio: /assets/albums/turnpot-pop/1-ok-ok-ok.mp3
slug: turnpot-pop/1-ok-ok-ok
albumSlug: turnpot-pop
trackSlug: 1-ok-ok-ok
coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/turnpot-pop/1-ok-ok-ok.jpeg
cover: /assets/albums/turnpot-pop/1-ok-ok-ok.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 256000
  codecProfile: CBR
  tool: LAME 3.97 (
  duration: 120.0065306122449
native:
  ID3v2.3:
    - id: TRCK
      value: '1'
    - id: TCON
      value: Electronic
    - id: TALB
      value: Turnpot Pop
    - id: TPE1
      value: Panixonik
    - id: TIT2
      value: OK OK OK
    - id: TYER
      value: '2009'
quality:
  warnings: []
common:
  track:
    'no': 1
    of: null
  disk:
    'no': null
    of: null
  genre:
    - Electronic
  album: Turnpot Pop
  artists:
    - Panixonik
  artist: Panixonik
  title: OK OK OK
  year: 2009
transformed:
  ID3v2.3:
    TRCK: '1'
    TCON: Electronic
    TALB: Turnpot Pop
    TPE1: Panixonik
    TIT2: OK OK OK
    TYER: '2009'
all:
  TRCK: '1'
  TCON: Electronic
  TALB: Turnpot Pop
  TPE1: Panixonik
  TIT2: OK OK OK
  TYER: '2009'
nextTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/turnpot-pop/2-magsyra.mp3
  audio: /assets/albums/turnpot-pop/2-magsyra.mp3
  slug: turnpot-pop/2-magsyra
  albumSlug: turnpot-pop
  trackSlug: 2-magsyra
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/turnpot-pop/2-magsyra.jpeg
  cover: /assets/albums/turnpot-pop/2-magsyra.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME 3.97UU
    duration: 95.97387755102041
  native:
    ID3v2.3:
      - id: TRCK
        value: '2'
      - id: TCON
        value: Electronic
      - id: TALB
        value: Turnpot Pop
      - id: TPE1
        value: Panixonik
      - id: TIT2
        value: Magsyra
      - id: TYER
        value: '2009'
  quality:
    warnings: []
  common:
    track:
      'no': 2
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    album: Turnpot Pop
    artists:
      - Panixonik
    artist: Panixonik
    title: Magsyra
    year: 2009
  transformed:
    ID3v2.3:
      TRCK: '2'
      TCON: Electronic
      TALB: Turnpot Pop
      TPE1: Panixonik
      TIT2: Magsyra
      TYER: '2009'
  all:
    TRCK: '2'
    TCON: Electronic
    TALB: Turnpot Pop
    TPE1: Panixonik
    TIT2: Magsyra
    TYER: '2009'
previousTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/turnpot-pop/4-87-vs-91.mp3
  audio: /assets/albums/turnpot-pop/4-87-vs-91.mp3
  slug: turnpot-pop/4-87-vs-91
  albumSlug: turnpot-pop
  trackSlug: 4-87-vs-91
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/turnpot-pop/4-87-vs-91.jpeg
  cover: /assets/albums/turnpot-pop/4-87-vs-91.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 256000
    codecProfile: CBR
    tool: LAME 3.97 (
    duration: 127.3730612244898
  native:
    ID3v2.3:
      - id: TRCK
        value: '4'
      - id: TCON
        value: Electronic
      - id: TALB
        value: Turnpot Pop
      - id: TPE1
        value: Panixonik
      - id: TIT2
        value: 87 VS 91
      - id: TYER
        value: '2009'
  quality:
    warnings: []
  common:
    track:
      'no': 4
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    album: Turnpot Pop
    artists:
      - Panixonik
    artist: Panixonik
    title: 87 VS 91
    year: 2009
  transformed:
    ID3v2.3:
      TRCK: '4'
      TCON: Electronic
      TALB: Turnpot Pop
      TPE1: Panixonik
      TIT2: 87 VS 91
      TYER: '2009'
  all:
    TRCK: '4'
    TCON: Electronic
    TALB: Turnpot Pop
    TPE1: Panixonik
    TIT2: 87 VS 91
    TYER: '2009'
---
