---
layout: track
path: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/tracker-dust-96-01-vol-1/7-de-kuyper.mp3
audio: /assets/albums/tracker-dust-96-01-vol-1/7-de-kuyper.mp3
slug: tracker-dust-96-01-vol-1/7-de-kuyper
albumSlug: tracker-dust-96-01-vol-1
trackSlug: 7-de-kuyper
coverPath: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/tracker-dust-96-01-vol-1/7-de-kuyper.jpeg
cover: /assets/albums/tracker-dust-96-01-vol-1/7-de-kuyper.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 1
  bitrate: 256000
  codecProfile: CBR
  tool: LAME3.98r
  duration: 112.90122448979592
native:
  ID3v2.3:
    - id: TALB
      value: Tracker Dust 96-01 Vol 1
    - id: TRCK
      value: '7'
    - id: TCON
      value: Electronic
    - id: TPE1
      value: Panix
    - id: TIT2
      value: De Kuyper
    - id: TYER
      value: '2005'
quality:
  warnings: []
common:
  track:
    'no': 7
    of: null
  disk:
    'no': null
    of: null
  album: Tracker Dust 96-01 Vol 1
  genre:
    - Electronic
  artists:
    - Panix
  artist: Panix
  title: De Kuyper
  year: 2005
transformed:
  ID3v2.3:
    TALB: Tracker Dust 96-01 Vol 1
    TRCK: '7'
    TCON: Electronic
    TPE1: Panix
    TIT2: De Kuyper
    TYER: '2005'
all:
  TALB: Tracker Dust 96-01 Vol 1
  TRCK: '7'
  TCON: Electronic
  TPE1: Panix
  TIT2: De Kuyper
  TYER: '2005'
nextTrack:
  path: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/tracker-dust-96-01-vol-1/8-future.mp3
  audio: /assets/albums/tracker-dust-96-01-vol-1/8-future.mp3
  slug: tracker-dust-96-01-vol-1/8-future
  albumSlug: tracker-dust-96-01-vol-1
  trackSlug: 8-future
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/tracker-dust-96-01-vol-1/8-future.jpeg
  cover: /assets/albums/tracker-dust-96-01-vol-1/8-future.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 1
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 195.10857142857142
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TALB
        value: Tracker Dust 96-01 Vol 1
      - id: TRCK
        value: '8'
      - id: TPE1
        value: Panix
      - id: TIT2
        value: Future
      - id: TYER
        value: '2005'
  quality:
    warnings: []
  common:
    track:
      'no': 8
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    album: Tracker Dust 96-01 Vol 1
    artists:
      - Panix
    artist: Panix
    title: Future
    year: 2005
  transformed:
    ID3v2.3:
      TCON: Electronic
      TALB: Tracker Dust 96-01 Vol 1
      TRCK: '8'
      TPE1: Panix
      TIT2: Future
      TYER: '2005'
  all:
    TCON: Electronic
    TALB: Tracker Dust 96-01 Vol 1
    TRCK: '8'
    TPE1: Panix
    TIT2: Future
    TYER: '2005'
previousTrack:
  path: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/tracker-dust-96-01-vol-1/6-hipphappbitzyoyozwei.mp3
  audio: /assets/albums/tracker-dust-96-01-vol-1/6-hipphappbitzyoyozwei.mp3
  slug: tracker-dust-96-01-vol-1/6-hipphappbitzyoyozwei
  albumSlug: tracker-dust-96-01-vol-1
  trackSlug: 6-hipphappbitzyoyozwei
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/tracker-dust-96-01-vol-1/6-hipphappbitzyoyozwei.jpeg
  cover: /assets/albums/tracker-dust-96-01-vol-1/6-hipphappbitzyoyozwei.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 1
    bitrate: 112000
    codecProfile: CBR
    numberOfSamples: 3384576
    duration: 76.74775510204081
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TALB
        value: Tracker Dust 96-01 Vol 1
      - id: TRCK
        value: '6'
      - id: TPE1
        value: Panix
      - id: TIT2
        value: Hipphappbitzyoyozwei
      - id: TYER
        value: '2005'
  quality:
    warnings: []
  common:
    track:
      'no': 6
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    album: Tracker Dust 96-01 Vol 1
    artists:
      - Panix
    artist: Panix
    title: Hipphappbitzyoyozwei
    year: 2005
  transformed:
    ID3v2.3:
      TCON: Electronic
      TALB: Tracker Dust 96-01 Vol 1
      TRCK: '6'
      TPE1: Panix
      TIT2: Hipphappbitzyoyozwei
      TYER: '2005'
  all:
    TCON: Electronic
    TALB: Tracker Dust 96-01 Vol 1
    TRCK: '6'
    TPE1: Panix
    TIT2: Hipphappbitzyoyozwei
    TYER: '2005'
---
