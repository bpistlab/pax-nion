---
layout: track
path: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/tracker-dust-96-01-vol-1/3-cv003.mp3
audio: /assets/albums/tracker-dust-96-01-vol-1/3-cv003.mp3
slug: tracker-dust-96-01-vol-1/3-cv003
albumSlug: tracker-dust-96-01-vol-1
trackSlug: 3-cv003
coverPath: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/tracker-dust-96-01-vol-1/3-cv003.jpeg
cover: /assets/albums/tracker-dust-96-01-vol-1/3-cv003.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 1
  bitrate: 256000
  codecProfile: CBR
  tool: LAME3.98r
  duration: 144.24816326530612
native:
  ID3v2.3:
    - id: TRCK
      value: '3'
    - id: TALB
      value: Tracker Dust 96-01 Vol 1
    - id: TPE1
      value: Panix
    - id: TIT2
      value: CV003
    - id: TCON
      value: Electronic
    - id: TYER
      value: '2005'
quality:
  warnings: []
common:
  track:
    'no': 3
    of: null
  disk:
    'no': null
    of: null
  album: Tracker Dust 96-01 Vol 1
  artists:
    - Panix
  artist: Panix
  title: CV003
  genre:
    - Electronic
  year: 2005
transformed:
  ID3v2.3:
    TRCK: '3'
    TALB: Tracker Dust 96-01 Vol 1
    TPE1: Panix
    TIT2: CV003
    TCON: Electronic
    TYER: '2005'
all:
  TRCK: '3'
  TALB: Tracker Dust 96-01 Vol 1
  TPE1: Panix
  TIT2: CV003
  TCON: Electronic
  TYER: '2005'
nextTrack:
  path: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/tracker-dust-96-01-vol-1/4-husnix.mp3
  audio: /assets/albums/tracker-dust-96-01-vol-1/4-husnix.mp3
  slug: tracker-dust-96-01-vol-1/4-husnix
  albumSlug: tracker-dust-96-01-vol-1
  trackSlug: 4-husnix
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/tracker-dust-96-01-vol-1/4-husnix.jpeg
  cover: /assets/albums/tracker-dust-96-01-vol-1/4-husnix.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 1
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 177.8677551020408
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TALB
        value: Tracker Dust 96-01 Vol 1
      - id: TRCK
        value: '4'
      - id: TPE1
        value: Panix
      - id: TIT2
        value: Husnix
      - id: TYER
        value: '2005'
  quality:
    warnings: []
  common:
    track:
      'no': 4
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    album: Tracker Dust 96-01 Vol 1
    artists:
      - Panix
    artist: Panix
    title: Husnix
    year: 2005
  transformed:
    ID3v2.3:
      TCON: Electronic
      TALB: Tracker Dust 96-01 Vol 1
      TRCK: '4'
      TPE1: Panix
      TIT2: Husnix
      TYER: '2005'
  all:
    TCON: Electronic
    TALB: Tracker Dust 96-01 Vol 1
    TRCK: '4'
    TPE1: Panix
    TIT2: Husnix
    TYER: '2005'
previousTrack:
  path: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/tracker-dust-96-01-vol-1/2-cv002.mp3
  audio: /assets/albums/tracker-dust-96-01-vol-1/2-cv002.mp3
  slug: tracker-dust-96-01-vol-1/2-cv002
  albumSlug: tracker-dust-96-01-vol-1
  trackSlug: 2-cv002
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/tracker-dust-96-01-vol-1/2-cv002.jpeg
  cover: /assets/albums/tracker-dust-96-01-vol-1/2-cv002.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 1
    bitrate: 256000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 38.50448979591837
  native:
    ID3v2.3:
      - id: TRCK
        value: '2'
      - id: TCON
        value: Electronic
      - id: TALB
        value: Tracker Dust 96-01 Vol 1
      - id: TPE1
        value: Panix
      - id: TIT2
        value: CV002
      - id: TYER
        value: '2005'
  quality:
    warnings: []
  common:
    track:
      'no': 2
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    album: Tracker Dust 96-01 Vol 1
    artists:
      - Panix
    artist: Panix
    title: CV002
    year: 2005
  transformed:
    ID3v2.3:
      TRCK: '2'
      TCON: Electronic
      TALB: Tracker Dust 96-01 Vol 1
      TPE1: Panix
      TIT2: CV002
      TYER: '2005'
  all:
    TRCK: '2'
    TCON: Electronic
    TALB: Tracker Dust 96-01 Vol 1
    TPE1: Panix
    TIT2: CV002
    TYER: '2005'
---
