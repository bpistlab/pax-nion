---
layout: track
path: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/tracker-dust-96-01-vol-1/10-minichip-zwei.mp3
audio: /assets/albums/tracker-dust-96-01-vol-1/10-minichip-zwei.mp3
slug: tracker-dust-96-01-vol-1/10-minichip-zwei
albumSlug: tracker-dust-96-01-vol-1
trackSlug: 10-minichip-zwei
coverPath: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/tracker-dust-96-01-vol-1/10-minichip-zwei.jpeg
cover: /assets/albums/tracker-dust-96-01-vol-1/10-minichip-zwei.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 1
  bitrate: 320000
  codecProfile: CBR
  tool: LAME3.98r
  duration: 115.35673469387756
native:
  ID3v2.3:
    - id: TCON
      value: Electronic
    - id: TRCK
      value: '10'
    - id: TALB
      value: Tracker Dust 96-01 Vol 1
    - id: TPE1
      value: Panix
    - id: TIT2
      value: Minichip Zwei
    - id: TYER
      value: '2005'
quality:
  warnings: []
common:
  track:
    'no': 10
    of: null
  disk:
    'no': null
    of: null
  genre:
    - Electronic
  album: Tracker Dust 96-01 Vol 1
  artists:
    - Panix
  artist: Panix
  title: Minichip Zwei
  year: 2005
transformed:
  ID3v2.3:
    TCON: Electronic
    TRCK: '10'
    TALB: Tracker Dust 96-01 Vol 1
    TPE1: Panix
    TIT2: Minichip Zwei
    TYER: '2005'
all:
  TCON: Electronic
  TRCK: '10'
  TALB: Tracker Dust 96-01 Vol 1
  TPE1: Panix
  TIT2: Minichip Zwei
  TYER: '2005'
nextTrack:
  path: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/tracker-dust-96-01-vol-1/1-cv001.mp3
  audio: /assets/albums/tracker-dust-96-01-vol-1/1-cv001.mp3
  slug: tracker-dust-96-01-vol-1/1-cv001
  albumSlug: tracker-dust-96-01-vol-1
  trackSlug: 1-cv001
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/tracker-dust-96-01-vol-1/1-cv001.jpeg
  cover: /assets/albums/tracker-dust-96-01-vol-1/1-cv001.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 1
    bitrate: 256000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 147.1738775510204
  native:
    ID3v2.3:
      - id: TRCK
        value: '1'
      - id: TCON
        value: Electronic
      - id: TALB
        value: Tracker Dust 96-01 Vol 1
      - id: TPE1
        value: Panix
      - id: TIT2
        value: CV001
      - id: TYER
        value: '2005'
  quality:
    warnings: []
  common:
    track:
      'no': 1
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    album: Tracker Dust 96-01 Vol 1
    artists:
      - Panix
    artist: Panix
    title: CV001
    year: 2005
  transformed:
    ID3v2.3:
      TRCK: '1'
      TCON: Electronic
      TALB: Tracker Dust 96-01 Vol 1
      TPE1: Panix
      TIT2: CV001
      TYER: '2005'
  all:
    TRCK: '1'
    TCON: Electronic
    TALB: Tracker Dust 96-01 Vol 1
    TPE1: Panix
    TIT2: CV001
    TYER: '2005'
previousTrack:
  path: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/tracker-dust-96-01-vol-1/9-minichip-eins.mp3
  audio: /assets/albums/tracker-dust-96-01-vol-1/9-minichip-eins.mp3
  slug: tracker-dust-96-01-vol-1/9-minichip-eins
  albumSlug: tracker-dust-96-01-vol-1
  trackSlug: 9-minichip-eins
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/tracker-dust-96-01-vol-1/9-minichip-eins.jpeg
  cover: /assets/albums/tracker-dust-96-01-vol-1/9-minichip-eins.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 1
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 70.4
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TALB
        value: Tracker Dust 96-01 Vol 1
      - id: TRCK
        value: '9'
      - id: TPE1
        value: Panix
      - id: TIT2
        value: Minichip Eins
      - id: TYER
        value: '2005'
  quality:
    warnings: []
  common:
    track:
      'no': 9
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    album: Tracker Dust 96-01 Vol 1
    artists:
      - Panix
    artist: Panix
    title: Minichip Eins
    year: 2005
  transformed:
    ID3v2.3:
      TCON: Electronic
      TALB: Tracker Dust 96-01 Vol 1
      TRCK: '9'
      TPE1: Panix
      TIT2: Minichip Eins
      TYER: '2005'
  all:
    TCON: Electronic
    TALB: Tracker Dust 96-01 Vol 1
    TRCK: '9'
    TPE1: Panix
    TIT2: Minichip Eins
    TYER: '2005'
---
