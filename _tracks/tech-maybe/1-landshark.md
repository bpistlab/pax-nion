---
layout: track
path: /mnt/usb32gb/organized-pax-nion/assets/albums/tech-maybe/1-landshark.mp3
audio: /assets/albums/tech-maybe/1-landshark.mp3
slug: tech-maybe/1-landshark
albumSlug: tech-maybe
trackSlug: 1-landshark
coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/tech-maybe/1-landshark.jpeg
cover: /assets/albums/tech-maybe/1-landshark.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 320000
  codecProfile: CBR
  tool: LAME3.99r
  duration: 99.73551020408163
native:
  ID3v2.3:
    - id: TCON
      value: Electronic
    - id: TYER
      value: '2013'
    - id: TALB
      value: Tech Maybe
    - id: TRCK
      value: '1'
    - id: TPE1
      value: Panix Hilton
    - id: TIT2
      value: Landshark
quality:
  warnings: []
common:
  track:
    'no': 1
    of: null
  disk:
    'no': null
    of: null
  genre:
    - Electronic
  year: 2013
  album: Tech Maybe
  artists:
    - Panix Hilton
  artist: Panix Hilton
  title: Landshark
transformed:
  ID3v2.3:
    TCON: Electronic
    TYER: '2013'
    TALB: Tech Maybe
    TRCK: '1'
    TPE1: Panix Hilton
    TIT2: Landshark
all:
  TCON: Electronic
  TYER: '2013'
  TALB: Tech Maybe
  TRCK: '1'
  TPE1: Panix Hilton
  TIT2: Landshark
nextTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/tech-maybe/2-mosbrickan.mp3
  audio: /assets/albums/tech-maybe/2-mosbrickan.mp3
  slug: tech-maybe/2-mosbrickan
  albumSlug: tech-maybe
  trackSlug: 2-mosbrickan
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/tech-maybe/2-mosbrickan.jpeg
  cover: /assets/albums/tech-maybe/2-mosbrickan.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 116.11428571428571
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TALB
        value: Tech Maybe
      - id: TRCK
        value: '2'
      - id: TYER
        value: '2013'
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: Mosbrickan
  quality:
    warnings: []
  common:
    track:
      'no': 2
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    album: Tech Maybe
    year: 2013
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: Mosbrickan
  transformed:
    ID3v2.3:
      TCON: Electronic
      TALB: Tech Maybe
      TRCK: '2'
      TYER: '2013'
      TPE1: Panix Hilton
      TIT2: Mosbrickan
  all:
    TCON: Electronic
    TALB: Tech Maybe
    TRCK: '2'
    TYER: '2013'
    TPE1: Panix Hilton
    TIT2: Mosbrickan
previousTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/tech-maybe/3-troek.mp3
  audio: /assets/albums/tech-maybe/3-troek.mp3
  slug: tech-maybe/3-troek
  albumSlug: tech-maybe
  trackSlug: 3-troek
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/tech-maybe/3-troek.jpeg
  cover: /assets/albums/tech-maybe/3-troek.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 48000
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.99r
    duration: 99.072
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2013'
      - id: TALB
        value: Tech Maybe
      - id: TRCK
        value: '3'
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: Troek
  quality:
    warnings: []
  common:
    track:
      'no': 3
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2013
    album: Tech Maybe
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: Troek
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2013'
      TALB: Tech Maybe
      TRCK: '3'
      TPE1: Panix Hilton
      TIT2: Troek
  all:
    TCON: Electronic
    TYER: '2013'
    TALB: Tech Maybe
    TRCK: '3'
    TPE1: Panix Hilton
    TIT2: Troek
---
