---
layout: track
path: /mnt/usb32gb/organized-pax-nion/assets/albums/nights-ep/3-later-nights.mp3
audio: /assets/albums/nights-ep/3-later-nights.mp3
slug: nights-ep/3-later-nights
albumSlug: nights-ep
trackSlug: 3-later-nights
coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/nights-ep/3-later-nights.jpeg
cover: /assets/albums/nights-ep/3-later-nights.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 320000
  codecProfile: CBR
  tool: LAME3.98r
  duration: 118.0734693877551
native:
  ID3v2.3:
    - id: TCON
      value: Electronic
    - id: TYER
      value: '2012'
    - id: TALB
      value: Nights EP
    - id: TPE1
      value: Panix Hilton
    - id: TIT2
      value: Later Nights
    - id: TRCK
      value: '3'
quality:
  warnings: []
common:
  track:
    'no': 3
    of: null
  disk:
    'no': null
    of: null
  genre:
    - Electronic
  year: 2012
  album: Nights EP
  artists:
    - Panix Hilton
  artist: Panix Hilton
  title: Later Nights
transformed:
  ID3v2.3:
    TCON: Electronic
    TYER: '2012'
    TALB: Nights EP
    TPE1: Panix Hilton
    TIT2: Later Nights
    TRCK: '3'
all:
  TCON: Electronic
  TYER: '2012'
  TALB: Nights EP
  TPE1: Panix Hilton
  TIT2: Later Nights
  TRCK: '3'
nextTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/nights-ep/4-morning-after.mp3
  audio: /assets/albums/nights-ep/4-morning-after.mp3
  slug: nights-ep/4-morning-after
  albumSlug: nights-ep
  trackSlug: 4-morning-after
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/nights-ep/4-morning-after.jpeg
  cover: /assets/albums/nights-ep/4-morning-after.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 77.40081632653062
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2012'
      - id: TALB
        value: Nights EP
      - id: TRCK
        value: '4'
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: Morning After
  quality:
    warnings: []
  common:
    track:
      'no': 4
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2012
    album: Nights EP
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: Morning After
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2012'
      TALB: Nights EP
      TRCK: '4'
      TPE1: Panix Hilton
      TIT2: Morning After
  all:
    TCON: Electronic
    TYER: '2012'
    TALB: Nights EP
    TRCK: '4'
    TPE1: Panix Hilton
    TIT2: Morning After
previousTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/nights-ep/2-late-nights.mp3
  audio: /assets/albums/nights-ep/2-late-nights.mp3
  slug: nights-ep/2-late-nights
  albumSlug: nights-ep
  trackSlug: 2-late-nights
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/nights-ep/2-late-nights.jpeg
  cover: /assets/albums/nights-ep/2-late-nights.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 83.72244897959183
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2012'
      - id: TALB
        value: Nights EP
      - id: TRCK
        value: '2'
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: Late nights
  quality:
    warnings: []
  common:
    track:
      'no': 2
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2012
    album: Nights EP
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: Late nights
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2012'
      TALB: Nights EP
      TRCK: '2'
      TPE1: Panix Hilton
      TIT2: Late nights
  all:
    TCON: Electronic
    TYER: '2012'
    TALB: Nights EP
    TRCK: '2'
    TPE1: Panix Hilton
    TIT2: Late nights
---
