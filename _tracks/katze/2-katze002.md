---
layout: track
path: /mnt/usb32gb/organized-pax-nion/assets/albums/katze/2-katze002.mp3
audio: /assets/albums/katze/2-katze002.mp3
slug: katze/2-katze002
albumSlug: katze
trackSlug: 2-katze002
coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/katze/2-katze002.jpeg
cover: /assets/albums/katze/2-katze002.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 256000
  codecProfile: CBR
  tool: LAME 3.98.2
  duration: 46.027755102040814
native:
  ID3v2.3:
    - id: TALB
      value: Katze
    - id: TRCK
      value: '2'
    - id: TCON
      value: Electronic
    - id: TPE1
      value: Panixonik
    - id: TIT2
      value: Katze002
    - id: TYER
      value: '2001'
quality:
  warnings: []
common:
  track:
    'no': 2
    of: null
  disk:
    'no': null
    of: null
  album: Katze
  genre:
    - Electronic
  artists:
    - Panixonik
  artist: Panixonik
  title: Katze002
  year: 2001
transformed:
  ID3v2.3:
    TALB: Katze
    TRCK: '2'
    TCON: Electronic
    TPE1: Panixonik
    TIT2: Katze002
    TYER: '2001'
all:
  TALB: Katze
  TRCK: '2'
  TCON: Electronic
  TPE1: Panixonik
  TIT2: Katze002
  TYER: '2001'
nextTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/katze/3-katze003.mp3
  audio: /assets/albums/katze/3-katze003.mp3
  slug: katze/3-katze003
  albumSlug: katze
  trackSlug: 3-katze003
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/katze/3-katze003.jpeg
  cover: /assets/albums/katze/3-katze003.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 256000
    codecProfile: CBR
    tool: LAME 3.98.2
    duration: 151.3534693877551
  native:
    ID3v2.3:
      - id: TALB
        value: Katze
      - id: TRCK
        value: '3'
      - id: TCON
        value: Electronic
      - id: TPE1
        value: Panixonik
      - id: TIT2
        value: Katze003
      - id: TYER
        value: '2001'
  quality:
    warnings: []
  common:
    track:
      'no': 3
      of: null
    disk:
      'no': null
      of: null
    album: Katze
    genre:
      - Electronic
    artists:
      - Panixonik
    artist: Panixonik
    title: Katze003
    year: 2001
  transformed:
    ID3v2.3:
      TALB: Katze
      TRCK: '3'
      TCON: Electronic
      TPE1: Panixonik
      TIT2: Katze003
      TYER: '2001'
  all:
    TALB: Katze
    TRCK: '3'
    TCON: Electronic
    TPE1: Panixonik
    TIT2: Katze003
    TYER: '2001'
previousTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/katze/1-katze001.mp3
  audio: /assets/albums/katze/1-katze001.mp3
  slug: katze/1-katze001
  albumSlug: katze
  trackSlug: 1-katze001
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/katze/1-katze001.jpeg
  cover: /assets/albums/katze/1-katze001.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 256000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 116.03591836734694
  native:
    ID3v2.3:
      - id: TALB
        value: Katze
      - id: TRCK
        value: '1'
      - id: TCON
        value: Electronic
      - id: TPE1
        value: Panixonik
      - id: TIT2
        value: Katze001
      - id: TYER
        value: '2001'
  quality:
    warnings: []
  common:
    track:
      'no': 1
      of: null
    disk:
      'no': null
      of: null
    album: Katze
    genre:
      - Electronic
    artists:
      - Panixonik
    artist: Panixonik
    title: Katze001
    year: 2001
  transformed:
    ID3v2.3:
      TALB: Katze
      TRCK: '1'
      TCON: Electronic
      TPE1: Panixonik
      TIT2: Katze001
      TYER: '2001'
  all:
    TALB: Katze
    TRCK: '1'
    TCON: Electronic
    TPE1: Panixonik
    TIT2: Katze001
    TYER: '2001'
---
