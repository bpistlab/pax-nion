---
layout: track
path: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/no-progress-ep/3-no-variation.mp3
audio: /assets/albums/no-progress-ep/3-no-variation.mp3
slug: no-progress-ep/3-no-variation
albumSlug: no-progress-ep
trackSlug: 3-no-variation
coverPath: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/no-progress-ep/3-no-variation.jpeg
cover: /assets/albums/no-progress-ep/3-no-variation.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 128000
  codecProfile: CBR
  tool: LAME3.98r
  duration: 113.71102040816326
native:
  ID3v2.3:
    - id: TCON
      value: Electronic
    - id: TYER
      value: '2010'
    - id: TRCK
      value: '3'
    - id: TALB
      value: No Progress EP
    - id: TPE1
      value: Panixonik
    - id: TIT2
      value: No Variation
quality:
  warnings: []
common:
  track:
    'no': 3
    of: null
  disk:
    'no': null
    of: null
  genre:
    - Electronic
  year: 2010
  album: No Progress EP
  artists:
    - Panixonik
  artist: Panixonik
  title: No Variation
transformed:
  ID3v2.3:
    TCON: Electronic
    TYER: '2010'
    TRCK: '3'
    TALB: No Progress EP
    TPE1: Panixonik
    TIT2: No Variation
all:
  TCON: Electronic
  TYER: '2010'
  TRCK: '3'
  TALB: No Progress EP
  TPE1: Panixonik
  TIT2: No Variation
nextTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/no-progress-ep/1-no-stress.mp3
  audio: /assets/albums/no-progress-ep/1-no-stress.mp3
  slug: no-progress-ep/1-no-stress
  albumSlug: no-progress-ep
  trackSlug: 1-no-stress
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/no-progress-ep/1-no-stress.jpeg
  cover: /assets/albums/no-progress-ep/1-no-stress.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 128000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 134.6873469387755
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2010'
      - id: TRCK
        value: '1'
      - id: TALB
        value: No Progress EP
      - id: TIT2
        value: No Stress
      - id: TPE1
        value: Panixonik
  quality:
    warnings: []
  common:
    track:
      'no': 1
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2010
    album: No Progress EP
    title: No Stress
    artists:
      - Panixonik
    artist: Panixonik
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2010'
      TRCK: '1'
      TALB: No Progress EP
      TIT2: No Stress
      TPE1: Panixonik
  all:
    TCON: Electronic
    TYER: '2010'
    TRCK: '1'
    TALB: No Progress EP
    TIT2: No Stress
    TPE1: Panixonik
previousTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/no-progress-ep/2-no-sleep.mp3
  audio: /assets/albums/no-progress-ep/2-no-sleep.mp3
  slug: no-progress-ep/2-no-sleep
  albumSlug: no-progress-ep
  trackSlug: 2-no-sleep
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/no-progress-ep/2-no-sleep.jpeg
  cover: /assets/albums/no-progress-ep/2-no-sleep.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 128000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 97.77632653061225
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2010'
      - id: TRCK
        value: '2'
      - id: TALB
        value: No Progress EP
      - id: TPE1
        value: Panixonik
      - id: TIT2
        value: No Sleep
  quality:
    warnings: []
  common:
    track:
      'no': 2
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2010
    album: No Progress EP
    artists:
      - Panixonik
    artist: Panixonik
    title: No Sleep
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2010'
      TRCK: '2'
      TALB: No Progress EP
      TPE1: Panixonik
      TIT2: No Sleep
  all:
    TCON: Electronic
    TYER: '2010'
    TRCK: '2'
    TALB: No Progress EP
    TPE1: Panixonik
    TIT2: No Sleep
---
