---
layout: track
path: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/norrmalm-state/4-charlie-says.mp3
audio: /assets/albums/norrmalm-state/4-charlie-says.mp3
slug: norrmalm-state/4-charlie-says
albumSlug: norrmalm-state
trackSlug: 4-charlie-says
coverPath: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/norrmalm-state/4-charlie-says.jpeg
cover: /assets/albums/norrmalm-state/4-charlie-says.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 320000
  codecProfile: CBR
  tool: LAME3.98r
  duration: 63.66040816326531
native:
  ID3v2.3:
    - id: TCON
      value: Electronic
    - id: TALB
      value: Norrmalm State
    - id: TRCK
      value: '4'
    - id: TPE1
      value: Panix hilton
    - id: TIT2
      value: Charlie Says
    - id: TYER
      value: '2011'
quality:
  warnings: []
common:
  track:
    'no': 4
    of: null
  disk:
    'no': null
    of: null
  genre:
    - Electronic
  album: Norrmalm State
  artists:
    - Panix hilton
  artist: Panix hilton
  title: Charlie Says
  year: 2011
transformed:
  ID3v2.3:
    TCON: Electronic
    TALB: Norrmalm State
    TRCK: '4'
    TPE1: Panix hilton
    TIT2: Charlie Says
    TYER: '2011'
all:
  TCON: Electronic
  TALB: Norrmalm State
  TRCK: '4'
  TPE1: Panix hilton
  TIT2: Charlie Says
  TYER: '2011'
nextTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/norrmalm-state/1-diba-diba.mp3
  audio: /assets/albums/norrmalm-state/1-diba-diba.mp3
  slug: norrmalm-state/1-diba-diba
  albumSlug: norrmalm-state
  trackSlug: 1-diba-diba
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/norrmalm-state/1-diba-diba.jpeg
  cover: /assets/albums/norrmalm-state/1-diba-diba.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 131.34367346938777
  native:
    ID3v2.3:
      - id: TRCK
        value: '1'
      - id: TCON
        value: Electronic
      - id: TALB
        value: Norrmalm State
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: Diba Diba
      - id: TYER
        value: '2011'
  quality:
    warnings: []
  common:
    track:
      'no': 1
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    album: Norrmalm State
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: Diba Diba
    year: 2011
  transformed:
    ID3v2.3:
      TRCK: '1'
      TCON: Electronic
      TALB: Norrmalm State
      TPE1: Panix Hilton
      TIT2: Diba Diba
      TYER: '2011'
  all:
    TRCK: '1'
    TCON: Electronic
    TALB: Norrmalm State
    TPE1: Panix Hilton
    TIT2: Diba Diba
    TYER: '2011'
previousTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/norrmalm-state/3-kraaka.mp3
  audio: /assets/albums/norrmalm-state/3-kraaka.mp3
  slug: norrmalm-state/3-kraaka
  albumSlug: norrmalm-state
  trackSlug: 3-kraaka
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/norrmalm-state/3-kraaka.jpeg
  cover: /assets/albums/norrmalm-state/3-kraaka.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 100.88489795918368
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TRCK
        value: '3'
      - id: TALB
        value: Norrmalm State
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: Kraaka
      - id: TYER
        value: '2011'
  quality:
    warnings: []
  common:
    track:
      'no': 3
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    album: Norrmalm State
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: Kraaka
    year: 2011
  transformed:
    ID3v2.3:
      TCON: Electronic
      TRCK: '3'
      TALB: Norrmalm State
      TPE1: Panix Hilton
      TIT2: Kraaka
      TYER: '2011'
  all:
    TCON: Electronic
    TRCK: '3'
    TALB: Norrmalm State
    TPE1: Panix Hilton
    TIT2: Kraaka
    TYER: '2011'
---
