---
layout: track
path: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/extender/1-house-kommer-alltid-att-vara-house.mp3
audio: /assets/albums/extender/1-house-kommer-alltid-att-vara-house.mp3
slug: extender/1-house-kommer-alltid-att-vara-house
albumSlug: extender
trackSlug: 1-house-kommer-alltid-att-vara-house
coverPath: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/extender/1-house-kommer-alltid-att-vara-house.jpeg
cover: /assets/albums/extender/1-house-kommer-alltid-att-vara-house.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 320000
  codecProfile: CBR
  tool: LAME3.98r
  duration: 137.87428571428572
native:
  ID3v2.3:
    - id: TCON
      value: Electronic
    - id: TYER
      value: '2012'
    - id: TALB
      value: Extender
    - id: TRCK
      value: '1'
    - id: TPE1
      value: Panix Hilton
    - id: TIT2
      value: House Kommer Alltid Att Vara House
quality:
  warnings: []
common:
  track:
    'no': 1
    of: null
  disk:
    'no': null
    of: null
  genre:
    - Electronic
  year: 2012
  album: Extender
  artists:
    - Panix Hilton
  artist: Panix Hilton
  title: House Kommer Alltid Att Vara House
transformed:
  ID3v2.3:
    TCON: Electronic
    TYER: '2012'
    TALB: Extender
    TRCK: '1'
    TPE1: Panix Hilton
    TIT2: House Kommer Alltid Att Vara House
all:
  TCON: Electronic
  TYER: '2012'
  TALB: Extender
  TRCK: '1'
  TPE1: Panix Hilton
  TIT2: House Kommer Alltid Att Vara House
nextTrack:
  path: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/extender/2-i-want-your-soul.mp3
  audio: /assets/albums/extender/2-i-want-your-soul.mp3
  slug: extender/2-i-want-your-soul
  albumSlug: extender
  trackSlug: 2-i-want-your-soul
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/extender/2-i-want-your-soul.jpeg
  cover: /assets/albums/extender/2-i-want-your-soul.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 81.81551020408163
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2012'
      - id: TALB
        value: Extender
      - id: TRCK
        value: '2'
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: I Want Your Soul
  quality:
    warnings: []
  common:
    track:
      'no': 2
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2012
    album: Extender
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: I Want Your Soul
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2012'
      TALB: Extender
      TRCK: '2'
      TPE1: Panix Hilton
      TIT2: I Want Your Soul
  all:
    TCON: Electronic
    TYER: '2012'
    TALB: Extender
    TRCK: '2'
    TPE1: Panix Hilton
    TIT2: I Want Your Soul
previousTrack:
  path: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/extender/6-matter-of-opinion.mp3
  audio: /assets/albums/extender/6-matter-of-opinion.mp3
  slug: extender/6-matter-of-opinion
  albumSlug: extender
  trackSlug: 6-matter-of-opinion
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/extender/6-matter-of-opinion.jpeg
  cover: /assets/albums/extender/6-matter-of-opinion.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME 3.98.4
    duration: 81.81551020408163
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2012'
      - id: TALB
        value: Extender
      - id: TRCK
        value: '6'
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: Matter Of Opinion
  quality:
    warnings: []
  common:
    track:
      'no': 6
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2012
    album: Extender
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: Matter Of Opinion
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2012'
      TALB: Extender
      TRCK: '6'
      TPE1: Panix Hilton
      TIT2: Matter Of Opinion
  all:
    TCON: Electronic
    TYER: '2012'
    TALB: Extender
    TRCK: '6'
    TPE1: Panix Hilton
    TIT2: Matter Of Opinion
---
