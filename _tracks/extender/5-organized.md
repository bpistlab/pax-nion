---
layout: track
path: /mnt/usb32gb/organized-pax-nion/assets/albums/extender/5-organized.mp3
audio: /assets/albums/extender/5-organized.mp3
slug: extender/5-organized
albumSlug: extender
trackSlug: 5-organized
coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/extender/5-organized.jpeg
cover: /assets/albums/extender/5-organized.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 320000
  codecProfile: CBR
  tool: LAME3.98r
  duration: 76.12081632653062
native:
  ID3v2.3:
    - id: TCON
      value: Electronic
    - id: TYER
      value: '2012'
    - id: TALB
      value: Extender
    - id: TRCK
      value: '5'
    - id: TPE1
      value: Panix Hilton
    - id: TIT2
      value: Organized
quality:
  warnings: []
common:
  track:
    'no': 5
    of: null
  disk:
    'no': null
    of: null
  genre:
    - Electronic
  year: 2012
  album: Extender
  artists:
    - Panix Hilton
  artist: Panix Hilton
  title: Organized
transformed:
  ID3v2.3:
    TCON: Electronic
    TYER: '2012'
    TALB: Extender
    TRCK: '5'
    TPE1: Panix Hilton
    TIT2: Organized
all:
  TCON: Electronic
  TYER: '2012'
  TALB: Extender
  TRCK: '5'
  TPE1: Panix Hilton
  TIT2: Organized
nextTrack:
  path: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/extender/6-matter-of-opinion.mp3
  audio: /assets/albums/extender/6-matter-of-opinion.mp3
  slug: extender/6-matter-of-opinion
  albumSlug: extender
  trackSlug: 6-matter-of-opinion
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/extender/6-matter-of-opinion.jpeg
  cover: /assets/albums/extender/6-matter-of-opinion.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME 3.98.4
    duration: 81.81551020408163
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2012'
      - id: TALB
        value: Extender
      - id: TRCK
        value: '6'
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: Matter Of Opinion
  quality:
    warnings: []
  common:
    track:
      'no': 6
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2012
    album: Extender
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: Matter Of Opinion
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2012'
      TALB: Extender
      TRCK: '6'
      TPE1: Panix Hilton
      TIT2: Matter Of Opinion
  all:
    TCON: Electronic
    TYER: '2012'
    TALB: Extender
    TRCK: '6'
    TPE1: Panix Hilton
    TIT2: Matter Of Opinion
previousTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/extender/4-eru-sejjoez-.mp3
  audio: /assets/albums/extender/4-eru-sejjoez-.mp3
  slug: extender/4-eru-sejjoez-
  albumSlug: extender
  trackSlug: 4-eru-sejjoez-
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/extender/4-eru-sejjoez-.jpeg
  cover: /assets/albums/extender/4-eru-sejjoez-.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 118.93551020408164
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2012'
      - id: TALB
        value: Extender
      - id: TRCK
        value: '4'
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: Eru sejjoez?
  quality:
    warnings: []
  common:
    track:
      'no': 4
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2012
    album: Extender
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: Eru sejjoez?
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2012'
      TALB: Extender
      TRCK: '4'
      TPE1: Panix Hilton
      TIT2: Eru sejjoez?
  all:
    TCON: Electronic
    TYER: '2012'
    TALB: Extender
    TRCK: '4'
    TPE1: Panix Hilton
    TIT2: Eru sejjoez?
---
