---
layout: track
path: /mnt/usb32gb/organized-pax-nion/assets/albums/extender/4-eru-sejjoez-.mp3
audio: /assets/albums/extender/4-eru-sejjoez-.mp3
slug: extender/4-eru-sejjoez-
albumSlug: extender
trackSlug: 4-eru-sejjoez-
coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/extender/4-eru-sejjoez-.jpeg
cover: /assets/albums/extender/4-eru-sejjoez-.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 320000
  codecProfile: CBR
  tool: LAME3.98r
  duration: 118.93551020408164
native:
  ID3v2.3:
    - id: TCON
      value: Electronic
    - id: TYER
      value: '2012'
    - id: TALB
      value: Extender
    - id: TRCK
      value: '4'
    - id: TPE1
      value: Panix Hilton
    - id: TIT2
      value: Eru sejjoez?
quality:
  warnings: []
common:
  track:
    'no': 4
    of: null
  disk:
    'no': null
    of: null
  genre:
    - Electronic
  year: 2012
  album: Extender
  artists:
    - Panix Hilton
  artist: Panix Hilton
  title: Eru sejjoez?
transformed:
  ID3v2.3:
    TCON: Electronic
    TYER: '2012'
    TALB: Extender
    TRCK: '4'
    TPE1: Panix Hilton
    TIT2: Eru sejjoez?
all:
  TCON: Electronic
  TYER: '2012'
  TALB: Extender
  TRCK: '4'
  TPE1: Panix Hilton
  TIT2: Eru sejjoez?
nextTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/extender/5-organized.mp3
  audio: /assets/albums/extender/5-organized.mp3
  slug: extender/5-organized
  albumSlug: extender
  trackSlug: 5-organized
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/extender/5-organized.jpeg
  cover: /assets/albums/extender/5-organized.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 76.12081632653062
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2012'
      - id: TALB
        value: Extender
      - id: TRCK
        value: '5'
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: Organized
  quality:
    warnings: []
  common:
    track:
      'no': 5
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2012
    album: Extender
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: Organized
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2012'
      TALB: Extender
      TRCK: '5'
      TPE1: Panix Hilton
      TIT2: Organized
  all:
    TCON: Electronic
    TYER: '2012'
    TALB: Extender
    TRCK: '5'
    TPE1: Panix Hilton
    TIT2: Organized
previousTrack:
  path: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/extender/3-kim-ska-vi-koepa-lim-.mp3
  audio: /assets/albums/extender/3-kim-ska-vi-koepa-lim-.mp3
  slug: extender/3-kim-ska-vi-koepa-lim-
  albumSlug: extender
  trackSlug: 3-kim-ska-vi-koepa-lim-
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/extender/3-kim-ska-vi-koepa-lim-.jpeg
  cover: /assets/albums/extender/3-kim-ska-vi-koepa-lim-.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 85.62938775510204
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2012'
      - id: TALB
        value: Extender
      - id: TRCK
        value: '3'
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: Kim Ska Vi Koepa Lim?
  quality:
    warnings: []
  common:
    track:
      'no': 3
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2012
    album: Extender
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: Kim Ska Vi Koepa Lim?
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2012'
      TALB: Extender
      TRCK: '3'
      TPE1: Panix Hilton
      TIT2: Kim Ska Vi Koepa Lim?
  all:
    TCON: Electronic
    TYER: '2012'
    TALB: Extender
    TRCK: '3'
    TPE1: Panix Hilton
    TIT2: Kim Ska Vi Koepa Lim?
---
