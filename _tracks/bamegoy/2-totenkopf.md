---
layout: track
path: /mnt/usb32gb/organized-pax-nion/assets/albums/bamegoy/2-totenkopf.mp3
audio: /assets/albums/bamegoy/2-totenkopf.mp3
slug: bamegoy/2-totenkopf
albumSlug: bamegoy
trackSlug: 2-totenkopf
coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/bamegoy/2-totenkopf.jpeg
cover: /assets/albums/bamegoy/2-totenkopf.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 128000
  codecProfile: CBR
  numberOfSamples: 5680512
  duration: 128.80979591836734
native:
  ID3v2.3:
    - id: TCON
      value: Electronic
    - id: TALB
      value: Bamegoy
    - id: TPE1
      value: Pan1k
    - id: TIT2
      value: Totenkopf
    - id: TRCK
      value: '2'
    - id: TYER
      value: '2001'
quality:
  warnings: []
common:
  track:
    'no': 2
    of: null
  disk:
    'no': null
    of: null
  genre:
    - Electronic
  album: Bamegoy
  artists:
    - Pan1k
  artist: Pan1k
  title: Totenkopf
  year: 2001
transformed:
  ID3v2.3:
    TCON: Electronic
    TALB: Bamegoy
    TPE1: Pan1k
    TIT2: Totenkopf
    TRCK: '2'
    TYER: '2001'
all:
  TCON: Electronic
  TALB: Bamegoy
  TPE1: Pan1k
  TIT2: Totenkopf
  TRCK: '2'
  TYER: '2001'
nextTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/bamegoy/3-isolierung.mp3
  audio: /assets/albums/bamegoy/3-isolierung.mp3
  slug: bamegoy/3-isolierung
  albumSlug: bamegoy
  trackSlug: 3-isolierung
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/bamegoy/3-isolierung.jpeg
  cover: /assets/albums/bamegoy/3-isolierung.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 128000
    codecProfile: CBR
    numberOfSamples: 4531968
    duration: 102.76571428571428
  native:
    ID3v2.3:
      - id: TPE1
        value: Pan1k
      - id: TALB
        value: Bamegoy
      - id: TCON
        value: Electronic
      - id: TIT2
        value: Isolierung
      - id: TRCK
        value: '3'
      - id: TYER
        value: '2001'
  quality:
    warnings: []
  common:
    track:
      'no': 3
      of: null
    disk:
      'no': null
      of: null
    artists:
      - Pan1k
    artist: Pan1k
    album: Bamegoy
    genre:
      - Electronic
    title: Isolierung
    year: 2001
  transformed:
    ID3v2.3:
      TPE1: Pan1k
      TALB: Bamegoy
      TCON: Electronic
      TIT2: Isolierung
      TRCK: '3'
      TYER: '2001'
  all:
    TPE1: Pan1k
    TALB: Bamegoy
    TCON: Electronic
    TIT2: Isolierung
    TRCK: '3'
    TYER: '2001'
previousTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/bamegoy/1-nebelwerfer.mp3
  audio: /assets/albums/bamegoy/1-nebelwerfer.mp3
  slug: bamegoy/1-nebelwerfer
  albumSlug: bamegoy
  trackSlug: 1-nebelwerfer
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/bamegoy/1-nebelwerfer.jpeg
  cover: /assets/albums/bamegoy/1-nebelwerfer.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 128000
    codecProfile: CBR
    numberOfSamples: 7171200
    duration: 162.6122448979592
  native:
    ID3v2.3:
      - id: TALB
        value: Bamegoy
      - id: TYER
        value: '2001'
      - id: TCON
        value: Electronic
      - id: TRCK
        value: '1'
      - id: TIT2
        value: Nebelwerfer
      - id: TPE1
        value: Pan1k
  quality:
    warnings: []
  common:
    track:
      'no': 1
      of: null
    disk:
      'no': null
      of: null
    album: Bamegoy
    year: 2001
    genre:
      - Electronic
    title: Nebelwerfer
    artists:
      - Pan1k
    artist: Pan1k
  transformed:
    ID3v2.3:
      TALB: Bamegoy
      TYER: '2001'
      TCON: Electronic
      TRCK: '1'
      TIT2: Nebelwerfer
      TPE1: Pan1k
  all:
    TALB: Bamegoy
    TYER: '2001'
    TCON: Electronic
    TRCK: '1'
    TIT2: Nebelwerfer
    TPE1: Pan1k
---
