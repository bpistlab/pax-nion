---
layout: track
path: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/shortjams-vol-1/5-shortjam-bonus-beats.mp3
audio: /assets/albums/shortjams-vol-1/5-shortjam-bonus-beats.mp3
slug: shortjams-vol-1/5-shortjam-bonus-beats
albumSlug: shortjams-vol-1
trackSlug: 5-shortjam-bonus-beats
coverPath: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/shortjams-vol-1/5-shortjam-bonus-beats.jpeg
cover: /assets/albums/shortjams-vol-1/5-shortjam-bonus-beats.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 320000
  codecProfile: CBR
  tool: LAME3.99r
  duration: 323.05632653061224
native:
  ID3v2.3:
    - id: TCON
      value: Electronic
    - id: TALB
      value: Shortjams vol 1
    - id: TPE1
      value: Panix Hilton
    - id: TIT2
      value: Shortjam Bonus Beats
    - id: TRCK
      value: '5'
    - id: TYER
      value: '2013'
quality:
  warnings: []
common:
  track:
    'no': 5
    of: null
  disk:
    'no': null
    of: null
  genre:
    - Electronic
  album: Shortjams vol 1
  artists:
    - Panix Hilton
  artist: Panix Hilton
  title: Shortjam Bonus Beats
  year: 2013
transformed:
  ID3v2.3:
    TCON: Electronic
    TALB: Shortjams vol 1
    TPE1: Panix Hilton
    TIT2: Shortjam Bonus Beats
    TRCK: '5'
    TYER: '2013'
all:
  TCON: Electronic
  TALB: Shortjams vol 1
  TPE1: Panix Hilton
  TIT2: Shortjam Bonus Beats
  TRCK: '5'
  TYER: '2013'
nextTrack:
  path: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/shortjams-vol-1/1-rise-or-shine.mp3
  audio: /assets/albums/shortjams-vol-1/1-rise-or-shine.mp3
  slug: shortjams-vol-1/1-rise-or-shine
  albumSlug: shortjams-vol-1
  trackSlug: 1-rise-or-shine
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/shortjams-vol-1/1-rise-or-shine.jpeg
  cover: /assets/albums/shortjams-vol-1/1-rise-or-shine.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.99r
    duration: 150.0734693877551
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TALB
        value: Shortjams vol 1
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: Rise Or Shine
      - id: TRCK
        value: '1'
      - id: TYER
        value: '2013'
  quality:
    warnings: []
  common:
    track:
      'no': 1
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    album: Shortjams vol 1
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: Rise Or Shine
    year: 2013
  transformed:
    ID3v2.3:
      TCON: Electronic
      TALB: Shortjams vol 1
      TPE1: Panix Hilton
      TIT2: Rise Or Shine
      TRCK: '1'
      TYER: '2013'
  all:
    TCON: Electronic
    TALB: Shortjams vol 1
    TPE1: Panix Hilton
    TIT2: Rise Or Shine
    TRCK: '1'
    TYER: '2013'
previousTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/shortjams-vol-1/4-patios.mp3
  audio: /assets/albums/shortjams-vol-1/4-patios.mp3
  slug: shortjams-vol-1/4-patios
  albumSlug: shortjams-vol-1
  trackSlug: 4-patios
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/shortjams-vol-1/4-patios.jpeg
  cover: /assets/albums/shortjams-vol-1/4-patios.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.99r
    duration: 152.3461224489796
  native:
    ID3v2.3:
      - id: TIT2
        value: Patios
      - id: TCON
        value: Electronic
      - id: TALB
        value: Shortjams vol 1
      - id: TPE1
        value: Panix Hilton
      - id: TRCK
        value: '4'
      - id: TYER
        value: '2013'
  quality:
    warnings: []
  common:
    track:
      'no': 4
      of: null
    disk:
      'no': null
      of: null
    title: Patios
    genre:
      - Electronic
    album: Shortjams vol 1
    artists:
      - Panix Hilton
    artist: Panix Hilton
    year: 2013
  transformed:
    ID3v2.3:
      TIT2: Patios
      TCON: Electronic
      TALB: Shortjams vol 1
      TPE1: Panix Hilton
      TRCK: '4'
      TYER: '2013'
  all:
    TIT2: Patios
    TCON: Electronic
    TALB: Shortjams vol 1
    TPE1: Panix Hilton
    TRCK: '4'
    TYER: '2013'
---
