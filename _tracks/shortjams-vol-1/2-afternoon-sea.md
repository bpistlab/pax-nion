---
layout: track
path: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/shortjams-vol-1/2-afternoon-sea.mp3
audio: /assets/albums/shortjams-vol-1/2-afternoon-sea.mp3
slug: shortjams-vol-1/2-afternoon-sea
albumSlug: shortjams-vol-1
trackSlug: 2-afternoon-sea
coverPath: >-
  /mnt/usb32gb/organized-pax-nion/assets/albums/shortjams-vol-1/2-afternoon-sea.jpeg
cover: /assets/albums/shortjams-vol-1/2-afternoon-sea.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 320000
  codecProfile: CBR
  tool: LAME3.99r
  duration: 143.28163265306122
native:
  ID3v2.3:
    - id: TALB
      value: Shortjams vol 1
    - id: TIT2
      value: Afternoon Sea
    - id: TCON
      value: Electronic
    - id: TPE1
      value: Panix Hilton
    - id: TRCK
      value: '2'
    - id: TYER
      value: '2013'
quality:
  warnings: []
common:
  track:
    'no': 2
    of: null
  disk:
    'no': null
    of: null
  album: Shortjams vol 1
  title: Afternoon Sea
  genre:
    - Electronic
  artists:
    - Panix Hilton
  artist: Panix Hilton
  year: 2013
transformed:
  ID3v2.3:
    TALB: Shortjams vol 1
    TIT2: Afternoon Sea
    TCON: Electronic
    TPE1: Panix Hilton
    TRCK: '2'
    TYER: '2013'
all:
  TALB: Shortjams vol 1
  TIT2: Afternoon Sea
  TCON: Electronic
  TPE1: Panix Hilton
  TRCK: '2'
  TYER: '2013'
nextTrack:
  path: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/shortjams-vol-1/3-riding-doherty.mp3
  audio: /assets/albums/shortjams-vol-1/3-riding-doherty.mp3
  slug: shortjams-vol-1/3-riding-doherty
  albumSlug: shortjams-vol-1
  trackSlug: 3-riding-doherty
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/shortjams-vol-1/3-riding-doherty.jpeg
  cover: /assets/albums/shortjams-vol-1/3-riding-doherty.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.99r
    duration: 158.69387755102042
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TPE1
        value: Panix Hilton
      - id: TALB
        value: Shortjams vol 1
      - id: TRCK
        value: '3'
      - id: TIT2
        value: Riding Doherty
      - id: TYER
        value: '2013'
  quality:
    warnings: []
  common:
    track:
      'no': 3
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    artists:
      - Panix Hilton
    artist: Panix Hilton
    album: Shortjams vol 1
    title: Riding Doherty
    year: 2013
  transformed:
    ID3v2.3:
      TCON: Electronic
      TPE1: Panix Hilton
      TALB: Shortjams vol 1
      TRCK: '3'
      TIT2: Riding Doherty
      TYER: '2013'
  all:
    TCON: Electronic
    TPE1: Panix Hilton
    TALB: Shortjams vol 1
    TRCK: '3'
    TIT2: Riding Doherty
    TYER: '2013'
previousTrack:
  path: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/shortjams-vol-1/1-rise-or-shine.mp3
  audio: /assets/albums/shortjams-vol-1/1-rise-or-shine.mp3
  slug: shortjams-vol-1/1-rise-or-shine
  albumSlug: shortjams-vol-1
  trackSlug: 1-rise-or-shine
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/shortjams-vol-1/1-rise-or-shine.jpeg
  cover: /assets/albums/shortjams-vol-1/1-rise-or-shine.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.99r
    duration: 150.0734693877551
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TALB
        value: Shortjams vol 1
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: Rise Or Shine
      - id: TRCK
        value: '1'
      - id: TYER
        value: '2013'
  quality:
    warnings: []
  common:
    track:
      'no': 1
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    album: Shortjams vol 1
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: Rise Or Shine
    year: 2013
  transformed:
    ID3v2.3:
      TCON: Electronic
      TALB: Shortjams vol 1
      TPE1: Panix Hilton
      TIT2: Rise Or Shine
      TRCK: '1'
      TYER: '2013'
  all:
    TCON: Electronic
    TALB: Shortjams vol 1
    TPE1: Panix Hilton
    TIT2: Rise Or Shine
    TRCK: '1'
    TYER: '2013'
---
