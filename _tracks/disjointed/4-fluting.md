---
layout: track
path: /mnt/usb32gb/organized-pax-nion/assets/albums/disjointed/4-fluting.mp3
audio: /assets/albums/disjointed/4-fluting.mp3
slug: disjointed/4-fluting
albumSlug: disjointed
trackSlug: 4-fluting
coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/disjointed/4-fluting.jpeg
cover: /assets/albums/disjointed/4-fluting.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 320000
  codecProfile: CBR
  tool: LAME3.98r
  duration: 100.88489795918368
native:
  ID3v2.3:
    - id: TCON
      value: Electronic
    - id: TYER
      value: '2013'
    - id: TALB
      value: Disjointed
    - id: TRCK
      value: '4'
    - id: TPE1
      value: Panix Hilton
    - id: TIT2
      value: Fluting
quality:
  warnings: []
common:
  track:
    'no': 4
    of: null
  disk:
    'no': null
    of: null
  genre:
    - Electronic
  year: 2013
  album: Disjointed
  artists:
    - Panix Hilton
  artist: Panix Hilton
  title: Fluting
transformed:
  ID3v2.3:
    TCON: Electronic
    TYER: '2013'
    TALB: Disjointed
    TRCK: '4'
    TPE1: Panix Hilton
    TIT2: Fluting
all:
  TCON: Electronic
  TYER: '2013'
  TALB: Disjointed
  TRCK: '4'
  TPE1: Panix Hilton
  TIT2: Fluting
nextTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/disjointed/5-stoekbroet.mp3
  audio: /assets/albums/disjointed/5-stoekbroet.mp3
  slug: disjointed/5-stoekbroet
  albumSlug: disjointed
  trackSlug: 5-stoekbroet
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/disjointed/5-stoekbroet.jpeg
  cover: /assets/albums/disjointed/5-stoekbroet.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 110.68081632653062
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2013'
      - id: TALB
        value: Disjointed
      - id: TRCK
        value: '5'
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: Stoekbroet
  quality:
    warnings: []
  common:
    track:
      'no': 5
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2013
    album: Disjointed
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: Stoekbroet
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2013'
      TALB: Disjointed
      TRCK: '5'
      TPE1: Panix Hilton
      TIT2: Stoekbroet
  all:
    TCON: Electronic
    TYER: '2013'
    TALB: Disjointed
    TRCK: '5'
    TPE1: Panix Hilton
    TIT2: Stoekbroet
previousTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/disjointed/3-oh-yeah.mp3
  audio: /assets/albums/disjointed/3-oh-yeah.mp3
  slug: disjointed/3-oh-yeah
  albumSlug: disjointed
  trackSlug: 3-oh-yeah
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/disjointed/3-oh-yeah.jpeg
  cover: /assets/albums/disjointed/3-oh-yeah.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.99r
    duration: 143.90857142857143
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2013'
      - id: TALB
        value: Disjointed
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: Oh Yeah
      - id: TRCK
        value: '3'
  quality:
    warnings: []
  common:
    track:
      'no': 3
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2013
    album: Disjointed
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: Oh Yeah
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2013'
      TALB: Disjointed
      TPE1: Panix Hilton
      TIT2: Oh Yeah
      TRCK: '3'
  all:
    TCON: Electronic
    TYER: '2013'
    TALB: Disjointed
    TPE1: Panix Hilton
    TIT2: Oh Yeah
    TRCK: '3'
---
