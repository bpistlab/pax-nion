---
layout: track
path: /mnt/usb32gb/organized-pax-nion/assets/albums/disjointed/1-disjointed.mp3
audio: /assets/albums/disjointed/1-disjointed.mp3
slug: disjointed/1-disjointed
albumSlug: disjointed
trackSlug: 1-disjointed
coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/disjointed/1-disjointed.jpeg
cover: /assets/albums/disjointed/1-disjointed.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 320000
  codecProfile: CBR
  tool: LAME3.99r
  duration: 74.2138775510204
native:
  ID3v2.3:
    - id: TCON
      value: Electronic
    - id: TYER
      value: '2013'
    - id: TALB
      value: Disjointed
    - id: TPE1
      value: Panix Hilton
    - id: TIT2
      value: Disjointed
    - id: TRCK
      value: '1'
quality:
  warnings: []
common:
  track:
    'no': 1
    of: null
  disk:
    'no': null
    of: null
  genre:
    - Electronic
  year: 2013
  album: Disjointed
  artists:
    - Panix Hilton
  artist: Panix Hilton
  title: Disjointed
transformed:
  ID3v2.3:
    TCON: Electronic
    TYER: '2013'
    TALB: Disjointed
    TPE1: Panix Hilton
    TIT2: Disjointed
    TRCK: '1'
all:
  TCON: Electronic
  TYER: '2013'
  TALB: Disjointed
  TPE1: Panix Hilton
  TIT2: Disjointed
  TRCK: '1'
nextTrack:
  path: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/disjointed/2-ze-morning-j-m.mp3
  audio: /assets/albums/disjointed/2-ze-morning-j-m.mp3
  slug: disjointed/2-ze-morning-j-m
  albumSlug: disjointed
  trackSlug: 2-ze-morning-j-m
  coverPath: >-
    /mnt/usb32gb/organized-pax-nion/assets/albums/disjointed/2-ze-morning-j-m.jpeg
  cover: /assets/albums/disjointed/2-ze-morning-j-m.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 106.57959183673469
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2013'
      - id: TALB
        value: Disjointed
      - id: TRCK
        value: '2'
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: Ze Morning Jäm
  quality:
    warnings: []
  common:
    track:
      'no': 2
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2013
    album: Disjointed
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: Ze Morning Jäm
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2013'
      TALB: Disjointed
      TRCK: '2'
      TPE1: Panix Hilton
      TIT2: Ze Morning Jäm
  all:
    TCON: Electronic
    TYER: '2013'
    TALB: Disjointed
    TRCK: '2'
    TPE1: Panix Hilton
    TIT2: Ze Morning Jäm
previousTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/disjointed/6-pure-shite.mp3
  audio: /assets/albums/disjointed/6-pure-shite.mp3
  slug: disjointed/6-pure-shite
  albumSlug: disjointed
  trackSlug: 6-pure-shite
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/disjointed/6-pure-shite.jpeg
  cover: /assets/albums/disjointed/6-pure-shite.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 118.0734693877551
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2013'
      - id: TALB
        value: Disjointed
      - id: TRCK
        value: '6'
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: Pure Shite
  quality:
    warnings: []
  common:
    track:
      'no': 6
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2013
    album: Disjointed
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: Pure Shite
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2013'
      TALB: Disjointed
      TRCK: '6'
      TPE1: Panix Hilton
      TIT2: Pure Shite
  all:
    TCON: Electronic
    TYER: '2013'
    TALB: Disjointed
    TRCK: '6'
    TPE1: Panix Hilton
    TIT2: Pure Shite
---
