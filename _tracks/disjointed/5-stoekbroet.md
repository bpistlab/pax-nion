---
layout: track
path: /mnt/usb32gb/organized-pax-nion/assets/albums/disjointed/5-stoekbroet.mp3
audio: /assets/albums/disjointed/5-stoekbroet.mp3
slug: disjointed/5-stoekbroet
albumSlug: disjointed
trackSlug: 5-stoekbroet
coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/disjointed/5-stoekbroet.jpeg
cover: /assets/albums/disjointed/5-stoekbroet.jpeg
format:
  tagTypes:
    - ID3v2.3
  trackInfo: []
  lossless: false
  container: MPEG
  codec: MPEG 1 Layer 3
  sampleRate: 44100
  numberOfChannels: 2
  bitrate: 320000
  codecProfile: CBR
  tool: LAME3.98r
  duration: 110.68081632653062
native:
  ID3v2.3:
    - id: TCON
      value: Electronic
    - id: TYER
      value: '2013'
    - id: TALB
      value: Disjointed
    - id: TRCK
      value: '5'
    - id: TPE1
      value: Panix Hilton
    - id: TIT2
      value: Stoekbroet
quality:
  warnings: []
common:
  track:
    'no': 5
    of: null
  disk:
    'no': null
    of: null
  genre:
    - Electronic
  year: 2013
  album: Disjointed
  artists:
    - Panix Hilton
  artist: Panix Hilton
  title: Stoekbroet
transformed:
  ID3v2.3:
    TCON: Electronic
    TYER: '2013'
    TALB: Disjointed
    TRCK: '5'
    TPE1: Panix Hilton
    TIT2: Stoekbroet
all:
  TCON: Electronic
  TYER: '2013'
  TALB: Disjointed
  TRCK: '5'
  TPE1: Panix Hilton
  TIT2: Stoekbroet
nextTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/disjointed/6-pure-shite.mp3
  audio: /assets/albums/disjointed/6-pure-shite.mp3
  slug: disjointed/6-pure-shite
  albumSlug: disjointed
  trackSlug: 6-pure-shite
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/disjointed/6-pure-shite.jpeg
  cover: /assets/albums/disjointed/6-pure-shite.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 118.0734693877551
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2013'
      - id: TALB
        value: Disjointed
      - id: TRCK
        value: '6'
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: Pure Shite
  quality:
    warnings: []
  common:
    track:
      'no': 6
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2013
    album: Disjointed
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: Pure Shite
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2013'
      TALB: Disjointed
      TRCK: '6'
      TPE1: Panix Hilton
      TIT2: Pure Shite
  all:
    TCON: Electronic
    TYER: '2013'
    TALB: Disjointed
    TRCK: '6'
    TPE1: Panix Hilton
    TIT2: Pure Shite
previousTrack:
  path: /mnt/usb32gb/organized-pax-nion/assets/albums/disjointed/4-fluting.mp3
  audio: /assets/albums/disjointed/4-fluting.mp3
  slug: disjointed/4-fluting
  albumSlug: disjointed
  trackSlug: 4-fluting
  coverPath: /mnt/usb32gb/organized-pax-nion/assets/albums/disjointed/4-fluting.jpeg
  cover: /assets/albums/disjointed/4-fluting.jpeg
  format:
    tagTypes:
      - ID3v2.3
    trackInfo: []
    lossless: false
    container: MPEG
    codec: MPEG 1 Layer 3
    sampleRate: 44100
    numberOfChannels: 2
    bitrate: 320000
    codecProfile: CBR
    tool: LAME3.98r
    duration: 100.88489795918368
  native:
    ID3v2.3:
      - id: TCON
        value: Electronic
      - id: TYER
        value: '2013'
      - id: TALB
        value: Disjointed
      - id: TRCK
        value: '4'
      - id: TPE1
        value: Panix Hilton
      - id: TIT2
        value: Fluting
  quality:
    warnings: []
  common:
    track:
      'no': 4
      of: null
    disk:
      'no': null
      of: null
    genre:
      - Electronic
    year: 2013
    album: Disjointed
    artists:
      - Panix Hilton
    artist: Panix Hilton
    title: Fluting
  transformed:
    ID3v2.3:
      TCON: Electronic
      TYER: '2013'
      TALB: Disjointed
      TRCK: '4'
      TPE1: Panix Hilton
      TIT2: Fluting
  all:
    TCON: Electronic
    TYER: '2013'
    TALB: Disjointed
    TRCK: '4'
    TPE1: Panix Hilton
    TIT2: Fluting
---
